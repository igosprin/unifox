<?php

function dateGNU($str,$del='/')
{
	$e=explode($del,$str);
	$s='';
	$s=$e[2].'-'.$e[1].'-'.$e[0];
	return $s;
}


function generateUrl( $text ) 
{
  $text = mb_strtolower( $text, 'UTF-8' );
  $fromSymbol = array(
   ' ', '_', '.', ',', '?', '&', '$', '#', '@', '!', '+', '(', ')', '*', '=', '/', '`', '~', '"', '\'', '^', '<', '>', ':', ';', '/', '{', '}', '№', '%'
  );
  
  $toSymbol = array(
   '-', '-', '-', '-', '', 'and', 's', 'cs', 'at', '', 'plus', '-', '-', '-',  
   '-', '-', '', '-', '', '', '', 
   '', '', '-', '', '-', '-', '-', 'no', ''
  );
  
  $text = str_replace( $fromSymbol, $toSymbol, $text );
  
  $fromLetter =  array( 'й', 'ц',  'у', 'к', 'е', 'н', 'г', 'ш',  'щ',   'з', 'х',  'ъ', 'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'э', 'я',  'ч',  'с', 'м', 'и', 'т', 'ь', 'б', 'ю',  'ё',  'ї',  'і', 'є', '‘', 'ª', '¿', '’', 'Ñ', '¨', 'ñ', '¡', '²', 'é', 'è', 'ç', 'à', 'ù', '°', '¨', '£', 'µ',  '§' );
  $toLetter =  array( 'j', 'tc', 'u', 'k', 'e', 'n', 'g', 'sh', 'tsh', 'z', 'kh', '',  'f', 'y', 'v', 'a', 'p', 'r', 'o', 'l', 'd', 'j', 'e', 'ja', 'ch', 's', 'm', 'i', 't', '',  'b', 'ju', 'je', 'ji', 'i', 'e', '',  'a', '',  '',  'n', '',  'n', 'i', '2', 'e', 'e', 'c', 'a', 'u', '',  '',  'f', 'mu', 's' );
  
  $text = str_replace( $fromLetter, $toLetter, $text );
  
  $text = str_replace( '--', '-', $text );
  
  if (strlen($text) > 0 && $text[0]=='-') {
   $text = substr( $text, 1, strlen($text) - 1 );
  }
  
  if (strlen($text) > 0 && $text[strlen($text)-1] == '-') {
   $text = substr( $text, 0, strlen($text) - 1 );
  }
  
  return $text;
  
}
function CatchError($cont)
{
	$content=$cont;
	$d=include PATH_SCRIPT.'/'.MODEL.'/404.php';
	return $d;
}

function PagingSaite($all_count,$count_per_page,$act_page,$link)
{
		//$all_count Всего страниц,
		//$count_per_page 
		//$act_page Активная страница
		//$link	-ссылка
		$returnPaging=array();
		$returnPaging['page_list']=array();
		$returnPaging['activation']=0;
		$nums = $count_per_page;			
		$page = $act_page;			
		$elements = $all_count;
		
		$pages = ceil($elements/$count_per_page);

		if ($pages <= 1) {
			return $returnPaging;
			
		}
		if ($page < 1) {
			return $returnPaging;
			
		}
		elseif ($page > $pages) {
			$page = $pages;
		}		
			
		
		$neighbours = 5;
		
		$left_neighbour = $page - $neighbours;
		if ($left_neighbour < 1) $left_neighbour = 1;

		$right_neighbour = $page + $neighbours;
		if ($right_neighbour > $pages) $right_neighbour = $pages;

		
		if ($page > 1)
		{
			$np=$page-1;
			if($np!=1)
				$returnPaging['left_bar']=array('link'=>$link.'?page='.$np);
			else
				$returnPaging['left_bar']=array('link'=>$link);
		}
			
				
		
		for ($i=$left_neighbour; $i<=$right_neighbour; $i++) {
			if($i!=1)
				$returnPaging['page_list'][$i]=array('link'=>$link.'?page='.$i);
			else	
				$returnPaging['page_list'][$i]=array('link'=>$link);
			if ($i == $page) 	
				$returnPaging['page_list'][$i]['active']=1;
		}

		if ($page < $pages)	
			$returnPaging['right_bar']=array('link'=>$link.'?page='.($page+1)); 
		
		$returnPaging['activation']=1;
		return $returnPaging;
}
/*
function tenderBlock($limit=1)
{
	$data=array();
	$sql='select t.*,r.title as reg_name from tenders t,regions r where t.status=1 and r.id=t.region order by r.id desc limit 0,'.$limit;		
	$data['list']=SelectAll($sql);
	return parserPage('tender_item.html',$data);
}*/
function escapeString($str)
{
	$str=str_replace(array('*','select','from'),'',$str);
	$srt=htmlspecialchars(strip_tags($str));
	return $srt;
}
function getUrlLogic()
{	
	$url=$_SERVER['REQUEST_URI'];
	$m=explode('?',$url);
	$url=$m[0];
	if(substr($url,-1)!='/') 
			return false;	
	return true;
	
}
function getUrlAddSlesh()
{	
	$url=$_SERVER['REQUEST_URI'];
	$m=explode('?',$url);
	$url=$m[0];
	if(substr($url,-1)!='/') $url=$url.'/';				
	return $url;	
}
function getUrlDelSlesh()
{	
	$url=$_SERVER['REQUEST_URI'];
	$m=explode('?',$url);
	$url=$m[0];
	if(substr($url,-1)=='/')
	{
		$url=substr($url,0,(strlen($url)-1));
	} 			
	return $url;	
}

function smallImg($path,$w,$name)
{	
	$ftp_path=PATH_ROOT.$path;	
	
	$name_new=str_replace('.jpg','_s6_'.$w.'.jpg',$name);
	
	$postPref=IMG_PATH_SRC;
	//return $postPref.$path.$name;
	$returnPath='';
	if(!file_exists($ftp_path.'/'.$name_new))
	{
		if(file_exists($ftp_path.'/'.$name))
		{
			$sz = getimagesize($ftp_path.'/'.$name);
			if ($sz)
			{
			  $iw = $sz[0];
			  $ih = $sz[1];
			  $im = @imagecreatefromjpeg($ftp_path.'/'.$name);
				if ($im)
				{
					if ($iw > $w && $w!=0 )
					{
						if($iw>$ih){
							$iw = $iw/($ih/$w);
							$ih=$w;							
						}
						else{

								$ih = $ih*$w/$iw;
								$iw = $w;                       
						}

					}                 
					$iw = (int) $iw;
					$ih = (int) $ih;
					
					$out_im = imagecreatetruecolor($w,$w);
					imagecopyresampled($out_im, $im, 0, 0, 0, 0, $iw, $ih, $sz[0], $sz[1]);                
					imagejpeg ($out_im,$ftp_path.'/'.$name_new ,55 );
					imagedestroy($out_im);
					
				}

			}
		}	
		
	}
	$returnPath=$postPref.$path.$name_new;

	return $returnPath;
}

function smallImgRectangle($path,$w,$name)
{	
	$ftp_path=PATH_ROOT.$path;	
	
	$name_new=str_replace('.jpg','_rectangle_w5_'.$w.'.jpg',$name);
	
	$postPref=IMG_PATH_SRC;
	//return $postPref.$path.$name;
	$returnPath='';
	if(!file_exists($ftp_path.'/'.$name_new))
	{
		if(file_exists($ftp_path.'/'.$name))
		{
			$sz = getimagesize($ftp_path.'/'.$name);
			if ($sz)
			{
			  $iw = $sz[0];
			  $ih = $sz[1];
			  $im = @imagecreatefromjpeg($ftp_path.'/'.$name);
				if ($im)
				{
					if ($iw > $w && $w!=0 )
					{
						if($iw>$ih){
							$iw = $iw/($ih/$w);
							$ih=$w;							
						}
						else{

								$ih = $ih*$w/$iw;
								$iw = $w;                       
						}

					}                 
					$iw = (int) $iw;
					$ih = (int) $ih;
					
					$out_im = imagecreatetruecolor($iw,$ih);
					imagecopyresampled($out_im, $im, 0, 0, 0, 0, $iw, $ih, $sz[0], $sz[1]);                
					imagejpeg ($out_im,$ftp_path.'/'.$name_new ,85 );
					imagedestroy($out_im);
					
				}

			}
		}	
		
	}
	$returnPath=$postPref.$path.$name_new;

	return $returnPath;
}
function smallImgRectangleProp($path,$w,$name,$h,$background='')
{	
	$ftp_path=PATH_ROOT.$path;	
	
	$name_new=str_replace('.jpg','_rectangle_props_w90_'.$w.'.jpg',$name);
	
	$postPref=IMG_PATH_SRC;
	//return $postPref.$path.$name;
	$returnPath='';
	if(!file_exists($ftp_path.'/'.$name_new))
	{
		if(file_exists($ftp_path.'/'.$name))
		{
			$sz = getimagesize($ftp_path.'/'.$name);
			if ($sz)
			{
			  $iw = $sz[0];
			  $ih = $sz[1];
			  $im = @imagecreatefromjpeg($ftp_path.'/'.$name);
				if ($im)
				{
					if ($iw > $w && $w!=0 )
					{
						if($iw>$ih){
							
							$ih=$ih/($iw/$w);	
							$iw=$w;
																	
													
						}
						else{

								$ih = $ih*$w/$iw;
								$iw = $w;                       
						}

					}                 
					$iw = (int) $iw;
					$ih = (int) $ih;
					
					$out_im = imagecreatetruecolor($w,$h);
					if($background!='')
						imagefill($out_im, 0, 0, $background); 
					imagecopyresampled($out_im, $im, 0, 0, 0, 0, $iw, $ih, $sz[0], $sz[1]);                
					imagejpeg ($out_im,$ftp_path.'/'.$name_new ,90);
					imagedestroy($out_im);
					
				}

			}
		}	
		
	}
	$returnPath=$postPref.$path.$name_new;

	return $returnPath;
}
function generateCode()
{
	$chars = 'abdefhknrstyz23456789';
	$length = rand(4, 6);
	$numChars = strlen($chars);
	
	// формируем код
	$str = '';
	for ($i = 0; $i < $length; $i++) 
	{
		$str .= substr($chars, rand(1, $numChars) - 1, 1);
	}
	
	// перемешиваем на всякий случай
	$array_mix = preg_split('//', $str, -1, PREG_SPLIT_NO_EMPTY);
		srand ((float)microtime()*1000000);
		shuffle ($array_mix);
	
	return implode("", $array_mix);
}

function mailSend($title,$text,$email_to,$email_from)
{
	if($email_from=='')
		$email_from='mail@arbitas.com';
  $go="MIME-Version: 1.0\r\n";
  $go.="Content-Type: text/html; charset=utf-8 \r\n";
  $go.= "From: allfirma.com <".$email_from.">\r\n";
  $go.= "Cc: ".$email_from."\r\n";
  $go.= "Bcc: ".$email_from."\r\n";
    @mail($email_to, $title, $text, $go);
}

 

?>