<?php
$info_type_key=array(
	/*'adres'=>array(
					'input'=>array('country','city','adres'),
					'file'=>array('proff_adres'),
				),*/
	'document'=>array(
					'input'=>array(),
					'file'=>array('documents_1','documents_2','selfi'),
				),
	'personal_info'=>array(
					'input'=>array('name','birthdate'),
					'file'=>array(),
				),				
);

if($content['personal_user_info']['is_company'])
{
	$info_type_key['company_info']=array(
					'input'=>array(
						'name_company','adress_company','vat_number'
					),
					'file'=>array('documents_company_1','documents_company_2'),
				);
}	

$info_name=array('documents_1'=>'Frontside','documents_2'=>'Backside','documents_company_1'=>'Certificate of incorporation','documents_company_2'=>'Power of attorney');

if(isset($_POST['createcode']))
{
	$code=strtoupper(md5($content['personal_user_info']['id'].$content['personal_user_info']['email'].time()));
	$db_connectors->DeleteRow('email_verification_code',array('id_user'=>intval($content['personal_user_info']['id'])));
	$insert=array('id_user'=>intval($content['personal_user_info']['id']),'code'=>$code,'date'=>time(),'email'=>$content['personal_user_info']['email']);
	$db_connectors->InsertRow('email_verification_code',$insert);
	
	include 'library/send_mail.php';
	$SEND_MAIL=new SendMail(PATH_TPL.'mail');
	$tp=array('code'=>$code,'ip'=>USER_IP_ADDRESS);
	$data_publish=array('title'=>'Activate account','action'=>'activate_email','email'=>$content['personal_user_info']['email']);
	$SEND_MAIL->send($data_publish,$tp);
	$result=array('result'=>true,'text'=>'');
	die(json_encode($result));
}
//обычная проверка кода и верификация мыла
if(isset($_POST['emailverif']))
{
	$_POST['code_mail']=trim($_POST['code_mail']);
	$email=$content['personal_user_info']['email'];
	$result=array('result'=>false);
	if(isset($_POST['code_mail']))
	{
		$us=$db_connectors->FetchRow('select * from email_verification_code where id_user='.intval($content['personal_user_info']['id']).' and code="'.$db_connectors->sql_escape($_POST['code_mail']).'" and email="'.$email.'"');
		if($us)
		{
			$result['result']=true;
			set_verificationUser($db_connectors,$content['personal_user_info']['id'],'email',1);
			$user_all_status=get_verificationUser($db_connectors,$content['personal_user_info']['id']);
			if($user_all_status['is_email']==1 and $user_all_status['is_document']==1 and $user_all_status['is_adres']==1 and $user_all_status['is_personal_info']==1 and $content['personal_user_info']['is_verification']==0)
				$db_connectors->UpdateRow('users', array('is_verification'=>1),array('id'=>intval($content['personal_user_info']['id'])));	
		}
		else
			$result['error']=__tr('personal.Not the correct code');
				
	}
	die(json_encode($result));
}
//проверка кода и верификация  нового мыла
if(isset($_POST['newemailcreate']))
{
	$result=array('result'=>false);
	$_POST['email']=trim($_POST['email']);
	if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
		$result['error']=__tr('personal.Please input correct email');
	elseif($_POST['email']!=$content['personal_user_info']['email'])
	{
		$exist=$db_connectors->FetchRow('select count(*) as cc from users where email="'.$db_connectors->sql_escape($_POST['email']).'" and id!='.$content['personal_user_info']['id']);
		if($exist['cc']>0)
			$result['error']=__tr('personal.This email address already exists');
		$email=$_POST['email'];	
		$result['result']=true;
		$result['email']=$email;
		
		
		$code=strtoupper(md5($content['personal_user_info']['id'].$content['personal_user_info']['email'].time()));
		$db_connectors->DeleteRow('email_verification_code',array('id_user'=>intval($content['personal_user_info']['id'])));
		$insert=array('id_user'=>intval($content['personal_user_info']['id']),'code'=>$code,'date'=>time(),'email'=>$email,'new'=>1);
		$db_connectors->InsertRow('email_verification_code',$insert);
		
		include 'library/send_mail.php';
		$SEND_MAIL=new SendMail(PATH_TPL.'mail');
		$tp=array('code'=>$code,'ip'=>USER_IP_ADDRESS);
		$data_publish=array('title'=>'Activate account','action'=>'activate_email_new','email'=>$email);
		$SEND_MAIL->send($data_publish,$tp);
		
	}
	else{
		$result['error']=__tr('personal.The new email is equal to the old one');
	}
	die(json_encode($result));
}

if(isset($_POST['emailverifnew']))
{
	$_POST['code_mail']=trim($_POST['code_mail']);
	$email=$_POST['email'];
	$result=array('result'=>false);
	if(isset($_POST['code_mail']))
	{
		
		$us=$db_connectors->FetchRow('select * from email_verification_code where id_user='.intval($content['personal_user_info']['id']).' and code="'.$db_connectors->sql_escape($_POST['code_mail']).'" and email="'.$email.'"');
		if($us)
		{
			
			$res=$API_WALET->change_login(array('token'=>get_tokenUser($API_WALET,$content['personal_user_info']),'login'=>$email));
			if($res['type']=='success'){
				$db_connectors->UpdateRow('users',array('email'=>$email),array('id'=>$content['personal_user_info']['id']));
				$result['result']=true;
				set_verificationUser($db_connectors,$content['personal_user_info']['id'],'email',1);
				
				$user_all_status=get_verificationUser($db_connectors,$content['personal_user_info']['id']);
				if($user_all_status['is_email']==1 and $user_all_status['is_document']==1 and $user_all_status['is_adres']==1 and $user_all_status['is_personal_info']==1 and $content['personal_user_info']['is_verification']==0)
					$db_connectors->UpdateRow('users', array('is_verification'=>1),array('id'=>intval($content['personal_user_info']['id'])));	
			}
			else 
				$result['error']=__tr('personal.This email address already exists');
			
						
		}
		else
			$result['error']=__tr('personal.Not the correct code');
				
	}
	die(json_encode($result));
}
	
if(isset($_POST['verification_type']))
{
	
	$result=array('result'=>false);	
	$arrayInsert=array();
	$info_key=$info_type_key[$_POST['verification_type']]['input'];
	$file_key=$info_type_key[$_POST['verification_type']]['file'];
	
	
	foreach($info_key as $v)
	{
		if(!isset($_POST[$v]))
			$_POST[$v]='';		
		if(trim($_POST[$v])=="")
			$result['error'][$v]=__tr('personal.Please input correct data');
		else $arrayInsert[$v]=$_POST[$v];
	}
	if(count($file_key)>0)
	{
		foreach($file_key as $v)
		{
			if(!isset($_POST[$v]))
				$_POST[$v]='';
			
			if(trim($_POST[$v])=="" and !isset($_POST[$v.'-file']))
				$result['error'][$v]=__tr('personal.Please choose file');
				
		}
	}
	
	if(count($result['error'])==0)
	{
		
		$file_error=false;
		
		if(count($file_key)>0)
		{
			foreach($file_key as $v)
			{
				if(!isset($_POST[$v.'-file']))				
				{ 	
					$name=md5($_POST[$v.'-name'].time().$v.$content['personal_user_info']['id']);
					$img=saveImgFromBase($_POST[$v],$content['personal_user_info']['img_path_save'].'/',$name);
					if(!$img)
					{
						$file_error=true;
						$result['error'][$v]=__tr('personal.Please choose file');
					}
					else
					{
						$array=array('user_name'=>$_POST[$v.'-name'],'file'=>$img);
						$arrayInsert[$v]=json_encode($array);
					}	
				}
				else $file_error=false;
			}
		}
		if(!$file_error)
		{
			$arrayInsert['id_user']=$content['personal_user_info']['id'];
			$arrayInsert['status']=1;
			$arrayInsert['date_update']=time();
			set_verificationUser($db_connectors,$content['personal_user_info']['id'],$_POST['verification_type'],2);
			
			
			if(intval($_POST['verification_id'])==0)
				$db_connectors->InsertRow('verification', $arrayInsert);
			else $db_connectors->UpdateRow('verification', $arrayInsert,array('id'=>intval($_POST['verification_id'])));
			$result['result']=true;
			include 'library/admin_send.php';
			$SEND_ADMIN=new	SendAdmin(PATH_ROOT.'plugin/send-m');
			$SEND_ADMIN->setRequest($content['personal_user_info']['id']);
			
		}
	}
	die(json_encode($result));
		
	
}

$content['title']='Account Confirmation';
$content['h1']='';
/*script_load*/
$content['script_load']=array(
	'/js/personal/account_confirmation.js',
); 
$content['style_load']=array(
	
); 
$content['url_active']='/personal/account-confirmation';	

$data=getUserWallets($API_WALET,$content['personal_user_info']);
$data['is_verification']=get_verificationUser($db_connectors,$content['personal_user_info']['id']);
if($data['is_verification']['is_email']==0)
	set_verificationUser($db_connectors,$content['personal_user_info']['id'],'email',1);
$d=get_data_verif($db_connectors,$content['personal_user_info']['id']);
if($d){
	$data['data']=$d;
	$data['data']['documents_1']=json_decode($data['data']['documents_1'],true);
	$data['data']['documents_2']=json_decode($data['data']['documents_2'],true);
	$data['data']['proff_adres']=json_decode($data['data']['proff_adres'],true);
	$data['data']['selfi']=json_decode($data['data']['selfi'],true);
	if($content['personal_user_info']['is_company'])
	{
		$data['data']['documents_company_1']=json_decode($data['data']['documents_company_1'],true);
		$data['data']['documents_company_2']=json_decode($data['data']['documents_company_2'],true);
	}	
	$data['error']=get_error($db_connectors,$content['personal_user_info']['id']);
}
$data['foto_path']=$content['DOMAIN'].'/user_file/'.$content['personal_user_info']['id'];
$data['info_type_key']=$info_type_key;
$data['list_country']=get_country();
ksort ($data['list_country']);
reset ($data['list_country']);


$data['key_name']=$info_name;
if($content['personal_user_info']['is_company'])
{
	if(!isset($data['data']['name_company']))
		$data['data']['name_company']="";
	if($data['data']['name_company']=="" and $content['personal_user_info']['name']!="")
		$data['data']['name_company']=$content['personal_user_info']['name'];
	$eneble=0;
	if($data['is_verification']['is_company']==1 or $data['is_verification']['is_company']==2)		
		$content['script_code']="
		<script>
			ConfirmationFOXCompany.enableElement();
		</script>
	";	
}
else
{
	if(!isset($data['data']['name']))
		$data['data']['name']="";
	if($data['data']['name']=="" and $content['personal_user_info']['name']!="")
		$data['data']['name']=$content['personal_user_info']['name'];
}


if($data['is_verification']['is_adres']==1 or $data['is_verification']['is_adres']==2)
	$content['script_load'][]='/js/personal/confirmation_enable/confirmation_enable_adres.js';
if($data['is_verification']['is_personal_info']==1 or $data['is_verification']['is_personal_info']==2)
	$content['script_load'][]='/js/personal/confirmation_enable/confirmation_enable_prof.js';



return parserPage('account_confirmation.html',$data,$content);

function get_data_verif($db,$id_user,$id=0)
{
	$sql='select * from verification where id_user='.$id_user;
	if($id>0)
	$sql.=' and id='.$id;	
	return $db->FetchRow($sql);	
}
function get_error($db,$id_user)
{
	$true=array();
	$error_=$db->FetchRow('select * from verification_error where id_user='.$id_user);
	if($error_)
	{
		foreach($error_ as $k=>$v)
		{
			if($v==1)
				$true[$k]=$v;
		}
	}
	$true['comment']=$error_['comment'];
	
	return $true;
	
}
function get_country()
{
	$file=PATH_ROOT.'/plugin/country/list.json';
	return json_decode(file_get_contents($file),true);
}
