<?php
AdminUrl($content['personal_user_info'],array(1,2,4));
if(isset($_POST['id_user']))
{
	$error=false;
	$res=array('result'=>false);
	if($_POST['bonus']=="")
		$error='Please input bonus';
	if(@intval($_POST['bonus'])<=0)
		$error='Please input bonus >0';
	if(!$error)
	{
		$bonus=@intval($_POST['bonus'])+$_POST['old_bonus'];
		$cc=$db_connectors->CountRow('referal_point', 'id', array('id_user'=>$_POST['id_user']));
		if($cc==0)
			$db_connectors->InsertRow('referal_point',array('id_user'=>$_POST['id_user'],'point'=>$bonus));
		else $db_connectors->UpdateRow('referal_point',array('point'=>$bonus),array('id_user'=>$_POST['id_user']));
		$db_connectors->InsertRow('affilate_stakes',array('id_user'=>$_POST['id_user'],'stakes'=>$_POST['bonus'],'date'=>time(),'cheking_user'=>$content['personal_user_info']['id']));
		
		$res['result']=true;
		$res['bonus']=$bonus;
	}
	else
		$res['error']=$error;	
	die(json_encode($res));
	
}

$content['LAYOUT']='admin/layout.html';
$content['title']='Administration::Users Affiliate';
$content['h1']='';
/*script_load*/
$content['script_load']=array(
	
	'/js/admin/affilate.js?newVersion=555',	
); 
$content['style_load']=array(
	
	
); 
$content['url_active']='/admin/affiliate';

$data=array();

if(!isset($_GET['ID_USER']))
{
	$data['link_get']='';
	$link_get=array();
	$page=1;
	if(isset($_GET['page']))
	{
		$page=intval($_GET['page']);
		if($page==0)
			$page=1;	
	}

	
	$data['filter']=array('link_'=>'','count'=>array('new'=>0,'all'=>0));
	
	if(count($link_get)>0)
		$data['link_get']='?'.implode('&',$link_get);
	else $data['link_get']='?';
	
	$data['obj']=get_data($db_connectors,$page);
	
	$data['template']='list';	
	
	return parserPage('admin/affilate.html',$data);
}
else
{
	
	$data['users_list']=get_data_users($db_connectors,$_GET['ID_USER']);
	$data['users_info']=get_data_id($db_connectors,$_GET['ID_USER']);
	$data['bonus']=get_point($db_connectors,$_GET['ID_USER']);
	$data['total_transaction']=get_my_users_transaction($db_connectors,$_GET['ID_USER'],$API_WALET);
	
	//var_dump($data['total_transaction']);
	$data['template']='item';
		
	$data['header_info']='user # '.$_GET['ID_USER'];
	$is_v=get_status($db_connectors,$_GET['ID_USER']);
	if($is_v)
		$data['header_info'].=' Profile is <span style="color:green;">verificated</span>';
	else
		$data['header_info'].=' Profile is <span style="color:red;">not verificated</span>';
	
	
	return parserPage('admin/affilate.html',$data);
}



function get_data($db,$page=1)
{
	$limit_row=20;
	
	$cc=$db->FetchRow('select count(distinct(id_referal_user)) as cc from referal_user');
	$count=$cc['cc'];
	$data=array('count_row'=>0,'list'=>array(),'count_page'=>0,'active_page'=>$page,'all_counts'=>$count,'limit_row'=>$limit_row);
	$limit='';
	if($page>0)
	{
		$limit_=($page-1)*$limit_row;
		$limit=' limit '.$limit_.','.$limit_row;
		
	}	
	if($count>0)
		$data['count_page']=ceil($count/$limit_row);
	
	
	$sql='SELECT referal_user.id_referal_user,users.email,users.name,count(*) as c_us FROM `referal_user`,users where users.id=referal_user.id_referal_user group by referal_user.id_referal_user '.$limit;	
	$data['list']=array();
	$data_sql=$db->Query($sql);
	if($data_sql)
	{
		while ($row = $data_sql->fetch_assoc()) {
			$row['point']=get_point($db,$row['id_referal_user']);			
			$data['list'][] = $row;
		}
	}
	
	
	$data['count_row']=count($data['list']);
	return $data;
}

function get_data_id($db,$id)
{
	$sql='SELECT referal_user.id_referal_user,users.email,users.name FROM `referal_user`,users where users.id=referal_user.id_referal_user and referal_user.id_referal_user='.$id;
	
	return $db->FetchRow($sql);
}
function get_data_users($db,$id)
{
	$sql='SELECT referal_user.id_user,users.email,users.fb_id,users.name FROM `referal_user`,users where users.id=referal_user.id_user and referal_user.id_referal_user='.$id;
	return $db->FetchAll($sql);
}
function get_my_users_transaction($db,$id_user,$API_WALET)
{
	$return=array('count_t'=>0,'count_sum'=>0,'user_list'=>array(),'user_info'=>array());
	$user=array();
	$sql='select IF(users.email="",users.fb_id,users.email) as login,users.id,users.is_verification from referal_user,users where users.id=referal_user.id_user and referal_user.id_referal_user='.$id_user;
	$res_ = $db->Query($sql);
	if ($res_)
	{
		while ($row = $res_->fetch_assoc()) {
			$user[] = 'accounts[]='.$row['login'];
			$return['user_info'][$row['login']] = $row;
		}
	}
	
	
	//var_dump($user);
	
	
	$send=array('token'=>get_tokenUser($API_WALET,$content['personal_user_info']),'accounts'=>$user);
	
	$res=$API_WALET->affilate_transaction_user($send);
	
	if($res['type']=="success" and isset($res['answer']['accounts']) and count($res['answer']['accounts'])>0)
	{
		foreach($res['answer']['accounts'] as $k=>$acc)
		{
			
			$return['user_list'][$k]=$acc;
			$return['count_t']=$return['count_t']+$acc['total_cnt'];
			$return['count_sum']=$return['count_sum']+$acc['total_sum'];
		}
	}
	
	return $return;
}  
function get_point($db,$id_user)
{
	$s=$db->FetchRow('select point from referal_point where id_user='.$id_user);
	if($s)
		return $s['point'];
	return 0;
}
function get_status($db,$id_user,$row_status='is_verification')
{
	$error_=$db->FetchRow('select '.$row_status.' from users where id='.$id_user);
	return $error_[$row_status];
}
