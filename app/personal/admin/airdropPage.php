<?php
AdminUrl($content['personal_user_info'],array(1,2,4));
$info_key=array('fb_link','youtube_link','twitter_link','telegram_link','rebit_link','medium_link','instagram_link','comment_1','comment_2','friends_name');
$info_key_name=array(
    'fb_link'=>'Follow us on Facebook, share this post and tag 2 friends there. Provide your profile URL****',
    'telegram_link'=>'Join our telegram group and provide your username',
    'twitter_link'=>'Follow us on Twitter, share this tweet, tag 2 friends and provide your profile URL',
    'youtube_link'=>'Subscribe to us on Youtube, comment the video and provide your profile URL',
    'rebit_link'=>'Follow us on Reddit, upvote one post and provide your profile URL',
    'medium_link'=>'Follow our blog channel on Medium',
    'instagram_link'=>'Follow us on Instagram and provide your username',
    'comment_1'=>'Leave a comment about UniFox on Bitcointalk ANN',
    //'comment_2'=>'Leave a comment about UniFox on Bitcointalk Bounty ANN',
    'friends_name'=>'Add 3 friends to official Telegram group and provide their usernames',
    );
$key_type=array(
	'Facebook'=>array('fb_link'),
	'Telegram'=>array('telegram_link','friends_name'),
	'Twitter'=>array('twitter_link'),
	'Youtube'=>array('youtube_link'),
	'Reddit'=>array('rebit_link'),
	'Medium'=>array('medium_link'),
	'Instagram'=>array('instagram_link'),
	//'Bitcointalk'=>array('comment_1','comment_2'),
	//'Bitcointalk'=>array('comment_1'),
	
	);	

if(isset($_POST['airpod_id']))
{
	//var_dump($_POST);die();
	
	
	$status=array();
	$error=0;
	$post=$key_type[$_POST['type']];
	
	foreach($post as $v)
	{
		if($_POST[$v.'_confirmation']==0)
		{
			$status[$v]=0;
			$error++;
		}	
		else $status[$v]=1;
	}
	
	
	$status['id_user']=$_POST['id_user'];
	
	$d=$db_connectors->FetchRow('select * from airpod_error where id_user='.$_POST['id_user']);
	if($d)
		$db_connectors->UpdateRow('airpod_error', $status,array('id'=>intval($d['id'])));
	else $db_connectors->InsertRow('airpod_error', $status);
	
	if($error>0)
		$profile=array('status'=>2);
	
	if($error==0)
		$profile=array('status'=>3);
	$profile['checking_user']=$content['personal_user_info']['id'];
	
	$db_connectors->UpdateRow('airpod', $profile,array('id'=>intval($_POST['airpod_id'])));
	if(get_status_airdrop($db_connectors,$_POST['id_user'],$key_type))
		$is_airdrop=1;
	else $is_airdrop=0;
	$db_connectors->UpdateRow('users', array('is_airdrop'=>$is_airdrop),array('id'=>intval($_POST['id_user'])));
	header('Location: /admin/airdrop/?ID='.$_POST['airpod_id']);
	die();
}



$content['LAYOUT']='admin/layout.html';
$content['title']='Administration::Users Airdrop request';
$content['h1']='';
/*script_load*/
$content['script_load']=array(
	'/css/personal/plugins/icheck/icheck.min.js',	
); 
$content['style_load']=array(
	"/css/personal/plugins/icheck/skins/all.css",
	
); 
$content['url_active']='/admin/airdrop';

$data=array();

if(!isset($_GET['ID']))
{
	$data['link_get']='';
	$link_get=array();
	$page=1;
	if(isset($_GET['page']))
	{
		$page=intval($_GET['page']);
		if($page==0)
			$page=1;	
	}

	$new=false;
	$data['filter']=array('link_'=>'','count'=>array('new'=>0,'all'=>0));
	if(isset($_GET['new']))	
	{	
		$new=true;
		$link_get[]='new=1';
		$data['filter']['link_']='new';
	}
	if(count($link_get)>0)
		$data['link_get']='?'.implode('&',$link_get);
	else $data['link_get']='?';


	$data['obj']=get_data($db_connectors,$new,$page);
	if($new)
	{
		$data['filter']['count']['new']=$data['obj']['all_counts'];
		$data['filter']['count']['all']=get_count($db_connectors,'airpod');
	}
	else
	{
		$data['filter']['count']['new']=get_count($db_connectors,'airpod',array('status'=>1));
		$data['filter']['count']['all']=$data['obj']['all_counts'];
	}
	$data['template']='list';	
}
else
{
	$data['filter']=array('link_'=>'','count'=>array('new'=>0,'all'=>0));
	$data['filter']['count']['new']=get_count($db_connectors,'airpod',array('status'=>1));
	$data['filter']['count']['all']=get_count($db_connectors,'airpod');
	$data['data']=	get_data_id($db_connectors,$_GET['ID']);
	
	
	$data['error']=get_error($db_connectors,$data['data']['id_user']);
	$data['is_profile']=get_status($db_connectors,$data['data']['id_user']);
	
	
	$data['info_key']=$info_key;
	$data['info_key_name']=$info_key_name;
	
	$data['template']='item';	
	$data['img_path']=$content['DOMAIN'].'/user_file/'.$data['data']['id_user'].'/';	
	
	
	
	$data['is_airdrop']=get_status($db_connectors,$data['data']['id_user'],'is_airdrop');
	$data['header_info']='user # '.$data['data']['id_user'];
	if($data['is_profile'])
		$data['header_info'].=' Profile is <span style="color:green;">verificated</span>';
	else
		$data['header_info'].=' Profile is <span style="color:red;">not verificated</span>';
	$data['list_type']=$key_type;
	$data['list_type_status']=array();
	foreach($key_type as $t=>$k)
	{
		$data['list_type_status'][$t]=0;
		$error=0;
		$process=0;
		foreach($k as $row)
		{
			if(isset($data['error'][$row]))
			{
				if($data['error'][$row]==0)
					$error++;
				if($data['error'][$row]==2)
					$process++;
			}
			else $process++;
			
		}
		if($error==0)
		{
			if($process==0)
				$data['list_type_status'][$t]=1;
			else $data['list_type_status'][$t]=2;
		}		
	}
	
	
}

return parserPage('admin/airdrop.html',$data);

function get_data($db,$new=false,$page=1)
{
	$limit_row=20;
	
	$where=array();
	$new_sql='';
	if($new)
	{
		$where['status']=1;
		$new_sql=' where status=1';
	}
	$count=get_count($db,'airpod',$where);
	$data=array('count_row'=>0,'list'=>array(),'count_page'=>0,'active_page'=>$page,'all_counts'=>$count,'limit_row'=>$limit_row);
	$limit='';
	if($page>0)
	{
		$limit_=($page-1)*$limit_row;
		$limit=' limit '.$limit_.','.$limit_row;
		
	}	
	if($count>0)
		$data['count_page']=ceil($count/$limit_row);
	if($new_sql=='')
		$sql_user='where users.id=airpod.id_user';
	else $sql_user=' and users.id=airpod.id_user';
	$sql='select airpod.id,airpod.date_update,airpod.id_user,users.name,users.email from airpod,users '.$new_sql.$sql_user.' order by date_update desc'.$limit;	
	
	$data['list']=$db->FetchAll($sql);
	$data['count_row']=count($data['list']);
	return $data;
}

function get_data_id($db,$id)
{
	$sql='select * from airpod where id='.$id;
	return $db->FetchRow($sql);
}
function get_error($db,$id_user)
{
	$true=array();
	$error_=$db->FetchRow('select * from airpod_error where id_user='.$id_user);
	if($error_)
	{
		foreach($error_ as $k=>$v)
		{
			if($v!="")
				$true[$k]=$v;
		}
	}
	
	return $true;	
	
}
function get_count($db,$table,$where=array())
{
	return $db->CountRow($table, 'id', $where);
}
function get_status($db,$id_user,$row_status='is_verification')
{
	$error_=$db->FetchRow('select '.$row_status.' from users where id='.$id_user);
	return $error_[$row_status];
}
function get_status_airdrop($db,$id_user,$key_p)
{
	$true=array();
	$error_=$db->FetchRow('select * from airpod_error where id_user='.$id_user);
	if($error_)
	{
		foreach($error_ as $k=>$v)
		{
			if($v==1)
				$true[$k]=$v;
		}
	}
	$confirm=array();
	foreach($key_p as $k=>$n)
	{
		$confirm[$k]=false;
	}	
	foreach($key_p as $k=>$val)
	{
		$confirm[$k]=false;
		$error=0;
		foreach($val as $r)
		{
			if(!isset($true[$r]))
				$error++;
		}
		if($error==0)
			$confirm[$k]=true;	
	}
	
	foreach($confirm as $t)
	{
		if($t)
			return true;		
	}
	return false;
	//die();
}

?>