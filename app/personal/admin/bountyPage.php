<?php
AdminUrl($content['personal_user_info'],array(1,2,4));
$social_=array('Facebook','Youtube','Twitter','Telegram');
$status_=array(1=>'New',2=>'Declined',3=>'Confirm');
if(isset($_POST['bounty_id']))
{
	$result=array('result'=>false);
	$status=3;
	if(isset($_GET['status']))
		$status=$_GET['status'];
	$error=false;
	$upd=array('status'=>$status);
	$_POST['stakes']=intval($_POST['stakes']);
	if($_GET['status']==3 and $_POST['stakes']<=0)
		$error='Please input stakes';
	
	
	if(!$error)
	{
		$upd['stakes']=$_POST['stakes'];
		$upd['date_update']=time();
		$db_connectors->UpdateRow('bounty_program', $upd,array('id'=>intval($_POST['bounty_id'])));
		$result['result']=true;
		$result['id']=$_POST['bounty_id'];
		$result['status_text']=$status_[$upd['status']];
		$result['status_id']=$upd['status'];		
	}
	else
		$result['error']=$error;		
	
	
	die(json_encode($result));
}



$content['LAYOUT']='admin/layout.html';
$content['title']='Administration::Users Bounty tasks';
$content['h1']='';
/*script_load*/
$content['script_load']=array(
	'/css/personal/plugins/icheck/icheck.min.js',	
	'/js/admin/bounty.js',	
); 
$content['style_load']=array(
	"/css/personal/plugins/icheck/skins/all.css",
	
); 
$content['url_active']='/admin/bounty';

$data=array();

if(!isset($_GET['ID']))
{
	$data['link_get']='';
	$link_get=array();
	$page=1;
	if(isset($_GET['page']))
	{
		$page=intval($_GET['page']);
		if($page==0)
			$page=1;	
	}

	$new=false;
	$data['filter']=array('link_'=>'','count'=>array('new'=>0,'all'=>0));
	if(isset($_GET['new']))	
	{	
		$new=true;
		$link_get[]='new=1';
		$data['filter']['link_']='new';
	}
	if(count($link_get)>0)
		$data['link_get']='?'.implode('&',$link_get);
	else $data['link_get']='?';

	$data['row']=get_data_socials($db_connectors,$social_);
	$data['obj']=get_data($db_connectors,$new,$page);
	if($new)
	{
		$data['filter']['count']['new']=$data['obj']['all_counts'];
		$data['filter']['count']['all']=get_count($db_connectors,'bounty_program');
	}
	else
	{
		$data['filter']['count']['new']=get_count($db_connectors,'bounty_program',array('status'=>1));
		$data['filter']['count']['all']=$data['obj']['all_counts'];
	}
	$data['template']='list';	
	$data['status_list']=$status_;	
	return parserPage('admin/bounty.html',$data);
}
else
{
	$res=array('result'=>false);	
	$data=array();
	$data['data']=	get_data_id($db_connectors,$_GET['ID']);
	$row=get_data_socials($db_connectors,array($data['data']['type']));

	$data['row']=$row[$data['data']['type']][$data['data']['row_key']];
	$res['html']=parserPage('admin/bounty/item.html',$data);
	$res['title']='Task #'.$data['data']['id'];
	die(json_encode($res));
}



function get_data($db,$new=false,$page=1)
{
	$limit_row=20;
	
	$where=array();
	$new_sql='';
	if($new)
	{
		$where['status']=1;
		$new_sql=' where status=1';
	}
	$count=get_count($db,'bounty_program',$where);
	$data=array('count_row'=>0,'list'=>array(),'count_page'=>0,'active_page'=>$page,'all_counts'=>$count,'limit_row'=>$limit_row);
	$limit='';
	if($page>0)
	{
		$limit_=($page-1)*$limit_row;
		$limit=' limit '.$limit_.','.$limit_row;
		
	}	
	if($count>0)
		$data['count_page']=ceil($count/$limit_row);
	if($new_sql=='')
		$sql_user='where users.id=bounty_program.id_user';
	else $sql_user=' and users.id=bounty_program.id_user';
	$sql='select bounty_program.id,bounty_program.date_update,bounty_program.row_key,bounty_program.type,bounty_program.status,bounty_program.id_user,users.name,users.email from bounty_program,users '.$new_sql.$sql_user.' order by date_update desc'.$limit;	
	
	$data['list']=$db->FetchAll($sql);
	$data['count_row']=count($data['list']);
	return $data;
}

function get_data_id($db,$id)
{
	$sql='select * from bounty_program where id='.$id;
	return $db->FetchRow($sql);
}
function get_data_socials($db,$array_socials)
{
	$data=array();
	
	foreach($array_socials as $v)
	{
		$data[$v]=array();
		$d=$db->FetchAll('select row_key,name,comment,stakes from bounty_list where type="'.$v.'"');
		if($d)
		{
			foreach($d as $task)
			{
				$data[$v][$task['row_key']]=$task;
			}
			
		}	
	}
	
	return $data;
}
function get_count($db,$table,$where=array())
{
	return $db->CountRow($table, 'id', $where);
}


?>