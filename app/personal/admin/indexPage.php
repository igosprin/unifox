<?php
AdminUrl($content['personal_user_info'],array(1,3,4));

$info_type_key=array(
	/*'adres'=>array(
					'input'=>array('country','city','adres'),
					'file'=>array('proff_adres'),
				),*/
	'document'=>array(
					'input'=>array(),
					'file'=>array('documents_1','documents_2','selfi'),
				),
	'personal_info'=>array(
					'input'=>array('name','birthdate'),
					'file'=>array(),
				),
	'company_info'=>array(
					'input'=>array(
						'name_company','adress_company','vat_number'
					),
					'file'=>array('documents_company_1','documents_company_2'),
				)			
);

/*$file_key=array('documents_1','documents_2','proff_adres','selfi');
$info_key=array('last_name','name','middle_name','birthdate','citizenship','number','issue_date','expaer_date','country','city','adres');*/
$info_key_name=array('last_name'=>'Last name','name'=>'Name','middle_name'=>'Middle name','birthdate'=>'Birthdate','citizenship'=>'Citizenship','number'=>'Id number','issue_date'=>'Id issue date','expaer_date'=>'Id expire date','country'=>'Country','city'=>'City','adres'=>'Address','documents_1'=>'Identity verifications 1','documents_2'=>'Identity verifications 2','proff_adres'=>'Proof of address','selfi'=>'Selfi width a passport/ID card in hand','name_company'=>'Company name','adress_company'=>'Company address','vat_number'=>'Company vat number','documents_company_1'=>'Certificate of incorporation','documents_company_2'=>'Power of attorney');

if(isset($_POST['verification_type']))
{
	
	$status=array();
	$error=0;
	$info_key=$info_type_key[$_POST['verification_type']]['input'];
	$file_key=$info_type_key[$_POST['verification_type']]['file'];
	foreach($info_key as $v)
	{
		if($_POST[$v.'_confirmation']==0)
		{
			$status[$v]=0;
			$error++;
		}	
		else $status[$v]=1;	
	}
	foreach($file_key as $v)
	{
		if($_POST[$v.'_confirmation']==0)
		{
			$status[$v]=0;
			$error++;
		}
		else $status[$v]=1;	
	}
	/*if(count($error)>0)	
		$status['comment']=$_POST['comment'];
	else */
	$status['comment']='';
	$status['id_user']=$_POST['id_user'];
	
	$d=$db_connectors->FetchRow('select * from verification_error where id_user='.$_POST['id_user']);
	if($d)
		$db_connectors->UpdateRow('verification_error', $status,array('id'=>intval($d['id'])));
	else $db_connectors->InsertRow('verification_error', $status);
	
	
	if($error>0){
		$profile=array('status'=>2);
		$type_status=3;			
	}
	else{
			$profile=array('status'=>3);
			$type_status=1;	
	}
	set_verificationUser($db_connectors,$_POST['id_user'],$_POST['verification_type'],$type_status);		
	$profile['checking_user']=$content['personal_user_info']['id'];
	
	$db_connectors->UpdateRow('verification', $profile,array('id'=>intval($_POST['verification_id'])));
	//is_email 	is_document 	is_adres 	is_personal_info 
	
	$user_all_status=get_verificationUser($db_connectors,$_POST['id_user']);
	
	$is_verification=0;
	$user_info=get_data_userid($db_connectors,$_POST['id_user']);
	if($user_info['is_company'])
	{
		if($user_all_status['is_email']==1 and $user_all_status['is_document']==1 and $user_all_status['is_personal_info']==1 and $user_all_status['is_company_info']==1)
			$is_verification=1;
	}
	else
	{
		if($user_all_status['is_email']==1 and $user_all_status['is_document']==1 and $user_all_status['is_personal_info']==1)
			$is_verification=1;
	}
	
	$db_connectors->UpdateRow('users', array('is_verification'=>$is_verification),array('id'=>intval($_POST['id_user'])));
	
	
	
	/*if($_POST['verification_type']=='personal_info' and $type_status==1)
	{
		
	}*/	
	
	header('Location: /admin?ID_VERIFICATION='.$_POST['verification_id']);
	die();
}



$content['LAYOUT']='admin/layout.html';
$content['title']='Administration::Users Profile request';
$content['h1']='';
/*script_load*/
$content['script_load']=array(
	'/css/personal/plugins/icheck/icheck.min.js',	
); 
$content['style_load']=array(
	"/css/personal/plugins/icheck/skins/all.css",
	
); 
$content['url_active']='/admin';

$data=array();

if(isset($_GET['ID_USER']))
{
	$d=$db_connectors->FetchRow('select id from verification where id_user='.intval($_GET['ID_USER']));
	if($d)
	{
		$_GET['ID_VERIFICATION']=$d['id'];
	}
	else
	{
		header('Location: /admin/');die();
	}
}	


if(!isset($_GET['ID_VERIFICATION']))
{
	$data['link_get']='';
	$link_get=array();
	$page=1;
	if(isset($_GET['page']))
	{
		$page=intval($_GET['page']);
		if($page==0)
			$page=1;	
	}

	$new=false;
	$data['filter']=array('link_'=>'','count'=>array('new'=>0,'all'=>0));
	if(isset($_GET['new']))	
	{	
		$new=true;
		$link_get[]='new=1';
		$data['filter']['link_']='new';
	}
	if(count($link_get)>0)
		$data['link_get']='?'.implode('&',$link_get);
	else $data['link_get']='?';


	$data['obj']=get_data_verif($db_connectors,$new,$page);
	if($new)
	{
		$data['filter']['count']['new']=$data['obj']['all_counts'];
		$data['filter']['count']['all']=get_count($db_connectors,'verification');
	}
	else
	{
		$data['filter']['count']['new']=get_count($db_connectors,'verification',array('status'=>1));
		$data['filter']['count']['all']=$data['obj']['all_counts'];
	}
	$data['template']='list';	
}
else
{
	
	
	$data['filter']=array('link_'=>'','count'=>array('new'=>0,'all'=>0));
	$data['filter']['count']['new']=get_count($db_connectors,'verification',array('status'=>1));
	$data['filter']['count']['all']=get_count($db_connectors,'verification');
	$data['data']=	get_data_id($db_connectors,$_GET['ID_VERIFICATION']);
	$user_info=get_data_userid($db_connectors,$data['data']['id_user']);
	$data['data']['documents_1']=json_decode($data['data']['documents_1'],true);
	$data['data']['documents_2']=json_decode($data['data']['documents_2'],true);
	$data['data']['proff_adres']=json_decode($data['data']['proff_adres'],true);
	$data['data']['selfi']=json_decode($data['data']['selfi'],true);
	
	if($user_info['is_company'])
	{
		$data['data']['documents_company_1']=json_decode($data['data']['documents_company_1'],true);
		$data['data']['documents_company_2']=json_decode($data['data']['documents_company_2'],true);
		
		
	}
	$data['error']=get_error($db_connectors,$data['data']['id_user']);
	
	//$data['file_key']=$file_key;
	$data['info_key_list']=$info_type_key;
	$data['info_key_name']=$info_key_name;
	$data['is_verification']=get_verificationUser($db_connectors,$data['data']['id_user']);
	$data['template']='item';	
	$data['img_path']=$content['DOMAIN'].'/user_file/'.$data['data']['id_user'].'/';
	$m=$db_connectors->FetchRow('select is_verification from users where id='.$data['data']['id_user']);
	$data['this_v_status']=$m['is_verification'];	
	$data['user_info']=$user_info;	
	$data['header_info']='user # '.$data['data']['id_user'];
	if($data['this_v_status'])
		$data['header_info'].=' Profile is <span style="color:green;">verificated</span>';
	else
		$data['header_info'].=' Profile is <span style="color:red;">not verificated</span>';
}

return parserPage('admin/index.html',$data);

function get_data_verif($db,$new=false,$page=1)
{
	$limit_row=20;
	
	$where=array();
	$new_sql='';
	if($new)
	{
		$where['status']=1;
		$new_sql=' where status=1';
	}
	$count=get_count($db,'verification',$where);
	$data=array('count_row'=>0,'list'=>array(),'count_page'=>0,'active_page'=>$page,'all_counts'=>$count,'limit_row'=>$limit_row);
	$limit='';
	if($page>0)
	{
		$limit_=($page-1)*$limit_row;
		$limit=' limit '.$limit_.','.$limit_row;
		
	}	
	if($count>0)
		$data['count_page']=ceil($count/$limit_row);
	if($new_sql=='')
		$sql_user='where users.id=verification.id_user';
	else $sql_user=' and users.id=verification.id_user';
	$sql='select verification.id,verification.date_update,verification.id_user,users.name,users.is_verification,users.email from verification,users '.$new_sql.$sql_user.' order by date_update desc'.$limit;	
	
	$data['list']=$db->FetchAll($sql);
	$data['count_row']=count($data['list']);
	return $data;
}

function get_data_id($db,$id)
{
	$sql='select * from verification where id='.$id;
	return $db->FetchRow($sql);
}
function get_error($db,$id_user)
{
	$true=array();
	$error_=$db->FetchRow('select * from verification_error where id_user='.$id_user);
	if($error_)
	{
		foreach($error_ as $k=>$v)
		{
			if($v==1)
				$true[$k]=$v;
		}
	}
	//$true['comment']=$error_['comment'];
	return $true;	
	
}
function get_count($db,$table,$where=array())
{
	return $db->CountRow($table, 'id', $where);
}
function get_data_userid($db,$id)
{
	return $db->FetchRow('select email,is_company from users where id='.$id);
}

?>