<?php
AdminUrl($content['personal_user_info'],array(1));
$content['LAYOUT']='admin/layout.html';
$wallet_my=getUserWallets($API_WALET,$content['personal_user_info']);

if(isset($_GET['action']))
{
	$result=array('result'=>false);
	switch($_GET['action'])
	{
		case 'form_send':			
			$data=array();
			$data['id_r']=$_POST['id'];
			$data['wallet_to']=$_POST['wallet_fox'];
			$data['amount']=$_POST['amount_fox'];	
			$data['array_w']=false;	
						
			if(count($wallet_my['fox']['wallets'])>1)
			{
				$data['array_w']=true;
				$data['wallets']=$wallet_my['fox']['wallets'];
			}
			else
			{
				$fox=array_keys(@$wallet_my['fox']['wallets']);			
				$data['wallets']=$wallet_my['fox']['wallets'][$fox[0]];
			}				
			$result['html']=parserPage('admin/request/send_form.html',$data);
			$result['result']=true;
			$result['title']='Send '.CURENCY;
			
						
		break;
		case 'send_fox':
			$send=array();
			if(isset($_POST['wallet2'])  and isset($_POST['value']))
			{				
				if($_POST['wallet2']=='')
					$result['error']['wallet2']='Please input correct wallet';
				if($_POST['wallet']=='')
					$result['error']['wallet']='Please input or select your wallet';
				$_POST['value']=str_replace(',','.',$_POST['value']);
				if($_POST['value']<=0)
					$result['error']['value']='Please input correct amount';
				if(count($result['error'])==0)
				{
					$send['wallet']=array('from'=>$_POST['wallet'],'to'=>$_POST['wallet2']);
					$send['amount']=$_POST['value'];					
				}			
			}			
			if(count($send)>0)
			{
				$send['token']=get_tokenUser($API_WALET,$content['personal_user_info']);
				$res=$API_WALET->send_currency_get($send);
				$result['dd']=$res;
				if(isset($res['answer']) and $res['type']=='success')
				{
					$result['txhash']=$res['answer']['txhash'];
					$result['result']=true;
					deleteCache('wallets_list',$content['personal_user_info']);					
				}
				else
					$result['error']['wallet']='There is no money on your wallet';			
				
			}	
				
		break;		
	}
	die(json_encode($result));
}



$content['title']='Administration::Request Buy FOX';
$content['url_active']='/admin/requestbuy';
$content['script_load']=array(	
		'js/admin/request.js',
	);
$data['link_get']='';

$link_get=array();
$page=1;
if(isset($_GET['page']))
{
	$page=intval($_GET['page']);
	if($page==0)
		$page=1;	
}
if(count($link_get)>0)
	$data['link_get']='?'.implode('&',$link_get);
else $data['link_get']='?';

$data['request']=get_request($API_WALET,$page);
$data['status']=array(
	0=>'Create request',
	1=>'Pending payment',
	2=>'Paid, waiting for fox translation',
	3=>'Completed',
);

$data['DB']=$db_connectors;
return parserPage('admin/request_buy.html',$data);

function get_request($API_WALET,$page=1)
{
	$limit=20;
	$data=array('count_page'=>1,'active_page'=>$page,'all_counts'=>1,'list'=>array());
	$send=array('token'=>get_tokenUser($API_WALET,$content['personal_user_info']));
	$send['page']=($page-1);
	$send['limit']=$limit;
	
	$res=$API_WALET->get_request_buy_all($send);
	if($res['type']=="success")
	{
		$data['list']=$res['answer']['requests'];
		//var_dump($res['answer']);die();
		$data['count_page']=ceil($res['answer']['requests_count']/$limit);
		$data['all_counts']=$res['answer']['requests_count'];		
	}	
	
	return $data;
}