<?php
AdminUrl($content['personal_user_info'],array(1));
// редактирование
if($_POST) {

    $id = (int)$_POST['id'];
    $data = array(
        'name' => $_POST['name'],
        'photo' => $_POST['photo'],
        'linkedin' => $_POST['linkedin'],
    );

    $db_connectors->UpdateRow('team', $data, $id);


    // языки en
    $data = array(
        'job' => $_POST['job_en'],
        'comment' => $_POST['comment_en'],

    );

    $db_connectors->UpdateRow('team_lang', $data, ['team_id' => $id, 'lang' => 'en']);

    // языки cz
    $data = array(
        'job' => $_POST['job_cz'],
        'comment' => $_POST['comment_cz'],

    );

    $db_connectors->UpdateRow('team_lang', $data, ['team_id' => $id, 'lang' => 'cz']);

    // языки ru
    $data = array(
        'job' => $_POST['job_ru'],
        'comment' => $_POST['comment_ru'],

    );

    $db_connectors->UpdateRow('team_lang', $data, ['team_id' => $id, 'lang' => 'ru']);

    header('Location: ' . $_SERVER['HTTP_REFERER']);
    die();


}
$content['LAYOUT']='admin/layout.html';


$data = array();


$data['persons'] = $db_connectors->FetchAll('select * from `team` ');

foreach($data['persons'] AS &$person) {

    $person['en'] = $db_connectors->FetchRow("select `job`, `comment` from `team_lang` WHERE `team_id` = {$person['id']} AND `lang` = 'en' ");
    $person['ru'] = $db_connectors->FetchRow("select `job`, `comment` from `team_lang` WHERE `team_id` = {$person['id']} AND `lang` = 'ru' ");
    $person['cz'] = $db_connectors->FetchRow("select `job`, `comment` from `team_lang` WHERE `team_id` = {$person['id']} AND `lang` = 'cz' ");


}

return parserPage('admin/team.html',$data);



?>



