<?php
AdminUrl($content['personal_user_info'],array(1));
if(isset($_GET['action']))
{
	if($_GET['action']=='activ_user' and $_POST['id']>0)
	{
		$r=array();
		$r['title']='Last 10 visits';
		$a=get_act_data($db_connectors,$_POST['id']);
		$r['html']=parserPage('admin/users/visits.html',$a);
		die(json_encode($r));
	}
	if($_GET['action']=='wallets' and $_POST['id']>0)
	{
		$user_data=$db_connectors->FetchRow('select email,password_api,fb_id from users where id='.$_POST['id']);		
		$r=array();
		$r['title']='Users wallets';
		$d=array();
		$d=$user_data;
		
		$token=$API_WALET->get_user($API_WALET->pars_data_user($user_data),true);
		$wallets=$API_WALET->get_user_balnace(array('token'=>$token));
		
		$user_data['wallets']=$wallets['wallets'];
		$r['html']=parserPage('admin/users/wallets.html',$user_data);
		die(json_encode($r));
	}
	if($_GET['action']=='create_list')
	{
		$res_ =$db_connectors->Query('select email from users where email!=""');
		$file_name='user_list_'.date('d-m-Y').'.csv';
		if ($res_)
		{
			file_put_contents(PATH_TMP_FILE.'/'.$file_name,'');
			chmod(PATH_TMP_FILE.'/'.$file_name,0777);		
			$f=fopen(PATH_TMP_FILE.'/'.$file_name,'a');
			while ($row = $res_->fetch_assoc()) {
				fwrite($f, $row['email']."\n");
			}
			fclose($f);	
			if (ob_get_level()) 
				ob_end_clean();
			$file=PATH_TMP_FILE.'/'.$file_name;
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=' . basename($file));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));		
			readfile($file);
			die();

			
		}
	}
}	




$content['LAYOUT']='admin/layout.html';
$content['title']='Administration::Users Affiliate';
$content['url_active']='/admin/users';
$data['link_get']='';
$link_get=array();
$page=1;
if(isset($_GET['page']))
{
	$page=intval($_GET['page']);
	if($page==0)
		$page=1;	
}

$content['script_load']=array(
	'/js/admin/user.js?new=34',	
);
$search=false;
$data['searcher']=false;
if(isset($_GET['search']) and trim($_GET['search']!=''))
{
	$link_get[]='search='.$_GET['search'];
	
	$type_s='email';
	if(isset($_GET['type']))
	{
		switch($_GET['type'])
		{
			case 'id':
				$type_s	='id';
				$search=' id='.intval($_GET['search']);
				$link_get[]='type='.$_GET['type'];
				
			break;
			case 'email':
				$type_s	='id';
				$search=" email LIKE '%".$db_connectors->sql_escape($_GET['search'])."%'";
				$link_get[]='type='.$_GET['type'];
					
			break;
			default:
				$type_s	='id';
				$search=" email LIKE '".$db_connectors->sql_escape($_GET['search'])."'";
				$link_get[]='type='.$_GET['type'];
				
			break;
		}
		if($search)
			$data['searcher']=array('type'=>$_GET['type'],'search'=>$_GET['search']);		
	}
}
	
$data['searcher_type']=array('email','id');

$data['obj']=get_data($db_connectors,$page,$search);
	
$data['template']='list';	
if(count($link_get)>0)
	$data['link_get']='?'.implode('&',$link_get);
else $data['link_get']='?';
//$content['script_code']="<script>UserAdminFOX.bountyDisabled();</script>";
return parserPage('admin/users.html',$data);
function get_data($db,$page=1,$search=false)
{
	$limit_row=20;
	$sql_search='';
	if($search)
	{
		$sql_search=' where '.$search.' ';	
		$s=$db->FetchRow('select count(id) as cc from users '.$sql_search);	
		$count=$s['cc'];
		//echo $count;
	}
	else
		$count=$db->CountRow('users', 'id');
	
	$data=array('count_row'=>0,'list'=>array(),'count_page'=>0,'active_page'=>$page,'all_counts'=>$count,'limit_row'=>$limit_row);
	$limit='';
	if($page>0)
	{
		$limit_=($page-1)*$limit_row;
		$limit=' limit '.$limit_.','.$limit_row;
		
	}	
	if($count>0)
		$data['count_page']=ceil($count/$limit_row);
	
	
	
	$sql='select users.name,users.is_verification,users.email,users.id,users.created_at,users.activate,users.is_verification,users.is_airdrop from users '.$sql_search.' order by id desc'.$limit;	
	
	$data['list']=$db->FetchAll($sql);
	$data['count_row']=count($data['list']);
	return $data;
}
function get_act_data($db,$id)
{
	return $db->FetchAll('select * from users_visitation where id_user='.intval($id).' order by `date` desc limit 10');
}