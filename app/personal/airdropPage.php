<?
if($content['personal_user_info']['activate']==0){
	header('Location: /personal/develop');
	die();
}
//b_link 	telegram_link 	twitter_link 	youtube_link 	rebit_link 	id_user 	status 
$social_=array(
	'fb_link'=>array('name'=>__tr('personal.AirdropFb_linkName'),'comment'=>false,'xpath'=>'facebook.com','href'=>'https://facebook.com/UniFoxNetwork'),
	'telegram_link'=>array('name'=>__tr('personal.AirdropTelegram_linkName'),'comment'=>__tr('personal.AirdropTelegramComment'),'xpath'=>array('telegram.org','t.me'),'href'=>'https://t.me/Unifoxofficial'),
	'twitter_link'=>array('name'=>__tr('personal.AirdropTwiter_linkName'),'comment'=>false,'xpath'=>'twitter.com','href'=>'https://twitter.com/UniFoxNetwork'),
	'youtube_link'=>array('name'=>__tr('personal.AirdropYoutube_linkName'),'comment'=>__tr('personal.AirdropYoutubeComment'),'xpath'=>'youtube.com','href'=>'https://youtu.be/dd8IPNRMVY0'),	
	'rebit_link'=>array('name'=>__tr('personal.AirdropRebit_linkName'),'comment'=>__tr('personal.AirdropReditComment'),'xpath'=>'reddit.com','href'=>'https://www.reddit.com/r/UniFoxNetwork/'),    
	'medium_link'=>array('name'=>__tr('personal.AirdropMedium_linkName'),'comment'=>false,'xpath'=>'medium.com','href'=>'https://medium.com/unifox'),
	'instagram_link'=>array('name'=>__tr('personal.AirdropInstagram_linkName'),'comment'=>false,'xpath'=>'','href'=>'https://www.instagram.com/unifox.io/'),
	'comment_1'=>array('name'=>__tr('personal.AirdropBitcointalk_linkName'),'comment'=>__tr('personal.AirdropBitcointalkComment'),'xpath'=>'','href'=>'https://bitcointalk.org/index.php?topic=4456595.20'),
	'comment_2'=>array('name'=>__tr('personal.AirdropBitcointalk2_linkName'),'comment'=>false,'xpath'=>'','href'=>false),
	'friends_name'=>array('name'=>__tr('personal.AirdropTelegram2_linkName'),'comment'=>__tr('personal.AirdropTelegram2Comment'),'xpath'=>array('telegram.org','t.me'),'href'=>'https://t.me/Unifoxofficial'),
	);
$key_type=array(
	'Facebook'=>array('fb_link'),
	'Telegram'=>array('telegram_link','friends_name'),
	'Twitter'=>array('twitter_link'),
	'Youtube'=>array('youtube_link'),
	'Reddit'=>array('rebit_link'),
	'Medium'=>array('medium_link'),
	'Instagram'=>array('instagram_link'),
	//'Bitcointalk'=>array('comment_1','comment_2'),
	//'Bitcointalk'=>array('comment_1'),
	
	);		
if(isset($_POST['airpod_id']))
{
	$result=array('result'=>false);	
	$arrayInsert=array();
	$post_type=array();
	if(isset($_POST['airpod_type']))
		$post_type=$key_type[$_POST['airpod_type']];
	else $post_type=array($_POST['airpod_name']);
	
	$error_data=get_error($db_connectors,$content['personal_user_info']['id']);
	
	foreach($post_type as $v)
	{
		$insert=true;
		if(isset($error_data[$v]) and $error_data[$v]==1)			
		$insert=false;
		if($insert)
		{
			$social_[$v]['xpath']='';
			if(!isset($_POST[$v]))
				$_POST[$v]='';		
			if(trim($_POST[$v])=="")
				$result['error'][$v]=__tr('personal.Please input correct data');
			elseif($social_[$v]['xpath']!='')
			{
				if(is_array($social_[$v]['xpath']))
				{
					$xpath_error=false;
					foreach($social_[$v]['xpath'] as $xpath)
					{
						if(substr_count($_POST[$v],$xpath)==0 and !$xpath_error)
							$xpath_error=true;
						else $xpath_error=false;
					}
					if($xpath_error)
						$result['error'][$v]=__tr('personal.Please input correct data');
					else $arrayInsert[$v]=$_POST[$v];
				}
				else
				{
					if(substr_count($_POST[$v],$social_[$v]['xpath'])>0)
						$arrayInsert[$v]=$_POST[$v];
					else $result['error'][$v]=__tr('personal.Please input correct data');
				}	
				
			}
			else $arrayInsert[$v]=$_POST[$v];
		}	
	}
	if(count($result['error'])==0)
	{
		$error_insert=array();
		
		foreach($arrayInsert as $row=>$e)
		{
			$error_insert[$row]=2;
		}
		$arrayInsert['id_user']=$content['personal_user_info']['id'];
		$arrayInsert['status']=1;
		$arrayInsert['date_update']=time();
		$arrayInsert['wallets']=$_POST['wallets'];
		if(intval($_POST['airpod_id'])==0)
			$db_connectors->InsertRow('airpod', $arrayInsert);		
		else $db_connectors->UpdateRow('airpod', $arrayInsert,array('id'=>intval($_POST['airpod_id'])));
		
		if(intval($_POST['error_id'])==0){
			$error_insert['id_user']=$content['personal_user_info']['id'];
			$db_connectors->InsertRow('airpod_error', $error_insert);
		}
		else $db_connectors->UpdateRow('airpod_error', $error_insert,array('id'=>intval($_POST['error_id'])));
		
		$result['result']=true;
		include 'library/admin_send.php';
		$SEND_ADMIN=new	SendAdmin(PATH_ROOT.'plugin/send-m');
		$SEND_ADMIN->setRequest($content['personal_user_info']['id'],'airdrop');
	}
	die(json_encode($result));	
}
if(isset($_POST['giftcard-send']))
{
	$result=array('result'=>false);	
	$arrayInsert=array();
	$_POST['giftcard']=@trim($_POST['giftcard']);
	if($_POST['giftcard']=="")
		$result['error']['giftcard']=__tr('personal.Please input correct data');
	else
	{
		$d=get_gift($db_connectors,$content['personal_user_info']['id'],$_POST['giftcard']);
		if($d)
			$result['error']['giftcard']=__tr('personal.This card has already been used');
		else
		{
			//giftcard_code=123asd&wallet=15mK6S3b6R9mXFWXeKEMzZ5gcqobirK5yz
			$send=array('token'=>get_tokenUser($API_WALET,$content['personal_user_info']),'giftcard_code'=>$_POST['giftcard'],'wallet'=>$_POST['giftcard-wallet']);
			$res=$API_WALET->set_gift_card($send);
			if($res and isset($res['answer']) and $res['type']=='success')
			{
				$arrayInsert=array('id_user'=>$content['personal_user_info']['id'],'code'=>$_POST['giftcard'],'wallet'=>$_POST['giftcard-wallet']);
				$db_connectors->InsertRow('gift_card',$arrayInsert);
				$result['result']=true;
				$result['amount']=$res['answer']['amount'];
				//$data=getUserWallets($API_WALET,$content['personal_user_info']);
			}
			else
			{
				$result['error']['giftcard']=__tr('personal.This card is not correct');
			}	
			
		}
	}
	die(json_encode($result));
}

$content['title']='My Airdrop Unifox';
$content['h1']='';
$content['script_load']=array(
	'/css/personal/plugins/icheck/icheck.min.js',
	'/js/personal/airdrop.js',
	'/js/personal/code_action.js',
); 
$content['style_load']=array(
	"/css/personal/plugins/icheck/skins/all.css",
	
); 
$content['url_active']='/personal/airpod';
$data=getUserWallets($API_WALET,$content['personal_user_info']);
//$data=$wallets;
//var_dump($data);die();

$fox=array_keys($data['fox']['wallets']);
$data['wallets']=$data['fox']['wallets'][$fox[0]];
$data['status_program']=__tr('personal.not registered');

$d=get_data_airpod($db_connectors,$content['personal_user_info']['id']);
if($d)
{
	$data['data']=$d;	
	$data['error']=get_error($db_connectors,$content['personal_user_info']['id']);
	
	
}
/*if(!$data['_button'])
{
	$content['script_load'][]='/js/personal/airdrop_disabled.js';
}*/
$gift_more=false;

$data['gift-code']='';
$data['gift_send']=false;
if(!$gift_more)
{
	$d=get_gift($db_connectors,$content['personal_user_info']['id']);
	if(!$d)
		$data['gift_send']=true;
	else $data['gift-code']=$d['code'];
}
else
	$data['gift_send']=true;	

$data['list_social']=$social_;
$data['list_type']=$key_type;

if(isset($_GET['type']))
{
	$content['script_code']="<script>Airdrop.addGet('?type=new');</script>";
}

return parserPage('airpod.html',$data,$content);
function get_data_airpod($db,$id_user)
{
	$sql='select * from airpod where id_user='.$id_user;
	
	return $db->FetchRow($sql);	
}
function get_error($db,$id_user)
{
	$true=array('id_error'=>0);
	$error_=$db->FetchRow('select * from airpod_error where id_user='.$id_user);
	if($error_)
	{
		$true['id_error']=$error_['id'];
		foreach($error_ as $k=>$v)
		{
			if($v!='')
				$true[$k]=$v;
		}
	}	
	
	return $true;	
	
}
function get_gift($db,$id_user,$code='')
{
	$sql='select * from gift_card where id_user='.$id_user;
	if($code!='')
		$sql.=' and code="'.$db->sql_escape($code).'"';	
	return $db->FetchRow($sql);	
}