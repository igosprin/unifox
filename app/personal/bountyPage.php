<?
if($content['personal_user_info']['activate']==0){
	header('Location: /personal/develop');
	die();
}
//b_link 	telegram_link 	twitter_link 	youtube_link 	rebit_link 	id_user 	status 
$social_=array('Facebook','Youtube','Twitter','Telegram','Bitcointalk');
$key_type=array(
	'Facebook'=>array('fb_link'),
	'Telegram'=>array('telegram_link','friends_name'),
	'Twitter'=>array('twitter_link'),
	'Youtube'=>array('youtube_link'),
	'Reddit'=>array('rebit_link'),
	'Instagram'=>array('instagram_link'),
	//'Bitcointalk'=>array('comment_1','comment_2'),
	//'Bitcointalk'=>array('comment_1'),
	
	);		
if(isset($_POST['task_recieve']))
{
	$result=array('result'=>false);	
	$arrayInsert=array();
	$array_soccial=get_data_socials($db_connectors,array($_POST['task_recieve']));	
	foreach($array_soccial[$_POST['task_recieve']] as $v=>$row)
	{
		if(!isset($_POST[$v]))
			$result['error'][$v]=__tr('personal.Please input correct data');		
		if(trim($_POST[$v])!="")
		{
			$cou=get_user_task_delp($db_connectors,$content['personal_user_info']['id'],$v,$_POST[$v]);
			if($cou['count']==0)
				$arrayInsert[]=array('id_user'=>$content['personal_user_info']['id'],'row_key'=>$v,'val'=>$_POST[$v],'status'=>1,'date_update'=>time(),'type'=>$_POST['task_recieve'],'day'=>intval(date('Ymd')));
			else
			{
				if($cou['type_stakes']=="")
					$result['error'][$v]=__tr('personal.This significance has already been introduced');
				if($cou['type_stakes']=="every_day")
					$result['error'][$v]=__tr('personal.This value has already been entered today Try again tomorrow');
				if($cou['type_stakes']=="many")
					$result['error'][$v]=__tr('personal.This significance has already been introduced');
			}
				
		}
	}
	if(count($result['error'])==0)
	{		
		if(count($arrayInsert)>0)
		{
			$result['html']=array();
			foreach($arrayInsert as $insert)
			{
				//var_dump($insert);
				$db_connectors->InsertRow('bounty_program', $insert);
				$result['html'][]='<tr><td>'.date('d/m/Y',$insert['date_update']).'</td>
				<td>'.$insert['type'].'</td>
				<td>'.$array_soccial[$_POST['task_recieve']][$insert['row_key']]['name'].'</td><td><span class="status-task inprogres">'.__tr('personal.In progress').'</span></td></tr>';
			}
		}	
		
		 
		$result['result']=true;
	}
	die(json_encode($result));	
}
if(isset($_POST['get_task']))
{
	$result=array('result'=>false,'next'=>false);
	$list=get_user_task($db_connectors,$content['personal_user_info']['id'],$_POST['type'],$_POST['page']);
	if(count($list['task'])==0)
	{
		if($_POST['page']==1)
			$result['html']=parserPage('bounty/user_task_list.html',$list);		 
	}	
	else
	{
		$t=get_data_socials($db_connectors,array($_POST['type']));
		$list['task_key']=$t[$_POST['type']];
		$result['html']=parserPage('bounty/user_task_list.html',$list);	
		$result['next']=$list['next_page'];
		if($result['next'])
			$result['page']=$_POST['page']+1;
		$result['result']=true;
	}
	$result['count_page']=$list['count_page'];
	die(json_encode($result));	
	
}

	
$content['title']=__tr('personal.My Bounty Unifox');
$content['h1']='';
$content['script_load']=array(
	'/js/personal/bounty.js',
); 
$content['style_load']=array(
	
	
); 
$content['url_active']='/personal/bounty';
$data=getUserWallets($API_WALET,$content['personal_user_info']);
//$data=$wallets;
$data['task']=get_data_socials($db_connectors,$social_);

$data['social']=$social_;
$data['isAirdrop']=true;
$confirm=array();
if($content['personal_user_info']['is_airdrop']==0)
{	
	$data['isAirdrop']=false;
	$content['script_code']="<script>BountyFox.bountyDisabled();</script>";	
	foreach($social_ as $k)
	{
		$confirm[$k]=true;
	}
}
else
{
	$conf_user=get_confirmated($db_connectors,$content['personal_user_info']['id']);
	foreach($key_type as $k=>$val)
	{
		$confirm[$k]=false;
		$error=0;
		foreach($val as $r)
		{
			if(!isset($conf_user[$r]))
				$error++;
		}
		if($error==0)
			$confirm[$k]=true;	
	}
	$confirm['Bitcointalk']=true;
}	
$data['confimeted_type']=$confirm;

return parserPage('bounty.html',$data,$content);

function get_data_socials($db,$array_socials)
{
	$data=array();
	
	foreach($array_socials as $v)
	{
		$data[$v]=array();
		$d=$db->FetchAll('select row_key,name,comment from bounty_list where type="'.$v.'"');
		if($d)
		{
			foreach($d as $task)
			{
				$data[$v][$task['row_key']]=$task;
			}
			
		}	
	}
	
	return $data;
}
function get_data($db,$id_user)
{
	$sql='select * from bounty_program where id_user='.$id_user;
	$data=array();
	$s=$db->FetchAll($sql);	
	if($s)
	{
		foreach($s as $v)
		{
			$data[$v['row_key']]=$v;
		}
	}
	return $data;
}
function get_user_task($db,$id_user,$type,$page=1)
{
	$data=array();
	$limit=10;
	if($page<=0) $page=1;	
	$start=($page-1)*$limit;	
	$sql_limit=' limit '.$start.','.$limit;	
	
	$count=$db->CountRow('bounty_program','id', array('id_user'=>$id_user,'type'=>$type));
	
	$data['count_page']=0;
	$data['next_page']=true;
	
	$data['task']=array();
	if($count>0)	
		$data['count_page']=ceil($count/$limit);
	if($page >=$data['count_page'])
		$data['next_page']=false;
	$s=$db->FetchAll('select * from bounty_program where id_user='.$id_user.' and type="'.$type.'" order by date_update desc '.$sql_limit);
	if($s)
	{
		$data['task']=$s;
	}
		
	
	
	return $data;
}
function get_user_task_delp($db,$id_user,$row,$val)
{
	$r=$db->FetchRow("select type_stakes from bounty_list where row_key='".$row."'");
	$rules=$r['type_stakes'];
	if($rules=='')
		return array('count'=>$db->CountRow('bounty_program','id', array('id_user'=>$id_user,'row_key'=>$row),'and status=3'),'type_stakes'=>$rules);
	if($rules=='every_day')	
		return array('count'=>$db->CountRow('bounty_program','id', array('id_user'=>$id_user,'row_key'=>$row,'day'=>intval(date('Ymd')))),'type_stakes'=>$rules);
	if($rules=='many')	
		return array('count'=>$db->CountRow('bounty_program','id', array('id_user'=>$id_user,'row_key'=>$row,'val'=>$val)),'type_stakes'=>$rules);
}
function get_confirmated($db,$id_user)
{
	$true=array();
	$error_=$db->FetchRow('select * from airpod_error where id_user='.$id_user);
	if($error_)
	{
		foreach($error_ as $k=>$v)
		{
			if($v!='' and $v==1)
				$true[$k]=$v;
		}
	}	
	
	return $true;	
	
}
