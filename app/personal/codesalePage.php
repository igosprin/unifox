<?
$table='code_action';
$data=array('result'=>false);
if(isset($_POST['code_sale']))
{
	$data=array('result'=>false);	
	$arrayInsert=array();
	$_POST['code_sale']=@trim($_POST['code_sale']);
	if($_POST['code_sale']=="")
		$data['error']['code_sale']=__tr('personal.Please input correct data');
	else
	{
		$d=get_sale($db_connectors,$table,$content['personal_user_info']['id']);
		if($d>0)
			$data['error']['code_sale']=__tr('personal.This card has already been used');
		else
		{
			$send=array('token'=>get_tokenUser($API_WALET,$content['personal_user_info']),'giftcard_code'=>$_POST['code_sale'],'wallet'=>$_POST['wallet']);
			$res=$API_WALET->set_public_card($send);
			//$res=array('type'=>'success','answer'=>array('status'=>1,'amount'=>20));
			if($res and isset($res['answer']) and $res['answer']['status']==1 and $res['type']=='success')
			{
				$arrayInsert=array('id_user'=>$content['personal_user_info']['id'],'code'=>$_POST['code_sale'],'date'=>time(),'wallet'=>$_POST['wallet']);
				$db_connectors->InsertRow($table,$arrayInsert);
				$data['result']=true;
				$data['txt']=str_replace('{NUMBER}',$res['answer']['amount'],__tr('personal.{NUMBER} FOX credited to your account'));			
				$data['answer']=$res['answer'];			
			}
			else
			{
				$data['error']['code_sale']=__tr('personal.This card is not correct');
				$data['answer']=$res['answer'];
			}	
			
		}
	}
}
else
{
	if(get_sale($db_connectors,$table,$content['personal_user_info']['id'])==0)
	{
		
		$w=getUserWallets($API_WALET,$content['personal_user_info']);
		$fox=array_keys($w['fox']['wallets']);
		if(isset($fox[0]))
		{
			$r=array();
			$r['wallets']=$w['fox']['wallets'][$fox[0]];		
			$data['result']=true;
			$data['html']=parserPage('codesale.html',$r,$content);
		}
	}
}
die(json_encode($data));	



function get_sale($db,$table,$id_user)
{
	return $db->CountRow($table,'id', array('id_user'=>$id_user));
}

?>