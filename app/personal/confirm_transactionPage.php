<?php
$content['LAYOUT']='layout_confirmation.html';

if(isset($_POST['code']) and isset($_POST['trx_id']))
{
	
	$codeID=$_POST['trx_id'];
	$d=$db_connectors->FetchRow('select * from send_transaction where id_user='.$content['personal_user_info']['id'].' and id_t="'.$db_connectors->sql_escape($codeID).'" and status=3');
	$result=array('result'=>false);
	if($d)
	{		
		$send=array('code'=>$_POST['code'],'token'=>get_tokenUser($API_WALET,$content['personal_user_info']));
		$res=$API_WALET->send_currency_confirmResult($send);	

/*test-ok*/
		//$res=array('type'=>'success');
		//$res['answer']=array('txhash'=>'asdasdas6676767asdhajsd898989');
/*test-error*/	
	/*	$res=array('type'=>'error');
		
		$res['msg']='adsads asd ad asd';*/
/*end test*/
		
				
		$_SESSION['transaction_show']=$codeID;
		if(isset($res['answer']) and $res['type']=='success')
		{
			/*$update=array('status'=>1,'txhash'=>$res['answer']['txhash']);
			$db_connectors->UpdateRow('send_transaction',$update,array('id_t'=>$codeID));*/	
			$result['result']=true;
			$result['status']=1;	
			$result['txhash']=$res['answer']['txhash'];	
			deleteCache('wallets_list',$content['personal_user_info']);			
		}
		else
		{
			
			if(isset($res['msg'])){
				$update=array('status'=>2);
				$update['error']=$res['msg'];
				
			}
			else
			{
				$update=array('status'=>2);
				$update['error']='Remote server is not responding. Try later.';
			}
				
			//$db_connectors->UpdateRow('send_transaction',$update,array('id_t'=>$codeID));*/
			$result['error']=$update['error'];
			$result['status']=$update['status'];
		}
	}
	die(json_encode($result));	
}
if(isset($_POST['save_code']) and isset($_POST['trx_id']))
{
	$result=array('result'=>false);
	$codeID=$_POST['trx_id'];
	$update=array('status'=>intval($_POST['save_code']));
	if(isset($_POST['error']))
		$update['error']=$_POST['error'];
	if(isset($_POST['txach']))
		$update['txhash']=$_POST['txach'];
	$db_connectors->UpdateRow('send_transaction',$update,array('id_t'=>$codeID));
	
	if($_POST['save_code']==1)
	{
		$result['url']=LINK_LANG.'/personal/wallets?confirm_id='.$codeID;
	}
	if($_POST['save_code']==2)
	{
		$result['url']=LINK_LANG.'/personal/wallets?confirm_id='.$codeID;
	}
		
	die(json_encode($result));	
}


if(isset($_GET['code']) and isset($_GET['tx_id']))
{
	$id=intval(base64_decode($_GET['tx_id']));
	if($id<=0)
	{	
		return CatchError($content);
		die();
	}
	else
	{
		if(trim($_GET['code'])=='' or trim($_GET['tx_id'])=='')
			return CatchError($content);
		
		$codeID=genereateTransactionCode($id,$content['personal_user_info']);
		$d=$db_connectors->FetchRow('select * from send_transaction where id_user='.$content['personal_user_info']['id'].' and id_t="'.$db_connectors->sql_escape($codeID).'"');
		if($d)
		{
			if($d['status']!=1)
			{				
				$db_connectors->UpdateRow('send_transaction',array('status'=>3),array('id_t'=>$codeID));
				$data=array();
				$data['trx_id']=$codeID;
				$data['code']=$_GET['code'];
				$content['title']=__tr('personal.Confirm transaction');			
				
				$content['script_code']=parserPage('confirm/script_code.html',$data);
				return parserPage('confirm.html',$data,$content);
			}	
			/*if($d['status']==2)
				return CatchError($content);	
			
			/*header('Location: '.$url_header);
			die();*/
			
		}
		else return CatchError($content);
	}
}
else return CatchError($content);