<?php
$wallet_def_=strtolower(CURENCY);
$wallet_min_buy=floatval(FOX_MIN_BUY);
if(!$content['XMLHttpRequest'])
	die('Access is denied');
	

$result=array('result'=>false);
if(isset($_GET['type']))
{
	switch($_GET['type'])
	{
		case 'send_pars':			
			if(isset($_POST['url']) and $_POST['url']!='')
			{
				
				
				$data=parse_url($_POST['url']);	
				$data['amount']=str_replace(array('amount=',','),array('','.'),$data['query']);
				
				if($data['scheme']==$wallet_def_)
				{
					if(strlen($data['path'])>18)
					{
						if($data['amount']>0)
						{
							$result['wallet2']=$data['path'];
							$result['tt']=$data['scheme'];
							$result['value']=$data['amount'];
							$result['result']=true;
						}
						else $result['error']=__tr('personal.Please input correct qr-code data');
						
						
					}
					else
						$result['error']=__tr('personal.Please input correct qr-code data');
				}
				else					
				$result['error']=__tr('personal.Please input correct qr-code data');
			}	
		break;
		case 'send_fox':
			$send=array();
			if(isset($_POST['wallet2'])  and isset($_POST['value']))
			{				
				//wallet[from]=0x590a11a68acc12f4a9d091ac2098ae1581beae2f&wallet[to]=0xab6d6f69f14421e53b07c6a8c3da73f511d88972&amount=1
				if($_POST['wallet2']=='')
					$result['error']['wallet2']=__tr('personal.Please input correct wallet');
				$_POST['value']=str_replace(',','.',$_POST['value']);
				if($_POST['value']<=0)
					$result['error']['value']=__tr('personal.Please input correct amount');
				if(!isset($result['error']['wallet2']))
				{
					if($_POST['wallet2']==$_POST['wallet'])
						$result['error']['wallet2']=__tr('personal.You can not send to this wallet');		
				}	
				
				if(count($result['error'])==0)
				{
					$send['wallet']=array('from'=>$_POST['wallet'],'to'=>$_POST['wallet2']);
					$send['amount']=$_POST['value'];					
				}	
				
			}
			elseif(isset($_POST['adress']))
			{
				$data=parse_url($_POST['adress']);	
				$data['amount']=str_replace(array('amount=',','),array('','.'),$data['query']);
				
				if($data['scheme']==$wallet_def_)
				{
					if(strlen($data['path'])>18)
					{
						if($data['amount']>0)
						{
							$send['wallet']=array('from'=>$_POST['wallet'],'to'=>$data['path']);
							$send['amount']=$data['amount'];						
						}
						else $result['error']=__tr('personal.Please input correct qr-code data');
						
						
					}
					else
						$result['error']=__tr('personal.Please input correct qr-code data');
				}
				else					
					$result['error']=__tr('personal.Please input correct qr-code data');
					
			}
			
			if(!isset($result['error']['value']))
			{
				//$balance=$API_WALET->get_user_wallet(array('token'=>get_tokenUser($API_WALET,$content['personal_user_info']),'wallet'=>$_POST['wallet']));
				$res=getUserWallets($API_WALET,$content['personal_user_info']);				
				if(isset($res['fox']['wallets'][$_POST['wallet']]))
				{
					if(floatval($_POST['value'])>=floatval($res['fox']['wallets'][$_POST['wallet']]['balance']))
					{	
						$send=array();
						$result['error']['value']=__tr('personal.Not enough money');
					}
				}
				else 
				{
					$send=array();
					$result['error']['value']=__tr('personal.Not enough money');
				}	
				
			}
			
			if(count($send)>0)
			{
				
				if(!$content['personal_user_info']['is_verification'])
				{
					$result['result']=true;			
					$result['html']=parserPage('wallets/buy_form_account_confirm.html',$data);
					$result['title']=__tr('personal.Error Send').' '.CURENCY;
					$result['verification_error']=true;	
				}
				else
				{
					$send['token']=get_tokenUser($API_WALET,$content['personal_user_info']);
					/*$res=$API_WALET->send_currency_get($send);
					$result['dd']=$res;
					if(isset($res['answer']) and $res['type']=='success')
					{
						$result['txhash']=$res['answer']['txhash'];
						$result['result']=true;
						deleteCache('wallets_list',$content['personal_user_info']);					
					}
					else
						$result['error']['wallet']='There is no money on your wallet';*/
					
					$send['email']=$content['personal_user_info']['email'];
				//	$send['email']='igosprin@gmail.com';
					$send['lang']=LANG;
					$res=$API_WALET->send_currency_getConfirm($send);				
					if(isset($res['answer']) and $res['type']=='success')
					{
						$codeID=genereateTransactionCode($res['answer']['tx_id'],$content['personal_user_info']);
						$db_connectors->InsertRow('send_transaction',array('id_t'=>$codeID,'id_user'=>$content['personal_user_info']['id'],'wallet'=>$_POST['wallet'],'date'=>time()));
						$result['result']=true;
						$data['result_id']=$result['result_id']=$codeID;
						$result['title']=__tr('personal.Send').' '.CURENCY;
						$result['html']=parserPage('wallets/new/form/send_result.html',$data);	
					}
					elseif(isset($res['msg']))
					{
						$result['error']['wallet']=$res['msg'];
					}
				}				
			}	
				
		break;
		case 'send_fox_result':
			$result['html']=parserPage('wallets/new/form/send_result.html',$data);
			$result['transaction']=true;
			$result['result']=true;
			$result['title']=__tr('personal.Send').' '.CURENCY;
		break;
		case 'rate':
			$curs=$API_WALET->get_rate(array('token'=>get_tokenUser($API_WALET,$content['personal_user_info']),'type'=>$_POST['type']));
			if(isset($curs['answer']['currency']))
			{
				
				$result['result']=true;
				$result['rate']=$curs['answer']['currency']['rate'];
				$result['string']=$curs['answer']['string_format'];
				
				$array_type=getTypeBuy($_POST['type']);				
				$result['currency_info']=$array_type;
				if(isset($_POST['ufox']))
				{
					$ufox=floatval($_POST['ufox']);
					/*
					if($ufox>=$array_type['min_buy'])
					{
						bcscale(20);
						//$type_p=bcdiv(1,floatval($curs['answer']['currency']['rate']));							
						$result['price']=bcmul($ufox,floatval($curs['answer']['currency']['rate']));
					}	*/
					if($ufox>=$array_type['min_buy'])
					{
						$result['price']=returnFoxAmount($curs['answer']['currency']['rate'],$ufox);
					}
					
				}	
					
			}
		break;
		case 'total_currency':
			if(isset($_POST['rate']) and isset($_POST['ufox']))
			{
				//var_dump($_POST);
				//echo floatval($_POST['rate']);
				$array_type=getTypeBuy($_POST['type']);
				$ufox=floatval($_POST['ufox']);
				if($ufox>=$array_type['min_buy'])
				{
					/*bcscale(20);
					$type_p=bcdiv(1,$_POST['rate']);							
					$result['price']=bcmul($ufox,$type_p);*/
					
					$result['price']=returnFoxAmount($_POST['rate'],$ufox);
					$result['result']=true;
				}
				else
					$result['error']=__tr('personal.Please input correct amount');
			}
		break;
		case 'resive_code':
			if(isset($_POST['value']))
			{
				
				if($_POST['value']>0)
				{
					$result['transaction']=$wallet_def_.':'.$_POST['wallet'].'?amount='.$_POST['value'];	
					$result['qr_mode']='https://api.qrserver.com/v1/create-qr-code/?size=150x150&data='.$wallet_def_.':'.$_POST['wallet'].'?amount='.$_POST['value'];					
					$result['result']=true;	
				}
				else
					$result['error']=__tr('personal.Please input correct amount');
			}
		break;
		case 'buy_fox':
			
			
			$SETTING_CURRENS=getTypeBuy($_POST['t_type']);
			
			if(isset($_POST['value']) and $_POST['t_type']!='')
			{
				$ufox=floatval($_POST['value']);
				if($ufox<=0)
					$result['error']['value']=__tr('personal.Please input correct amount');
				else
				{
					$content['personal_user_info']['is_verification']=1;
					if(!$content['personal_user_info']['is_verification'])
					{
						$result['result']=true;			
						$result['html']=parserPage('wallets/buy_form_account_confirm.html',$data);
						$result['title']=__tr('personal.Error Buy').' '.CURENCY;
						$result['verification_error']=true;	
					}
					else
					{
						/*bcscale(20);
						$type_p=bcdiv(1,$_POST['rate']);							
						$amount_=bcmul($ufox,$type_p);*/
						
						$amount_=returnFoxAmount($_POST['rate'],$ufox);					
						
						//amount=0.0005&wallet_fox=qweqweqweqweqweqwe&amount_fox=10&rate=100
						setWalletUser($db_connectors,$content['personal_user_info']['id'],$_POST['wallet']);
						$send=array('amount_fox'=>$amount_,'wallet_fox'=>$_POST['wallet'],'rate'=>$_POST['rate'],'token'=>get_tokenUser($API_WALET,$content['personal_user_info']));
						$send['amount']=$ufox;
						$send['currency']=$SETTING_CURRENS['currency_form'];			
						
						switch($_POST['t_type'])
						{
							case 'btc':
								$res=$API_WALET->set_request_buy_crypto($send);		
						
								if(isset($res['answer']) and $res['type']=='success')
								{
									$result['transaction']=time();	
									$data['qr_mode']=$result['qr_mode']=$res['answer']['qr_url'];
									
									$data['amount_ufx']=$result['amount_ufx']=$amount_.' '.CURENCY;	
									
									$data['sum']=$result['sum']=$res['answer']['sum'];	
									$data['type']=$result['type']=$_POST['t_type'];
									
									$data['wallet_to']=$result['wallet_to']=$res['answer']['wallet'];	
									$text=__tr('personal.Compleate buy fox text');
									$data['text']=$result['text']=str_replace(array('AMOUNT_FOX','AMMOUNT_FO_CURRENCY'),array($amount_.' '.CURENCY,$ufox.' '.$SETTING_CURRENS['currency']),$text);	
									//To complete the purchase '..', please transfer '.$amount_.' '.$SETTING_CURRENS['currency'].'  to the specified address.
									$result['result']=true;
									$result['html']=parserPage('wallets/new/form/buy_result.html',$data);
								}
							break;
							case 'eur':
								$res=$API_WALET->set_request_buy_bank($send);
								var_dump($res);
								die();	
						
								if(isset($res['answer']) and $res['type']=='success')
								{
									$result['transaction']=time();	
									$data['qr_mode']=$result['qr_mode']=$res['answer']['qr_url'];
									
									$data['amount_ufx']=$result['amount_ufx']=$amount_.' '.CURENCY;	
									
									$data['sum']=$result['sum']=$res['answer']['sum'];	
									$data['type']=$result['type']=$_POST['t_type'];
									
									$data['wallet_to']=$result['wallet_to']=$res['answer']['wallet'];	
									$text=__tr('personal.Compleate buy fox text');
									$data['text']=$result['text']=str_replace(array('AMOUNT_FOX','AMMOUNT_FO_CURRENCY'),array($amount_.' '.CURENCY,$ufox.' '.$SETTING_CURRENS['currency']),$text);	
									//To complete the purchase '..', please transfer '.$amount_.' '.$SETTING_CURRENS['currency'].'  to the specified address.
									$result['result']=true;
									$result['html']=parserPage('wallets/new/form/buy_result.html',$data);
								}
							break;
							
						}
					}					
					
				}
			}
			else
			{
				if($_POST['value']<$wallet_min_buy) $result['error']['value']=__tr('personal.Please input correct amount');
				if($_POST['type']=='') $result['error']['type']=__tr('personal.Please select payment method');
			} 
		
		break;
		case 'my_balance':
			
			$data=getUserWallets($API_WALET,$content['personal_user_info']);
			if($_POST['time']=='strict')
			{
				$result['balance']=$data['fox']['balance'];			
				$result['w_balance']=array();
				foreach($data['fox']['wallets'] as $w)
				{
					$result['w_balance'][$w['wallet']]=$w['balance'];
				}
			}
			else
			{
			
				$fox=array_keys(@$data['fox']['wallets']);
				$result['wallet_list']=array('wallets'=>$data['fox']['wallets'][$fox[0]]['wallet'],'balance'=>$data['fox']['balance'].' FOX');
				/*$result['w_crip']=$data['fox']['wallets'][$fox[0]]['wallet'];
				$result['balance_crip']=;*/
			}
			$result['result']=true;
			
			
		break;
		case 'my_stakes':
			$result['stakes']=0;	
			$d=$db_connectors->FetchRow('select sum(stakes) as stakes from bounty_program where id_user='.$content['personal_user_info']['id'].' and status=3');
			
			if($d)
				$result['stakes']=intval($d['stakes']);
			$s=$db_connectors->FetchRow('select point from referal_point where id_user='.$content['personal_user_info']['id']);
			if($d)
				$result['stakes']=$result['stakes']+intval($s['point']);	
		break;
		case 'send_status':
			if(isset($_POST['code_p']))
			{
				$d=$db_connectors->FetchRow('select * from send_transaction where id_user='.$content['personal_user_info']['id'].' and id_t="'.$db_connectors->sql_escape($_POST['code_p']).'"');	
				if($d)
				{
					$result['result']=true;
					if($d['status']==1)
					{
						$result['status']=true;
						$result['txhash']=$d['txhash'];						
					}
					if($d['status']==0)
						$result['status']=false;
										
					if($d['status']==2){
						$result['result']=false;
						$result['error']=$d['error'];
					}
											
				}
			}			
		break;
		/*case 'user_info':
			header('Access-Control-Allow-Origin: *');
			header('Content-Type: application/json');
			$result=array();
		break;*/
		
	}
	
}
die(json_encode($result));

function returnFoxAmount($rate,$ammount_currency)
{
	bcscale(20);
	//$type_p=bcdiv(1,floatval($curs['answer']['currency']['rate']));							
	return round(bcmul($ammount_currency,floatval($rate)));
}
/*$data=getUserWallets($API_WALET,$content['personal_user_info']);*/
//$_SESSION['t']=$data['token_temp'];

/*
$walet_user=$API_WALET->get_user($API_WALET->pars_data_user($content['personal_user_info']),true);
$walets=$API_WALET->get_user_balnace(array('token'=>$walet_user));
$data=$walets;*/


//return parserPage('wallets.html',$data);


?>