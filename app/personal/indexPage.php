<?php
/* temp */
/*if($content['personal_user_info']['is_admin']!=1)
{
	
	die();
}*/
header("Location: /personal/wallets");

$content['h1']='';
$content['title']=__tr('personal.Dashboard Unifox');
/*script_load*/
$content['script_load']=array(
	'/css/personal/plugins/echarts/echarts-custom-for-dashboard.js',
    '/css/personal/plugins/flot-chart/jquery.flot.js',
    '/css/personal/plugins/flot-chart/jquery.flot.time.js',
    '/js/personal/chart-flot.js',
    '/css/personal/plugins/morris-chart/js/raphael-min.js',
    '/css/personal/plugins/morris-chart/js/morris.min.js',
	'js/personal/chart-morris.js',

);
$content['style_load']=array(
	"/css/personal/plugins/jvectormap/jquery-jvectormap-2.0.1.css",
	"/css/personal/plugins/morris-chart/css/morris.css" ,
);

$content['url_active']='/personal';

$data=array();


/*FOX*/

$data=getUserWallets($API_WALET,$content['personal_user_info']);

$fox=array_keys(@$data['fox']['wallets']);
$unicach=array_keys($data['unicach']['wallets']);
$data['fox_first']=$data['fox']['wallets'][$fox[0]];
$data['unicash_first']=$data['unicach']['wallets'][$unicach[0]];

$data['list_transaction']=$API_WALET->get_transaction(array('token'=>get_tokenUser($API_WALET,$content['personal_user_info']),'index'=>true));

return parserPage('index.html',$data,$content);


?>