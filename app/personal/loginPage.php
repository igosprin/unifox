<?php
$content['LAYOUT']='layout_login.html';
$data=array();
$url=LINK_LANG.'/personal/';
if($AUTH_APP->hasIdentity())
	header('Location:'.$url);
else{
	if(isset($_SESSION[USER_SESSION_NAME.'_t']))
		unset($_SESSION[USER_SESSION_NAME.'_t']);
}
if($params['action']!='index')
{
	switch($params['action'])
	{
		case 'registration':
			$content['title']=__tr('personal.Registration Unifox');
			$content['type_login']='SignUp';
			$result=registration($db_connectors);
			if(!$result)
				return parserPage('registration.html',$data);
			else
			{
				if($result['result'])
				{
					//$AUTH_APP->setIdentity($result['id'],true);			
					//$result['acp_url']=$url;
					$result['activation']=true;				
					
					die(json_encode($result));
				}
				else
					die(json_encode($result));
			}			
		break;
		case 'social':
			$result=$params['type']($db_connectors);
			$AUTH_APP->setIdentity($result['id'],true);
			unset($result['id']);
			if(isset($_SESSION['confirm_redirect']) and $_SESSION['confirm_redirect']!='')
			{	
				$result['acp_url']=$_SESSION['confirm_redirect'];
				unset($_SESSION['confirm_redirect']);
			}
			else $result['acp_url']=$url;
			die(json_encode($result));
		break;
		case 'recovery':
			$content['title']=__tr('personal.Recovery Unifox');
			$content['type_login']='Repaswd';
			$result=recovery($db_connectors,$SEND_MAIL);
			if(!$result)
				return parserPage('recovery.html',$data);
			else die(json_encode($result));
		break;
		case 'recovery_access':	
			if(isset($_GET['code']))
			{
				$content['title']=__tr('personal.New Password Unifox');
				$content['type_login']='Repaswd_new';
				$result=recovery_access($db_connectors);
				if(!$result) 
					return CatchError($content);
				else{
					
					return parserPage('recovery_access.html',$data);
				}
				
			}
			elseif(isset($_POST['code']) and isset($_POST['email']) and isset($_POST['password']))
			{
				$result=repassword($db_connectors);
				if($result['result'])
					$result['acp_url']='/signin';
				die(json_encode($result));
			}
			else			
				return CatchError($content);
			
		break;
		case 'activate_link_send':			
			$result=activate_link_send($db_connectors,$SEND_MAIL);
			
			die(json_encode($result));
		break;
		
		case 'activate_account':
			if(isset($_GET['code']))
			{
				$result=activate_account($db_connectors);
				if(!$result)
					return CatchError($content);
				else
				{
					
					$AUTH_APP->setIdentity($result,true);
					header('Location: /personal');die();
				}
			}
			else return CatchError($content);
			
			
			
		break;
	}
}
else
{
	$content['title']=__tr('personal.Login Unifox');
	$content['type_login']='SignIn';
	$res=autorization($db_connectors);
	if(!$res)
		return parserPage('login.html',$data);
	else
	{
		$result=array('result'=>false);
		if($res['result'])
		{
			
			$AUTH_APP->setIdentity($res['id'],true);
			if(isset($_SESSION['confirm_redirect']) and $_SESSION['confirm_redirect']!='')
			{	
				$result['acp_url']=$_SESSION['confirm_redirect'];
				unset($_SESSION['confirm_redirect']);
			}
			else $result['acp_url']=$url;	
			//$result['acp_url']=$url;
			$result['result']=true;
			unset($result['id']);
		}
		else
		{
			$result['error']=array();
			if(isset($res['error']))
				$result['error']=$res['error'];
			else 
				$result['error']['global']='invalid e-mail or password';
		}	
		die(json_encode($result));	
	}
	
	
}	


function autorization($db_connectors)
{
		if(isset($_POST['password']))
		{
			
			$email=$_POST['email'];
			$result=array('result'=>false);
			$error=array();
			if(!filter_var($email, FILTER_VALIDATE_EMAIL))
				$error['email']='Registration-error-email';
			if(trim($_POST['password'])=="")
				$error['password']='Registration-error-password1';
			$pwd=createPWD($_POST['password']);
			if(count($error)==0)
			{
				$user=$db_connectors->FetchRow('select id,email,activate from users where email="'.$db_connectors->sql_escape($email).'" and password="'.$pwd.'"');
				
				if($user)
				{
					if($user['activate']==1)
					{
						$result['result']=true;				
						$result['email']=$user['email'];
						$result['id']=$user['id'];
						$result['remeber']=false;
						if(isset($_POST['rpwd']))
							$result['remeber']=true;
					}
					else
					{
						$result['error']['global']='This user is not activated';
					}	
				}	
			}
			else
				$result['error']=$error;
			return $result;	
		}
		return false;	
}
function registration($db_connectors)
{
	if(isset($_POST['password']))
	{
		$pwd=createPWD($_POST['password']);
		$re_pwd=createPWD($_POST['password_confirmation']);
		$email=$_POST['email'];
		$result=array('result'=>false);
		$error=array();
		if(!filter_var($email, FILTER_VALIDATE_EMAIL))
			$error['email']='Registration-error-email';
		else{
			$exist=$db_connectors->CountRow('users','id',array('email'=>$db_connectors->sql_escape($email)));
			if($exist>0)
				$error['email']	='Registration-error-email2';
		}
		if(strlen($_POST['password'])<6)
			$error['password']='Registration-error-passord';
		elseif($re_pwd!=$pwd)
			$error['password']='Registration-error-password2';
			
		if(!isset($_POST['terms']))
			$error['terms']='You should agree with terms and conditions';
		if(!isset($_POST['police']))
			$error['police']='You should agree with privacy policy';
		if(!isset($_POST['notification']))
			$error['notification']='You should agree with newsletter and notification';
		
		if(count($error)==0)
		{
			$arra_insert=array('email'=>$email,'name'=>$_POST['name'],'password'=>$pwd,'password_api'=>$pwd,'img'=>'','activate'=>0,'created_at'=>date('Y-m-d G:i'),'fb_id'=>'','activate_code'=>'');
			$id=$db_connectors->InsertRow('users',$arra_insert);		
			$result['result']=true;			
			$result['id']=$id;
			if(isset($_COOKIE[REFERAL_COOKIE])){
				$db_connectors->InsertRow('referal_user',array('id_user'=>$id,'id_referal_user'=>$_COOKIE[REFERAL_COOKIE],'date_update'=>time()));
				setCookie(REFERAL_COOKIE,0,time()-3600,'/');
			}				
		}
		else
			$result['error']=$error;
		return $result;
	}
}
function social_fb($db_connectors)
{
	$update_=array();			
	$result=array('result'=>false);
	if(isset($_POST['user']) and isset($_POST['user']['id']) and intval($_POST['user']['id'])>0)
	{
		/*$data_search=array();
		$data_search['fb']=$_POST['user']['id'];
		if(isset($_POST['user']['email']) and $_POST['user']['email']!='')
			$data_search['email']=$_POST['user']['email'];*/
		
		$user_=search_($db_connectors,$_POST['user']);
		if($user_)
		{
			if($user_['fb_id']=='')
				$update_['fb_id']=$_POST['user']['id'];
			if($user_['fb_id']!='' and isset($_POST['user']['email']) and $user_['email']=='')
				$update_['email']=$db_connectors->sql_escape($_POST['user']['email']);
			if(isset($_POST['user']['name']) and $_POST['user']['name']!='')
				$update_['name']=$db_connectors->sql_escape($_POST['user']['name']);
			if($user_['img']=='')
				$update_['img']='http://graph.facebook.com/' . $_POST['user']['id'] . '/picture?type=large';
			if(count($update_)>0)
				$db_connectors->UpdateRow('users',$update_ ,array('id'=>$user_['id']));
			$result['result']=true;
			$result['id']=$user_['id'];			
		}
		else
		{
			$update_=array('name'=>'','email'=>'','password'=>'','img'=>'','activate'=>1,'created_at'=>date('Y-m-d G:i'),'fb_id'=>$_POST['user']['id'],'activate_code'=>'');		
			if(isset($_POST['user']['email']) and $_POST['user']['email']!='')
				$update_['email']=$db_connectors->sql_escape($_POST['user']['email']);			
			if(isset($_POST['user']['name']) and $_POST['user']['name']!='')
				$update_['name']=$db_connectors->sql_escape($_POST['user']['name']);			
			
			$update_['img']='http://graph.facebook.com/' . $_POST['user']['id'] . '/picture?type=large';			
			$id=$db_connectors->InsertRow('users',$update_);
			
			if($id)
			{
				if(isset($_COOKIE[REFERAL_COOKIE])){
					$db_connectors->InsertRow('referal_user',array('id_user'=>$id,'id_referal_user'=>$_COOKIE[REFERAL_COOKIE],'date_update'=>time()));
					setCookie(REFERAL_COOKIE,0,time()-3600,'/');
				}
				$result['result']=true;
				$result['id']=$id;
			}
		}
		
	}
	return $result;		
	
	
}

function search_($db_connectors,$data)
{
	$user_=$db_connectors->FetchRow('select * from users where fb_id="'.$db_connectors->sql_escape($data['id']).'"');	
	if(!$user_)
	{
		if(isset($data['email']) and $data['email']!='')
		{
			$user_=$db_connectors->FetchRow('select * from users where email="'.$db_connectors->sql_escape($data['email']).'"');
			if($user_)
				return $user_;
		}	
	}
	else return $user_;
	return false;	 
	
}
function recovery_access($db_connectors)
{
	if(isset($_SESSION['recovery']))
		unset($_SESSION['recovery']);
	$code=trim($db_connectors->sql_escape($_GET['code']));
	//$email=trim($db_connectors->sql_escape($_GET['email']));
	if($code=='')
		return false;
	else
	{
		$user=$db_connectors->FetchRow('select * from recovery where key_recovery="'.$code.'"');
		if($user)
		{
			$_SESSION['recovery']=$user;
			return true;
		}
	}
	return false;
}

function repassword($db_connectors)
{
	$result=array('result'=>false);
	if(!isset($_SESSION['recovery']))
		return false;
	else
	{
		$code=@trim($db_connectors->sql_escape($_SESSION['recovery']['key_recovery']));
		//$email=@trim($db_connectors->sql_escape($_SESSION['recovery']['email']));
		$user=$db_connectors->FetchRow('select id_user from recovery where key_recovery="'.$code.'" and id="'.$db_connectors->sql_escape($_SESSION['recovery']['id']).'"');
		if($user)
		{
			$pwd=createPWD($_POST['password']);
			$re_pwd=createPWD($_POST['password_confirmation']);
			if(strlen($_POST['password'])<6)
				$error['password']='Registration-error-passord';
			elseif($re_pwd!=$pwd)
				$error['password_confirmation']='Registration-error-password2';
			if(count($error)==0)
			{
				$arra_insert=array('password'=>$pwd);
				$id=$db_connectors->UpdateRow('users',$arra_insert,array('id'=>$user['id_user']));
				unset($_SESSION['recovery']);
				$db_connectors->DeleteRow('recovery',array('id_user'=>$db_connectors->sql_escape($user['id_user'])));
				$result['result']=true;						
			}
			else
				$result['error']=$error;
			
		}
		return $result;	
				
	}
	return false;
}
function recovery($db_connectors)
{
	if(isset($_POST['email']))
	{
		$email=trim($_POST['email']);		
		$result=array('result'=>false);
		if($email!='')
		{
			$user=$db_connectors->FetchRow('select id from users where email="'.$db_connectors->sql_escape($email).'"');
			if(!$user)		
				$result['error']['email']='This email not found';
			else
			{
				$time=time();
				$hech=md5($user['id'].$time);
				$db_connectors->DeleteRow('recovery',array('id_user'=>$db_connectors->sql_escape($user['id'])));
				$insert=array('id_user'=>$db_connectors->sql_escape($user['id']),'email'=>$db_connectors->sql_escape($email),'date'=>$time,'key_recovery'=>$hech);
				$db_connectors->InsertRow('recovery',$insert);
				
				include 'library/send_mail.php';
				$SEND_MAIL=new SendMail(PATH_TPL.'mail');
				$tp=array('link'=>'/recovery_access?code='.$hech);
				$data_publish=array('title'=>'Link recovery','action'=>'recovery','email'=>$email);
				$SEND_MAIL->send($data_publish,$tp);			
				
				$result['result']=true;
				
				
			}
		}
		else $result['error']['email']='This email not found';
			
		return $result;		
	}	
	return false;	
}

function activate_link_send($db_connectors,$SEND_MAIL)
{
	$email=trim($_POST['email']);		
	$result=array('result'=>false);
	if(isset($_POST['email']))
	{
		$user=$db_connectors->FetchRow('select id from users where email="'.$db_connectors->sql_escape($email).'"');
		if($user)
		{
			$time=time();
			$hech=md5($user['id'].$time);
			$db_connectors->UpdateRow('users',array('activate_code'=>$hech),array('id'=>$user['id']));
			include 'library/send_mail.php';
			$SEND_MAIL=new SendMail(PATH_TPL.'mail');
			$tp=array('link'=>'/activate?code='.$hech);
			$data_publish=array('title'=>'Activate account','action'=>'activate_account','email'=>$email);
			$SEND_MAIL->send($data_publish,$tp);
			$result['text']='To activate the account, a message was sent to your email address with further instructions.</br></br> If the message does not arrive within 20 minutes, check the "Spam" tab.';	
		}
		$result['result']=true;
	}
	return $result;
}
function activate_account($db_connectors)
{
	
	$code=trim($db_connectors->sql_escape($_GET['code']));
	//$email=trim($db_connectors->sql_escape($_GET['email']));
	if($code=='')
		return false;
	$user=$db_connectors->FetchRow('select id from users where activate_code="'.$code.'"');
	if($user)
	{
		$db_connectors->UpdateRow('users',array('activate'=>1,'activate_code'=>''),array('id'=>$user['id']));
		return $user['id'];
	}	
}




?>