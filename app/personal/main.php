<?php

function dateGNU($str,$del='/')
{
	$e=explode($del,$str);
	$s='';
	$s=$e[2].'-'.$e[1].'-'.$e[0];
	return $s;
}


function generateUrl( $text ) 
{
  $text = mb_strtolower( $text, 'UTF-8' );
  $fromSymbol = array(
   ' ', '_', '.', ',', '?', '&', '$', '#', '@', '!', '+', '(', ')', '*', '=', '/', '`', '~', '"', '\'', '^', '<', '>', ':', ';', '/', '{', '}', '№', '%'
  );
  
  $toSymbol = array(
   '-', '-', '-', '-', '', 'and', 's', 'cs', 'at', '', 'plus', '-', '-', '-',  
   '-', '-', '', '-', '', '', '', 
   '', '', '-', '', '-', '-', '-', 'no', ''
  );
  
  $text = str_replace( $fromSymbol, $toSymbol, $text );
  
  $fromLetter =  array( 'й', 'ц',  'у', 'к', 'е', 'н', 'г', 'ш',  'щ',   'з', 'х',  'ъ', 'ф', 'ы', 'в', 'а', 'п', 'р', 'о', 'л', 'д', 'ж', 'э', 'я',  'ч',  'с', 'м', 'и', 'т', 'ь', 'б', 'ю',  'ё',  'ї',  'і', 'є', '‘', 'ª', '¿', '’', 'Ñ', '¨', 'ñ', '¡', '²', 'é', 'è', 'ç', 'à', 'ù', '°', '¨', '£', 'µ',  '§' );
  $toLetter =  array( 'j', 'tc', 'u', 'k', 'e', 'n', 'g', 'sh', 'tsh', 'z', 'kh', '',  'f', 'y', 'v', 'a', 'p', 'r', 'o', 'l', 'd', 'j', 'e', 'ja', 'ch', 's', 'm', 'i', 't', '',  'b', 'ju', 'je', 'ji', 'i', 'e', '',  'a', '',  '',  'n', '',  'n', 'i', '2', 'e', 'e', 'c', 'a', 'u', '',  '',  'f', 'mu', 's' );
  
  $text = str_replace( $fromLetter, $toLetter, $text );
  
  $text = str_replace( '--', '-', $text );
  
  if (strlen($text) > 0 && $text[0]=='-') {
   $text = substr( $text, 1, strlen($text) - 1 );
  }
  
  if (strlen($text) > 0 && $text[strlen($text)-1] == '-') {
   $text = substr( $text, 0, strlen($text) - 1 );
  }
  
  return $text;
  
}
function CatchError($cont)
{
	$content=$cont;
	$d=include PATH_SCRIPT.'/'.MODEL.'/404.php';
	return $d;
}

function PagingSaite($all_count,$count_per_page,$act_page,$link)
{
		//$all_count Всего страниц,
		//$count_per_page 
		//$act_page Активная страница
		//$link	-ссылка
		$returnPaging=array();
		$returnPaging['page_list']=array();
		$returnPaging['activation']=0;
		$nums = $count_per_page;			
		$page = $act_page;			
		$elements = $all_count;
		
		$pages = ceil($elements/$count_per_page);

		if ($pages <= 1) {
			return $returnPaging;
			
		}
		if ($page < 1) {
			return $returnPaging;
			
		}
		elseif ($page > $pages) {
			$page = $pages;
		}		
			
		
		$neighbours = 5;
		
		$left_neighbour = $page - $neighbours;
		if ($left_neighbour < 1) $left_neighbour = 1;

		$right_neighbour = $page + $neighbours;
		if ($right_neighbour > $pages) $right_neighbour = $pages;

		
		if ($page > 1)
		{
			$np=$page-1;
			if($np!=1)
				$returnPaging['left_bar']=array('link'=>$link.'?page='.$np);
			else
				$returnPaging['left_bar']=array('link'=>$link);
		}
			
				
		
		for ($i=$left_neighbour; $i<=$right_neighbour; $i++) {
			if($i!=1)
				$returnPaging['page_list'][$i]=array('link'=>$link.'?page='.$i);
			else	
				$returnPaging['page_list'][$i]=array('link'=>$link);
			if ($i == $page) 	
				$returnPaging['page_list'][$i]['active']=1;
		}

		if ($page < $pages)	
			$returnPaging['right_bar']=array('link'=>$link.'?page='.($page+1)); 
		
		$returnPaging['activation']=1;
		return $returnPaging;
}

function escapeString($str)
{
	$str=str_replace(array('*','select','from'),'',$str);
	$srt=htmlspecialchars(strip_tags($str));
	return $srt;
}
function getUrlLogic()
{	
	$url=$_SERVER['REQUEST_URI'];
	$m=explode('?',$url);
	$url=$m[0];
	if(substr($url,-1)!='/') 
			return false;	
	return true;
	
}
function getUrlAddSlesh()
{	
	$url=$_SERVER['REQUEST_URI'];
	$m=explode('?',$url);
	$url=$m[0];
	if(substr($url,-1)!='/') $url=$url.'/';				
	return $url;	
}
function getUrlDelSlesh()
{	
	$url=$_SERVER['REQUEST_URI'];
	$m=explode('?',$url);
	$url=$m[0];
	if(substr($url,-1)=='/')
	{
		$url=substr($url,0,(strlen($url)-1));
	} 			
	return $url;	
}

function smallImg($ftp_path,$w,$name)
{	
	$name_new=$name.'.jpg';
	$returnPath=false;
	if(!file_exists($ftp_path.'/'.$name_new))
	{
		if(file_exists($ftp_path.'/'.$name))
		{
			$sz = getimagesize($ftp_path.'/'.$name);
			if ($sz)
			{
			  $iw = $sz[0];
			  $ih = $sz[1];
			  //1 = GIF, 2 = JPG, 3 = PNG
			  switch($sz[2])
			  {
				  case 1:
					$im = @imagecreatefromgif($ftp_path.'/'.$name);
				  break;
				  case 2:
					$im = @imagecreatefromjpeg($ftp_path.'/'.$name);
				  break; 
				  case 3:
					$im = @imagecreatefrompng($ftp_path.'/'.$name);
				  break;
				  default:
					$im=false;
				  break;
			  }
				   
			 
				if ($im)
				{
					if ($iw > $w && $w!=0 )
					{
						if($iw>$ih){
							$iw = $iw/($ih/$w);
							$ih=$w;							
						}
						else{

								$ih = $ih*$w/$iw;
								$iw = $w;                       
						}

					}                 
					$iw = (int) $iw;
					$ih = (int) $ih;
					
					$out_im = imagecreatetruecolor($iw,$ih);
					imagecopyresampled($out_im, $im, 0, 0, 0, 0, $iw, $ih, $sz[0], $sz[1]);                
					imagejpeg ($out_im,$ftp_path.'/'.$name_new ,55 );
					imagedestroy($out_im);
					$returnPath=$name_new;	
				}

			}
		}	
		
	}
	

	return $returnPath;
}

function smallImgRectangle($path,$w,$name)
{	
	$ftp_path=PATH_ROOT.$path;	
	
	$name_new=str_replace('.jpg','_rectangle_w5_'.$w.'.jpg',$name);
	
	$postPref=IMG_PATH_SRC;
	//return $postPref.$path.$name;
	$returnPath='';
	if(!file_exists($ftp_path.'/'.$name_new))
	{
		if(file_exists($ftp_path.'/'.$name))
		{
			$sz = getimagesize($ftp_path.'/'.$name);
			if ($sz)
			{
			  $iw = $sz[0];
			  $ih = $sz[1];
			  $im = @imagecreatefromjpeg($ftp_path.'/'.$name);
				if ($im)
				{
					if ($iw > $w && $w!=0 )
					{
						if($iw>$ih){
							$iw = $iw/($ih/$w);
							$ih=$w;							
						}
						else{

								$ih = $ih*$w/$iw;
								$iw = $w;                       
						}

					}                 
					$iw = (int) $iw;
					$ih = (int) $ih;
					
					$out_im = imagecreatetruecolor($iw,$ih);
					imagecopyresampled($out_im, $im, 0, 0, 0, 0, $iw, $ih, $sz[0], $sz[1]);                
					imagejpeg ($out_im,$ftp_path.'/'.$name_new ,85 );
					imagedestroy($out_im);
					
				}

			}
		}	
		
	}
	$returnPath=$postPref.$path.$name_new;

	return $returnPath;
}
	


function createPWD($char)
{
	$sold='GlINa56$%#2';
	$char=md5(strrev($sold.$char).$char);
	return $char;
}
function generatePwd()
{
	$char=uniqid(rand(),1);
	return $char;
}

function getUserInfo($db)
{
	$data=array();
	if(isset($_SESSION[USER_SESSION_NAME]) and intval($_SESSION[USER_SESSION_NAME])>0)
	{
		$data=$db->FetchRow('select * from users where id='.intval($_SESSION[USER_SESSION_NAME]));
		if($data)
		{
			$data['img_isset']=true;
			$update=array();
			/*if($data['password']=='')	
				$data['password']=$update['password']=createPWD(generatePwd());*/
			if($data['password_api']=='')
				$data['password_api']=$update['password_api']=createPWD(generatePwd());
			if(count($update)>0)
				$db->UpdateRow('users',$update,array('id'=>$data['id']));	
			if($data['img']==''){
				$data['img']=DOMEN_NAME.'/css/personal/images/icons/default-avatar.png';
				$data['img_isset']=false;
			}
			else $data['img']=str_replace('https://ico.unifox.io',DOMEN_NAME,$data['img']);
				
			$img_path=PATH_ROOT.'user_file/'.$data['id'];
			if(!file_exists($img_path))
			{
				mkdir($img_path,0777);
				@chmod($img_path,0777);
			}
			$data['img_path_save']=$img_path;
			
			/*if($data['name']=='')
				$data['name']=$data['email'];*/			
			return $data;
		}
			
	}
	return false;	
}
function getUserWallets($API_WALET,$user_info)
{
	$data=array();
	$token=get_tokenUser($API_WALET,$user_info);
	//$CACHE=new Cache_Api(PATH_ROOT.'user_file/'.$user_info['id']);
	$CACHE=new Cache_Api();
	$file_name='wallets_list';
	$walets=$CACHE->getDataCache($file_name);
	if(!$walets)	
	{
	/*FOX API*/
			
		$walets=$API_WALET->get_user_balnace(array('token'=>$token));
		
		if($walets['type']!="success")
		{
			deleteToken();
			$token=get_tokenUser($API_WALET,$user_info);
			if($token)
				$walets=$API_WALET->get_user_balnace(array('token'=>$token));
			else $walets=array();
		}
		$CACHE->setDataCache($file_name,$walets);
	}
	
	$data['fox']=$walets;unset($walets);
	//var_dump($data['fox']);
	/*unicash*/
	$data['unicach']=array('balance'=>'0','wallets'=>$data['fox']['wallets']);	
	
	return $data;
}
function get_tokenUser($API_WALET,$user_info)
{
	$token=false;
	if(isset($_SESSION[USER_SESSION_NAME.'_t']))
		$token=$_SESSION[USER_SESSION_NAME.'_t'];
	else{		
		$token=$API_WALET->get_user($API_WALET->pars_data_user($user_info),true);
		if(!isset($token['Unifox_type']))
			$_SESSION[USER_SESSION_NAME.'_t']=$token;
		else return false;			
	}	
	return $token;		
}
function saveImgFromBase($base64code,$path,$name)
{
	$tmp=explode('base64,',$base64code);
	$base64code = str_replace(' ', '+', $tmp[1]);
	$img_tmp=base64_decode($base64code);	
	file_put_contents($path.$name,$img_tmp);
	unset($img_tmp);
	$img=smallImg($path,2000,$name);
	
	return $img;
}
function deleteToken()
{
	if(isset($_SESSION[USER_SESSION_NAME.'_t']))
		unset($_SESSION[USER_SESSION_NAME.'_t']);
}
function deleteCache($name,$user_info)
{
	//$CACHE=new Cache_Api(PATH_ROOT.'user_file/'.$user_info['id']);
	$CACHE=new Cache_Api();
	$CACHE->removeCache($name);	
}
function saveAvatarFromBase($base64code,$path,$name)
{
	$tmp=explode('base64,',$base64code);
	$base64code = str_replace(' ', '+', $tmp[1]);
	$img_tmp=base64_decode($base64code);	
	file_put_contents($path.$name,$img_tmp);
	unset($img_tmp);
	
	include 'library/Thumbnail.php';
	$IMG_THUMB=new Thumbnail('jpeg');
	$IMG_THUMB->set_square(true);
	return $IMG_THUMB->createImg($path.$name,$path.$name.'.jpg',130);
	
	//$img=smallImgRectangle($path,100,$name);
	
	
}
function set_verificationUser($db,$id_user,$type,$value)
{
	$db->UpdateRow('verification_new',array('is_'.$type=>$value),array('id_user'=>intval($id_user)));
}
function get_verificationUser($db,$id_user,$type='')
{
	$r=$db->FetchRow('select * from verification_new where id_user='.intval($id_user));
	if(!$r)
	{
		$db->InsertRow('verification_new',array('id_user'=>intval($id_user),'is_email'=>1));
		if($type!='')
			return 0;
		else return array('is_email'=>1,'is_document'=>0,'is_adres'=>0,'is_company_info'=>0);
	}
	if($type!='')
		return $r['is_'.$type];
	else return $r;
}
function genereateTransactionCode($id,$user_data)
{
	$sold='Fox_transaction';
	return md5($user_data['id'].$sold.$id);
}
function setWalletUser($db,$id_user,$wallet)
{
  $n=$db->FetchRow('select * from user_wallets where id_user="'.$id_user.'" and wallet="'.$wallet.'"');
  if(!$n)
	 $db->InsertRow('user_wallets',array('id_user'=>$id_user,'wallet'=>$wallet));
} 
function getWalletUser($db,$wallet)
{
	$n=$db->FetchRow('select id_user from user_wallets where  wallet="'.$wallet.'"');
	if($n)
		return $n['id_user'];
	else return false;
	
}
function getTypeBuy($type=false)
{
	$array_type=array(
		'btc'=>array('name'=>'BTC','currency'=>'BTC','currency_form'=>'btc','icon'=>'<i class="fa fa-btc"></i>','min_buy'=>0.01,'active'=>true,'amount_label'=>__tr('personal.Amount in BTC'),'input_placeholder'=>__tr('personal.enter amount in BTC')),			
		'eur'=>array('name'=>'Bank Transfer','currency'=>'EUR','currency_form'=>'eur','icon'=>'<i class="fa fa-eur"></i>','min_buy'=>1,'active'=>true,'amount_label'=>__tr('personal.Amount in EUR'),'input_placeholder'=>__tr('personal.enter amount in EUR')),			
	);
	if($type and isset($array_type[$type]))
		return $array_type[$type];
	return $array_type;
}

function CatchAccessDenied($cont)
{
	$content=$cont;
	$d=include PATH_SCRIPT.'/'.MODEL.'/accessisPage.php';
	return $d;
}
function AdminUrl($user_data,$access=array(1))
{
	$n=array_search($user_data['is_admin'], $access);	
	if($n===FALSE){
		header('Location: /admin/accessis');
		die();
	}
	return true;		
}
function makePager($iCurr, $iEnd, $iLeft, $iRight,$url)
{
	//$return=array('start'=>1,'stop'=>$iEnd,'revolt'=>$iEnd);
	$start=1;
	$stop=$iEnd;
	$last_start=$iEnd-$iRight;
	$last_stop=$iEnd;
	if($iCurr > $iLeft && $iCurr < ($iEnd-$iRight))
	{
		$start=$iCurr-$iLeft;
		$stop=$iCurr+$iRight;		
	}
	elseif($iCurr<=$iLeft)
	{
		$iSlice = 1+$iLeft-$iCurr;
		$stop=$iCurr+($iRight+$iSlice);	
	}
	else
	{
		$iSlice = $iRight-($iEnd - $iCurr);
		$start=$iCurr-($iLeft+$iSlice);
		$stop=$iEnd;	
	}
	if($stop > $iEnd)
		$stop=$iEnd;
	
	$return=array('leftPaging'=>array('start'=>$start,'stop'=>$stop),'rightPaging'=>array('start'=>$last_start,'stop'=>$last_stop),'active_page'=>$iCurr,'count_page'=>$iEnd,'url'=>$url);
	
	if($stop >= $last_start)
	{
		$return['leftPaging']['stop']=$iEnd;		
		$return['rightPaging']=false;	
	}	
	if($stop>=$last_stop){
		$return['rightPaging']=false;
	}
	
	return parserPage('block/paging_native.html',$return);
	
	//return $return;
}
?>