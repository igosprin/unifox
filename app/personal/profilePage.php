<?php
header('Location: /personal/account-confirmation');
$file_key=array('documents_1','documents_2','proff_adres','selfi');
$info_key=array('last_name','name','middle_name','birthdate','citizenship','number','issue_date','expaer_date','country','city','adres');
$img_path=PATH_ROOT.'user_file/'.$content['personal_user_info']['id'];
if(isset($_POST['verification_id']))
{
	
	$result=array('result'=>false);	
	$arrayInsert=array();
	if(!isset($_POST['igree']))
		$result['error']['igree']='Please read the terms';
	foreach($info_key as $v)
	{
		if(!isset($_POST[$v]))
			$_POST[$v]='';		
		if(trim($_POST[$v])=="")
			$result['error'][$v]='Please input correct data';
		else $arrayInsert[$v]=$_POST[$v];
	}
	
	foreach($file_key as $v)
	{
		if(!isset($_POST[$v]))
			$_POST[$v]='';
		if(trim($_POST[$v])=="" and !isset($_POST[$v.'-file']))
			$result['error'][$v]='Please choose file';
			
	}
	
	if(count($result['error'])==0)
	{
		if(!file_exists($img_path))
		{
			mkdir($img_path,0777);
			@chmod($img_path,0777);
		}
			
		/*file*/
		$file_error=false;
		
		foreach($file_key as $v)
		{
			if(!isset($_POST[$v.'-file']))				
			{ 	
				$name=md5($_POST[$v.'-name'].time().$v.$content['personal_user_info']['id']);
				$img=saveImgFromBase($_POST[$v],$img_path.'/',$name);
				if(!$img)
				{
					$file_error=true;
					$result['error'][$v]='Please choose file';
				}
				else
				{
					$array=array('user_name'=>$_POST[$v.'-name'],'file'=>$img);
					$arrayInsert[$v]=json_encode($array);
				}	
			}
			else $file_error=false;
		}
		if(!$file_error)
		{
			$arrayInsert['id_user']=$content['personal_user_info']['id'];
			$arrayInsert['status']=1;
			$arrayInsert['date_update']=time();
			$arrayInsert['wallets']=$_POST['wallets'];
			
			
			if(intval($_POST['verification_id'])==0)
				$db_connectors->InsertRow('verification', $arrayInsert);
			else $db_connectors->UpdateRow('verification', $arrayInsert,array('id'=>intval($_POST['verification_id'])));
			$result['result']=true;
			
		}	
		
		
		
		
	}
	die(json_encode($result));
		
	
}	

$content['title']='My Profile Unifox';
$content['h1']='';
/*script_load*/
$content['script_load']=array(
	'/css/personal/plugins/icheck/icheck.min.js',
	'/js/personal/profile.js',
); 
$content['style_load']=array(
	"/css/personal/plugins/icheck/skins/all.css",
	
); 
$content['url_active']='/personal/profile';

$data=array();
$status=array(
	1=>'Your request has been sent to the moderator for review',
	2=>''
);

$wallets=getUserWallets($API_WALET,$content['personal_user_info']);
$data=$wallets;
$fox=array_keys($wallets['fox']['wallets']);
$data['wallets']=$wallets['fox']['wallets'][$fox[0]];

$data['data']=array('id'=>0);
$data['error']=array();
/*if(isset($_POST['birthdate']))
	$data['data']['birthdate']=$_POST['birthdate'];*/
$data['list_country']=get_country();

$data['verification_button']=true;
$d=get_data_verif($db_connectors,$content['personal_user_info']['id']);
if($d){
	$data['data']=$d;
	$data['data']['documents_1']=json_decode($data['data']['documents_1'],true);
	$data['data']['documents_2']=json_decode($data['data']['documents_2'],true);
	$data['data']['proff_adres']=json_decode($data['data']['proff_adres'],true);
	$data['data']['selfi']=json_decode($data['data']['selfi'],true);
	if($data['data']['status']==1)
		$data['verification_button']=false;
	if($data['data']['status']==2){
		$data['error']=get_error($db_connectors,$content['personal_user_info']['id']);
		
	}
	
}
else 
{
	$data['verification_button']=true;
}

if(!$data['verification_button'])
{
	$content['script_load'][]='/js/personal/profile_disabled.js';
}	
$data['status_program']=__tr('personal.not verified');
if($content['personal_user_info']['is_verification']==1)
	$data['status_program']=__tr('personal.verified');


return parserPage('profile.html',$data,$content);
function get_data_verif($db,$id_user,$id=0)
{
	$sql='select * from verification where id_user='.$id_user;
	if($id>0)
	$sql.=' and id='.$id;	
	return $db->FetchRow($sql);	
}
function get_error($db,$id_user)
{
	$true=array();
	$error_=$db->FetchRow('select * from verification_error where id_user='.$id_user);
	if($error_)
	{
		foreach($error_ as $k=>$v)
		{
			if($v==1)
				$true[$k]=$v;
		}
	}
	$true['comment']=$error_['comment'];
	
	return $true;	
	
}
function get_country()
{
	$file=PATH_ROOT.'/plugin/country/list.json';
	return json_decode(file_get_contents($file),true);
}

?>