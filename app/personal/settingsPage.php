<?php
if(isset($_POST['save_settings']))
{
	$result=array('result'=>false);	
	$repassword=false;
	$email=false;
	/*if($_POST['name']=="")
		$result['error']['name']='Please input name';
	if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
		$result['error']['email']='Please input correct email';
	elseif($_POST['email']!=$content['personal_user_info']['email'])
	{
		$exist=$db_connectors->FetchRow('select count(*) as cc from users where email="'.$db_connectors->sql_escape($_POST['email']).'" and id!='.$content['personal_user_info']['id']);
		if($exist['cc']>0)
			$result['error']['email']='This email address already exists';
		$email=$_POST['email'];		
	}	
		
	if($_POST['password']!="")
	{
		if(strlen($_POST['password'])<6)
			$result['error']['password']='at least 6 characters';
		else
		{
			if($_POST['password']!=$_POST['password-confirm'])
				$result['error']['password-confirm']='Please сonfirm password';
			else
				$repassword=createPWD($_POST['password']);
		}			
	}*/
	$img=false;
	if($_POST['img']!="")
	{
		$name=md5($_POST['img-name'].time().$content['personal_user_info']['id']);		
		$img=saveAvatarFromBase($_POST['img'],$content['personal_user_info']['img_path_save'].'/',$name);
		if(!$img)
			$result['error'][$v]='Please choose file';		
		else
			$img=DOMEN_NAME.'/user_file/'.$content['personal_user_info']['id'].'/'.$img;
			 
	}
	
	if(count($result['error'])==0)
	{
		$update=array();
		$data=array();
		/*if($_POST['name']!=$content['personal_user_info']['name'])
		{	
			$update['name']=$_POST['name'];
			$data['name']=$update['name'];
		}*/
		if($email)
		{
			$res=$API_WALET->change_login(array('token'=>get_tokenUser($API_WALET,$content['personal_user_info']),'login'=>$email));
			if($res['type']=='success'){
				$update['email']=$email;
				$data['email']=$update['name'];
			}
			else{ 
				$result['error']['email']='This email address already exists';
				die(json_encode($result));
			}				
		}
		
		
		
		if($repassword)
			$update['password']=$repassword;
		if($img){
				
			$data['img']=$update['img']=$img;
		}
		
		if(count($update)>0)
		{
			$db_connectors->UpdateRow('users',$update,array('id'=>$content['personal_user_info']['id']));
		}
		//die('asd');
		$result['result']=true;
		$result['data']=$data;
	}	
	
		
	die(json_encode($result));
	
}
if(isset($_POST['passwordform']))
{
	$result=array();
	$result['title']=__tr('personal.Change your account password');
	$result['html']=parserPage('settings/change_password_form.html');
	die(json_encode($result));
}
if(isset($_POST['passwordchange']))
{
	$result=array('result'=>false);
	$pwd_new=createPWD($_POST['newpassword']);
	$re_pwd_new=createPWD($_POST['renewpassword']);
	/*$_POST['cpassword']=trim($_POST['cpassword']);
	$_POST['newpassword']=trim($_POST['newpassword']);
	$_POST['renewpassword']=trim($_POST['renewpassword']);*/
	if(strlen($_POST['cpassword'])==0)
		$result['error']['cpassword']='Please enter current password';
	if(strlen($_POST['newpassword'])==0)
		$result['error']['newpassword']='At least 6 characters';
	elseif($re_pwd_new!=$pwd_new){		
		$result['error']['renewpassword']='Please confirm password';
	}
	if(count($result['error'])==0)
	{
		$pwd=createPWD($_POST['cpassword']);
		$exist=$db_connectors->FetchRow('select id from users where password="'.$pwd.'" and id='.$content['personal_user_info']['id']);	
		if(!$exist)
			$result['error']['cpassword']='Incorrect password';	
		else
		{
			
			$db_connectors->UpdateRow('users',array('password'=>$pwd_new),array('id'=>$content['personal_user_info']['id']));
			$result['result']=true;
		}	
	}		
	
	
	die(json_encode($result));
}



$content['title']='My Settings Unifox';
$content['h1']='';
/*script_load*/
$content['script_load']=array(
	'/js/personal/settings.js'
); 
$content['style_load']=array(
	
	
); 
$content['url_active']='/personal/settings';

$data=array();

$wallets=getUserWallets($API_WALET,$content['personal_user_info']);
$data=$wallets;

$data['list']=array(

	'personal_info'=>array('name','birthdate'),
	'adres'=>array('country','city','adres'),
	'document'=>array('documents_1','documents_2','selfi','proff_adres'),
		
);
$data['list_name']=array('last_name'=>'Last name','name'=>'Name','middle_name'=>'Middle name','birthdate'=>'Birthdate','citizenship'=>'Citizenship','number'=>'Id number','issue_date'=>'Id issue date','expaer_date'=>'Id expire date','country'=>'Country','city'=>'City','adres'=>'Address','documents_1'=>'Frontside','documents_2'=>'Backside','proff_adres'=>'Proof of address','selfi'=>'Selfie with a passport/ID card in hand');
$data['foto_path']=$content['DOMAIN'].'/user_file/'.$content['personal_user_info']['id'];
$data['info']=get_data_verif($db_connectors,$content['personal_user_info']['id']);
if($data['info'])
{
	$data['info']['documents_1']=json_decode($data['info']['documents_1'],true);
	$data['info']['documents_2']=json_decode($data['info']['documents_2'],true);
	$data['info']['proff_adres']=json_decode($data['info']['proff_adres'],true);
	$data['info']['selfi']=json_decode($data['info']['selfi'],true);
}	

return parserPage('settings.html',$data,$content);
function get_data_verif($db,$id_user,$id=0)
{
	$sql='select * from verification where id_user='.$id_user;
	if($id>0)
	$sql.=' and id='.$id;	
	return $db->FetchRow($sql);	
}