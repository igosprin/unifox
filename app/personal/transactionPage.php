<?php

$data=array();/*FOX*/



if(!isset($_SERVER["HTTP_X_REQUESTED_WITH"]))
{
	
	$data=getUserWallets($API_WALET,$content['personal_user_info']);
	foreach($data as $n)
	{
		$data['list_my_vallet']=$n['wallet'];
	}
	$send=array('token'=>get_tokenUser($API_WALET,$content['personal_user_info']));

	$page=1;
	$limit=10;
	if(isset($_GET['page']) and intval($_GET['page'])>1)
		$page=intval($_GET['page']);
	$send['page']=($page-1);
	$send['limit']=$limit;


	$transaction=$API_WALET->get_transaction($send);
	$data['list']=	$transaction['transactions'];
	$data['paging']=array('count_page'=>ceil($transaction['transactions_count']/$limit),'active_page'=>$page,'all_count'=>$transaction['transactions_count']);
	
	
	
	$content['h1']='';
	$content['title']=__tr('personal.My transactions Unifox');
	/*script_load*/
	$content['script_load']=array(	
		//'js/personal/transaction.js',

	);
	$content['style_load']=array(	
	);
	$content['url_active']='/personal/transaction';
	$data['is_ajax']=false;
	return parserPage('transaction.html',$data,$content);
}
elseif($_SERVER["HTTP_X_REQUESTED_WITH"]=="XMLHttpRequest")
{
	
	
	$send=array('token'=>get_tokenUser($API_WALET,$content['personal_user_info']));

	$page=1;
	$limit=10;
	if(isset($_GET['page']) and intval($_GET['page'])>1)
		$page=intval($_GET['page']);
	$send['page']=($page-1);
	$send['limit']=$limit;
	$template='list.html';
	if($_POST['w']=='')
		$transaction=$API_WALET->get_transaction($send);
	else 
	{
		$send['wallet']=$_POST['w'];
		$transaction=$API_WALET->get_transaction_wallet($send);
		$template='list_wallet.html';
	}
	
	$data['status']=array(
		0=>array('name'=>'pending','css'=>'pending'),
		1=>array('name'=>'complete','css'=>'complete'),
	);
	$data['list']=	$transaction['transactions'];
	$data['paging']=array('count_page'=>ceil($transaction['transactions_count']/$limit),'active_page'=>$page,'all_count'=>$transaction['transactions_count']);
	$result=array('result'=>true);
	$data['is_ajax']=true;
	$result['html']=parserPage('transaction/'.$template,$data,$content);
	die(json_encode($result));
}




?>