<?php
ini_set('display_errors','on');
define('CRON_DIRNAME',str_replace(array('/cron','\\cron'),'',dirname(__FILE__)));
include CRON_DIRNAME.'/config.php';
include CRON_DIRNAME.'/library/db_clean.php';
$db =  new Database;
include CRON_DIRNAME.'/library/send_mail.php';
include CRON_DIRNAME.'/library/admin_send.php';
$SEND_ADMIN=new	SendAdmin(PATH_ROOT.'plugin/send-m');
$SEND_MAIL=new SendMail(PATH_TPL.'mail');

$type='verif';
if(isset($_GET['argv']))
	$_SERVER['argv'][1]=$_GET['argv'];	
	
if(isset($_SERVER['argv'][1]))
{
	$type=$_SERVER['argv'][1];
}
$send=false;
$name_title='';
$link='';
switch($type)
{
	case 'verif':
		$send=true;
		$name_title='user info';
		$link='https://ico.unifox.io/admin/';
	break;
	case 'airdrop':
		$send=true;
		$name_title='airdrop';
		$link='https://ico.unifox.io/admin/airdrop/';
	break;
	
}

if($send)
{
	$data=$SEND_ADMIN->getRequest($type);
	$count_request=count($data);
	if($count_request>0)
	{
		$user_admin=$db->FetchAll('select email from users where is_admin=1');		
		if($user_admin)
		{
			$tp=array('count_request'=>$count_request,'link'=>$link);
			$data_publish=array('title'=>'New applications from users('.$name_title.')','action'=>'admin_request_'.$type);
			foreach($user_admin as $user)
			{
				if($user['email']!="")
				{
					$data_publish['email']=$user['email'];
					$SEND_MAIL->send($data_publish,$tp);
				}
				
			}		
		}
        		
		foreach($data as $f)
		{
			$SEND_ADMIN->removeRequest($f,$type);
		}
		
	}	
}



die();