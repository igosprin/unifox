/**
 * @author as3coder
 */
(function() {
  /**
   * Определяем наличие медийных данных пользователя
   * Переопределяем метод получения данных для разных браузеров
   */
  navigator.getUserMedia = ( navigator.getUserMedia ||
                             navigator.webkitGetUserMedia ||
                             navigator.mozGetUserMedia ||
                             navigator.msGetUserMedia);
  //
  var video;
  var canvas;
  var take_button;
  var save_button;
  var message;
  //
  window.onload = function ()
  {
    /**
     * Определяем ссылки на элементы 
     * управления используемые в примере
     */
    video = document.getElementById('video');
    canvas = document.getElementById('canvas');
    take_button = document.getElementById('take_button');
    save_button = document.getElementById('save_button');
    message = document.getElementById('message');
    /**
     * Если нет возможности получить медийные данные, 
     * показываем соответсвующее сообщение и прекратим работу
     */
    if(!navigator.getUserMedia) 
    {
      message.innerHTML = 'Your browser does not support using the camera!';
      take_button.disabled = true;
      return;
    }
    /**
     * Получаем данные от браузера
     * В первом параметре указываем требования для данных
     * В нашем случае, это видео с веб-камеры
     */
    navigator.getUserMedia({ video : true }, 
    /**
     * Обрабатываем событие успешного получения данных
     * Задаем полученый поток видео элементу
     */
    function (stream)
    {
      video.src = window.URL.createObjectURL(stream);
      //---
      take_button.addEventListener('click', OnTakeButtonClick);
      save_button.addEventListener('click', OnSaveButtonClick);
    }, 
    /**
     * Обрабатываем событие отказа при получении данных
     * Покажем соответсвующее сообщение
     */
    function ()
    {
      message.innerHTML = 'No access to the camera';
      take_button.disabled = true;
    });
  }
  /**
   * Обрабатываем событие нажатия на кнопку снимка
   * Отрисовываем на графическом элементе canvas содержимое элемента video
   */
  function OnTakeButtonClick ()
  {
    var width = video.videoWidth;
    var height = video.videoHeight;
    //---
    video.style.width = width + 'px';
    video.style.height = height + 'px';
    //---
    canvas.width = width;
    canvas.height = height;
    //---
    var context = canvas.getContext('2d');
    context.drawImage(video, 0, 0);
    //---
    video.style.width = '';
    video.style.height = '';
    //---
    if(width > height)
    {
      canvas.style.width = '300px';
      canvas.style.height = 300 / width * height + 'px';
    }
    else
    {
      canvas.style.height = '300px';
      canvas.style.width = 300 / height * width + 'px';
    }
    //---
    save_button.disabled = false;
  }
  /**
   * Обрабатываем событие нажатия на кнокпу сохранения изображения
   * Получаем бинарные данные изображения и сохраняем их на диск
   */
  function OnSaveButtonClick ()
  {
    saveAs(base64toblob(canvas.toDataURL('image/png').split(',')[1]), 'image_from_camera.png');
  }
  /**
   * Преобразуем данные изображения из строки Base64 в Blob для сохранения на диск 
   */
  function base64toblob (base64)
  {
    var utf8 = atob(base64),
    array = [];
    //---
    for(var i = 0, j = utf8.length; i < j; i++)
      array.push(utf8.charCodeAt(i));
    //---
    return(new Blob([new Uint8Array(array)], {type: 'image/png'}));
  }
})();