

--UPDATE `roadmap` SET 
--`header`=[value-3],
--`text`=[value-4]
-- WHERE `id` = 2
 -- Задача №82
-----------------------
 
UPDATE `roadmap` SET 
`header`= '10/2016 Zhotovení funkčního Kiosku',
`text`= '<p>Fungující model automatizovaného platebního terminálu se schopností přijímat a vydávat hotovost. Základní softwarové vybavení a dlouhé testování.</p>'
 WHERE `id` = 2;
 
 
UPDATE `roadmap` SET 
`header`= '4/2017 Vize decentralizovaného tržiště',
`text`= '<p>Sepsání ekonomického konceptu tržiště s vlastnostmi blockchainu – 100% nezávislé, decentralizované bez možnosti zastavit transakce a zároveň 100% dostupnost.</p>'
 WHERE `id` =3;
 
UPDATE `roadmap` SET 
`header`= '11/2017 Zhotovení základní verze POS terminálů',
`text`= '<p>Plně fungující verze POS terminálu.</p>'
 WHERE `id` =4;  
 
UPDATE `roadmap` SET 
`header`= '12/2017 Dokončení Unicash.io a unifox blockchainu',
`text`= '<p>Vytvoření stránek Unicash.io a veškerých prezentací + videí.<br>Vytvoření kryptoměny Unicash na Unifox blockchainu.</p>'
 WHERE `id` =5;  
 
UPDATE `roadmap` SET 
`header`= '5/2018 Zveřejnění Whitepaperu',
`text`= '<p>Umístění kompletní technické dokumentace na stránky.
<br>
Přidání Finančních vstupů a výstupů – to vše formou whitepaperu</p>'
 WHERE `id` =6;
 
UPDATE `roadmap` SET 
`header`= '25/5/2018 UniFox meeting pro investory a partnery',
`text`= '<p></p>'
 WHERE `id` =7;  
 
UPDATE `roadmap` SET 
`header`= '6/2018 Presale událost',
`text`= '<p>UniFox projekt v měsíci červnu startuje výběr privátních investic do projektu do výše 2 milionů $. Souhrnná investovaná částka do projektu tak přesáhne 9 milionů dolarů.</p>'
 WHERE `id` = 8; 
 
UPDATE `roadmap` SET 
`header`= '7/2018 Propojení všech částí Unicash a start',
`text`= '<p>Zapojení Unicash kryptoměny do všech složek projektu a oficiální spuštěni Unicash infrastruktury k Burze.</p>'
 WHERE `id` = 9; 
 
UPDATE `roadmap` SET 
`header`= 'Q4/2018 ICO UniFox',
`text`= '<p>UniFox hlavní veřejný prodej mincí za účelem nabídnout uživatelům podíl v celé myšlence UniFox.
<br><br>
Soft Cap: 20 000 000 $<br>
Hard Cap: 100 000 000 $</p>'
 WHERE `id` = 10; 
 
UPDATE `roadmap` SET 
`header`= '12/2018 Zapojení UniFox peněženky',
`text`= '<p>Veškeré tokeny jsou nahrány na UniFox blockchain peněženku a mohou tedy být i volně obchodovány.</p>'
 WHERE `id` =  11;
 
UPDATE `roadmap` SET 
`header`= '3/2019 Dokončení testovací verze burzy',
`text`= '<p>Spuštění prvního provozu systému za účelem odladit veškeré chyby a garantovat uživatelsky příjemné prostředí k dokonalosti.</p>'
 WHERE `id` =  12;
 
UPDATE `roadmap` SET 
`header`= '6/2019 UniFoxBurza Plné spuštění',
`text`= '<p>Otevření plné verze UniFox blockchainu zahrnující otevření největší burzy pro digitální měny na světě. Otevření burzy zahrnuje napojení celé infrastruktury Bankomaty, Směnárny, POS terminály, Unicash.</p>'
 WHERE `id` = 13; 
 
UPDATE `roadmap` SET 
`header`= '9/2019 Dokončení hardware burzy',
`text`= '<p>Dokončení hardware části, která zajistí možnost uživatelům skladovat jakoukoliv kryptoměnu v jedné hardware peněžence a možnost tyto měny obchodovat (skrze aplikaci).</p>'
 WHERE `id` = 14; 
 
UPDATE `roadmap` SET 
`header`= '2020 Získání postu vedoucí světové burzy',
`text`= '<p>Dosažení světové majority v rámci směn virtuálních měn na základě dokonalého zpracování
<br><br>
Podíl 10% na světovém trhu virtuálních měn.</p>'
 WHERE `id` = 15; 
 
 DELETE FROM `roadmap` WHERE id = 16;
 DELETE FROM `roadmap` WHERE id = 17;



 -- faq changes
UPDATE `faq` SET
`question`= 'Jak nakoupit FOX tokeny?',
`answer`= '<p>Je to jednoduché, registrujte se ZDE. Ve vašem backoffice si založíte peněženku pro držení FOX tokenů. Následně stačí jen odeslat libovolnou částku na uvedenou adresu.<br><br><strong>Pozor! Nikdy neposílejte peníze na jinou adresu než na tu, kterou uvidíte ve svém backoffice!</strong></p>'
 WHERE id = 2;

UPDATE `faq` SET
`question`= 'Kde najdu více podrobných informací o tom, jak přesně funguje UniFox?',
`answer`= '<p>Veškeré souvislosti, které mohou zajímat toho, kdo investuje do FOX tokenu, naleznete v obecném Whitepaperu UniFox zde.
<br><br>
Technické informace týkající se jednotlivých částí si můžete nalézt v záložce dokumentace.</p>'
 WHERE id = 3;

UPDATE `faq` SET
`question`= 'Čím je dána hodnota FOX tokenů?',
`answer`= '<p>Tím, že FOX kryptoměna znamená podíl na celém projektu, lze její hodnotu brát jako digitální podíl na celém projektu.
<br><br>
Zásadní růst ceny však nastává, když FOX kryptoměna začne být využívána na poplatky v blockchainu a veškeré služby – poptávka po službách společnosti vyvolá poptávku po FOX kryptoměně, která přímo způsobí růst tržní ceny.</p>'
 WHERE id = 4;

UPDATE `faq` SET
`question`= 'Mohu se nějak přesvědčit o existenci projektu a jeho zakladatelů?',
`answer`= '<p>Jistě, je možné nás navštívit a prohlídnout si naše produkty osobně, přímo v naší kanceláři.
<br><br>
Je vhodné kontaktovat s touto žádostí náší podporu.</p>'
 WHERE id = 7;

UPDATE `faq` SET
`question`= 'Mohu se účastnit ICO, i když nemám prostředky na nákup?',
`answer`= '<p>Ano, je to možné. ICO Unifox nabízí možnost participovat se skrze tyto programy:
<br><br>
<ul>
	<li>Bounty: umožňující získat FOX tokeny za propagaci projektu veřejně</li>
	<li>Airdrop: možnost získat nějaké FOX tokeny jako registrační bonus:</li>
	<li>Affiliate program (možnost získat tokeny za přímé doporučení)</li>
</ul><br><br>
Veškeré bonusy z těchto programů budou vypláceny až po ICO. Zaregistrujte se a zjistěte více informací ve svém backoffice.</p>'
 WHERE id = 8;


UPDATE `faq` SET `answer` = '<p>\r\nYes, it is possible. ICO UniFox offers the opportunity to participate in the following programs:<br>\r\n<ul>\r\n<li>Bounty: opportunity to get FOX tokens by promoting the project publicly</li>\r\n<li>Airdrop: opportunity to get FOX tokens as a signup bonus</li>\r\n<li>Affiliate program (opportunity to get tokens for direct referrals)</li>\r\n</ul>\r\n<br>\r\nAll bonuses from these programs will be paid out as soon as the ICO is finished. Sign up and find out more information in your back office.\r\n</p>' WHERE `faq`.`id` = 15;
UPDATE `faq` SET `answer` = '<p>\r\nДа, это возможно. ICO UniFox предлагает возможность участвовать в следующих программах:\r\n<br>\r\n<ul>\r\n<li>Баунти: возможность получать FOX токены, публично продвигая проект</li>\r\n<li>Airdrop: возможность получить FOX токены в качестве бонуса регистрации</li>\r\n<li>Партнерская программа (возможность получить токены с помощью реферальных ссылок)</li>\r\n</ul>\r\n<br>\r\nВсе бонусы будут выплачены, сразу как только ICO будет завершено. Зарегистрируйтесь и узнайте больше информации в своем кабинете.\r\n</p>' WHERE `faq`.`id` = 22;

UPDATE `faq` SET `answer` = '<p>\r\nДа, это возможно. ICO UniFox предлагает возможность участвовать в следующих программах:\r\n<br>\r\n<ul>\r\n<li>Баунти: возможность получать FOX токены, публично продвигая проект</li>\r\n<li>Airdrop: возможность получить FOX токены в качестве бонуса регистрации</li>\r\n<li>Партнерская программа (возможность получить токены с помощью реферальных ссылок)</li>\r\n</ul>\r\n<br>\r\nВсе бонусы будут выплачены, сразу как только ICO будет завершено. Зарегистрируйтесь и узнайте больше информации в <a href=\"/personal\" >своем кабинете</a>.\r\n</p>' WHERE `faq`.`id` = 22;
UPDATE `faq` SET `answer` = '<p>\r\nYes, it is possible. ICO UniFox offers the opportunity to participate in the following programs:<br><br>\r\n<ul>\r\n<li>Bounty: opportunity to get FOX tokens by promoting the project publicly</li>\r\n<li>Airdrop: opportunity to get FOX tokens as a signup bonus</li>\r\n<li>Affiliate program (opportunity to get tokens for direct referrals)</li>\r\n</ul>\r\n<br><br>\r\nAll bonuses from these programs will be paid out as soon as the ICO is finished. Sign up and find out more information in your <a href=\"/personal\" >back office</a>.\r\n</p>' WHERE `faq`.`id` = 15;
UPDATE `faq` SET `answer` = '<p>Ano, je to možné. ICO Unifox nabízí možnost participovat se skrze tyto programy:\r\n<br><br>\r\n<ul>\r\n <li>Bounty: umožňující získat FOX tokeny za propagaci projektu veřejně</li>\r\n <li>Airdrop: možnost získat nějaké FOX tokeny jako registrační bonus:</li>\r\n <li>Affiliate program (možnost získat tokeny za přímé doporučení)</li>\r\n</ul><br><br>\r\nVeškeré bonusy z těchto programů budou vypláceny až po ICO. Zaregistrujte se a zjistěte více informací ve <a href=\"/personal\">svém backoffice</a>.</p>' WHERE `faq`.`id` = 8;

UPDATE `faq` SET `answer` = '<p>All information that may interest those who want to invests in the FOX token can be found in the UniFox Whitepaper <a href=\"/documents\" >here</a>.</p><p>You can find technical information for individual parts on the <a href=\"/documents\" >DOCUMENTS</a> page</p>' WHERE `faq`.`id` = 10;
UPDATE `faq` SET `answer` = '<p>Veškeré souvislosti, které mohou zajímat toho, kdo investuje do FOX tokenu, naleznete v obecném Whitepaperu UniFox <a href=\"/cz/documents\" >zde</a>.\r\n<br><br>\r\nTechnické informace týkající se jednotlivých částí si můžete nalézt v záložce <a href=\"/cz/documents\" >dokumentace</a>.</p>' WHERE `faq`.`id` = 3;
UPDATE `faq` SET `answer` = '<p>Вся информация, которая может заинтересовать тех, кто хочет инвестировать в FOX токен, можно найти в документе UniFox перейдя по этой <a href=\"/ru/documents\" >ссылке</a>. </p><p>Вы также можете найти техническую информацию для отдельных частей наших продуктов на странице <a href=\"/ru/documents\" >ДОКУМЕНТАЦИЯ</a>.</p>' WHERE `faq`.`id` = 17;




UPDATE `roadmap` SET
`header`= '10/2016 Fabrication of a functional kiosk',
`text`= '<p>A working model of an automated payment terminal with the ability to receive and issue cash. Basic software equipment and long testing.</p>'
 WHERE `id` = 18;


UPDATE `roadmap` SET
`header`= '4/2017 The vision of the decentralized marketplace',
`text`= '<p>A written form of the economic concept of a marketplace with blockchain features – 100% independent, decentralized, without the ability to stop transactions and 100% accessible.</p>'
 WHERE `id` = 19;


UPDATE `roadmap` SET
`header`= '11/2017 Basic version of POS terminals',
`text`= '<p>A fully functional version of the POS terminal</p>'
 WHERE `id` = 20;


UPDATE `roadmap` SET
`header`= '12/2017 Unicash.io and Unifox blockchain completion',
`text`= '<p>Unicash.io web page and all presentations + videos.
<br><br>
Unicash cryptocurrency and Unifox blockchain basic version.</p>'
 WHERE `id` = 21;


UPDATE `roadmap` SET
`header`= '5/2018 Whitepaper release',
`text`= '<p>Uploading of complete technical documentation to the website. Adding financial inputs and outputs – all within the whitepaper.</p>'
 WHERE `id` = 22;


UPDATE `roadmap` SET
`header`= '25/5/2018 UniFox meeting for all investors and partners',
`text`= '<p></p>'
 WHERE `id` = 23;


UPDATE `roadmap` SET
`header`= '6/2018 Start of the presale event',
`text`= '<p>Unifox presale is going to raise 2 million dollars from private investors. The total invested value in the project will be over 9 million dollars.</p>'
 WHERE `id` = 24;


UPDATE `roadmap` SET
`header`= '7/2018 Interconnection of all Unicash parts and launch',
`text`= '<p>Implementation of a Unicash cryptocurrency into all parts of the project and the official launch of Unicash infrastructure to the market.</p>'
 WHERE `id` = 25;


UPDATE `roadmap` SET
`header`= 'Q4/2018 ICO UniFox',
`text`= '<p>UniFox main public sale with the intent to offer users a share in the entire idea of UniFox.
<br>Soft Cap: 20 000 000 $
<br>Hard Cap: 100 000 000 $</p>'
 WHERE `id` = 26;


UPDATE `roadmap` SET
`header`= '12/2018 UniFox Wallet Connection',
`text`= '<p>All tokens are uploaded to the UniFox blockchain wallet. Therefore, they can be freely traded.</p>'
 WHERE `id` = 27;


UPDATE `roadmap` SET
`header`= '3/2019 Completion of the test version of the Exchange',
`text`= '<p>The launch of the first system in order to debug and improve the user-friendly environment to perfection.</p>'
 WHERE `id` = 28;


UPDATE `roadmap` SET
`header`= '6/2019 UniFox Exchange Full Launch',
`text`= '<p>The opening of a full version of the UniFox blockchain, including the opening of the biggest exchange for the digital currency. The opening of the Exchange includes the connection of the whole infrastructure – ATMs, Exchange Offices, POS terminals.</p>'
 WHERE `id` = 29;


UPDATE `roadmap` SET
`header`= '9/2019 Completion of  Exchange hardware',
`text`= '<p>Completion of a hardware part to ensure that users can store any cryptocurrency in one wallet + the ability to trade these currencies (using the app).</p>'
 WHERE `id` = 30;


UPDATE `roadmap` SET
`header`= '2020 Gaining a lead position as the Worldwide Exchange',
`text`= '<p>
Achieving a worldwide majority in virtual exchange currencies thanks to perfect development.
<br><br>
The 10% share of the global virtual currency market.
</p>'
 WHERE `id` = 31;

 DELETE FROM `roadmap` WHERE `roadmap`.`id` = 32;
 DELETE FROM `roadmap` WHERE `roadmap`.`id` = 33;


UPDATE `roadmap` SET
`header`= '10/2016 Изготовление функционального киоска',
`text`= '<p>Рабочая модель автоматизированного платежного терминала с возможностью получения и выдачи наличных. Основное программное обеспечение.</p>'
 WHERE `id` = 34;

UPDATE `roadmap` SET
`header`= '4/2017 Видение децентрализованного рынка',
`text`= '<p>Письменная форма экономической концепции рынка с блокчейн функционалом – 100% независимый, децентрализованный и 100% доступный рынок.</p>'
 WHERE `id` = 35;



UPDATE `roadmap` SET
`header`= '7/2017 Рабочий обменный пункт',
`text`= '<p>Рабочий проект обмена валюты – приложение с нужным функционалом. Полуавтоматизированная версия.</p>'
 WHERE `id` = 36;


UPDATE `roadmap` SET
`header`= '11/2017 Базовая версия POS-терминала',
`text`= '<p>Полностью функциональная версия POS-терминала.</p>'
 WHERE `id` = 37;


UPDATE `roadmap` SET
`header`= '12/2017 Завершение подготовки проекта Unicash',
`text`= '<p>Завершение сайта Unicash.io вместе с презентацией и видео.
<br><br>
Создание криптовалюты Unicash на собственном блокчейне UniFox.</p>'
 WHERE `id` = 38;


UPDATE `roadmap` SET
`header`= '5/2018 Публикация Whitepaper',
`text`= '<p>Загрузка полной технической документации на наш сайт (вместе с финансовыми входами и выходами) на наш сайт.</p>'
 WHERE `id` = 39;


UPDATE `roadmap` SET
`header`= '25/5/2018 UniFox конференция для всех инвесторов и партнеров',
`text`= '<p></p>'
 WHERE `id` = 40;


UPDATE `roadmap` SET
`header`= '6/2018 Предпродажа монет',
`text`= '<p>UniFox препродажа с целью привлечь 2 млн долларов от частных инвесторов. Общая инвестированная стоимость в проект в итоге составит более 9 миллионов долларов.</p>'
 WHERE `id` = 41;


UPDATE `roadmap` SET
`header`= '6/2018 Взаимоподключение всех компонентов Unicash и запуск',
`text`= '<p>Внедрение криптовалюты Unicash во все части проекта и официальный запуск инфраструктуры Unicash на рынок.</p>'
 WHERE `id` = 42;


UPDATE `roadmap` SET
`header`= '9/2018 ICO UniFox',
`text`= '<p>Общая публичная продажа монет с целью предложить пользователям долю в идее UniFox.
<br><br>
Soft Cap: 20 000 000 $
<br><br>
Hard Cap: 100 000 000 $</p>'
 WHERE `id` = 43;


UPDATE `roadmap` SET
`header`= '12/2018 Подключения UniFox кошелька',
`text`= '<p>Все токены загружаются в блокчейн кошелек UniFox. Поэтому они могут торговаться без всяких ограничений.</p>'
 WHERE `id` = 44;


UPDATE `roadmap` SET
`header`= '3/2019 Завершение тестовой версии биржи UniFox',
`text`= '<p>Первый запуск системы в целях отладки и улучшения пользовательской среды и системы в целом.</p>'
 WHERE `id` = 45;


UPDATE `roadmap` SET
`header`= '6/2019 Полный запуск биржи UniFox',
`text`= '<p>Запуск полной версии блокчейна UniFox, включая открытие крупнейшей биржи для цифровых валют. Открытие биржи включает в себя подключение всей инфраструктуры – банкоматы, обменники, POS-терминалы.</p>'
 WHERE `id` = 46;


UPDATE `roadmap` SET
`header`= '9/2019 Завершение аппаратного обеспечения',
`text`= '<p>Завершение аппаратной части, которая обеспечит пользователям хранение любых криптовалют в одном кошельке + возможность обмена этими валютами (с помощью приложения)</p>'
 WHERE `id` = 47;


UPDATE `roadmap` SET
`header`= '2020 Достижение лидирующий позиции на мировом рынке',
`text`= '<p>Достижение всемирного лидерства в сфере виртуальных валют благодаря идеальному развитию.
<br><br>
10% доля в глобальном рынке виртуальных валют.</p>'
 WHERE `id` = 48;

  DELETE FROM `roadmap` WHERE `roadmap`.`id` = 49;






