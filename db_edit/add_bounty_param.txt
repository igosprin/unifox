INSERT INTO `bounty_list` ( `name`, `comment`, `stakes`, `type`, `row_key`) VALUES
('POST(For telegram channel owners)', '- Create a post about Unifox and pin it in your channel<br/>\r\n- The channel has to have at least 500 members<br/>\r\n- Any language is allowed<br/>\r\n- Share your channels URL<br/>\r\n500 members � 2 stakes/post<br/>\r\n2000 members � 4 stakes<br/>\r\n4000 members � 6 stakes<br/>\r\n8000 members � 8 stakes<br/>\r\n16 000 members � 15 stakes', 0, 'Telegram', 'telegram_1'),
('Join Unifox official telegram group', '2 stakes (one time)', 2, 'Telegram', 'telegram_2'),
('Add �Unifox ICO�  to your name (F.E. monkey55 - Unifox ICO)', '20 stakes (one time)', 20, 'Telegram', 'telegram_3'),
('Change your profile picture to Unifox logo', '50 stakes (one time)', 50, 'Telegram', 'telegram_4');
COMMIT;