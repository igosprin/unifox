
CREATE TABLE `faq_affiliate` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `position` int(11) NOT NULL,
  `lang` varchar(4) NOT NULL DEFAULT 'en'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `faq_affiliate`
--

INSERT INTO `faq_affiliate` (`id`, `question`, `answer`, `position`, `lang`) VALUES
(1, 'When will I get all FOX tokens from the Airdrop program?', 'We will begin sending procedure of all FOX tokens as soon ICO is finished.', 1, 'en'),
(2, 'How many tokens are prepared for Airdrop program?', '2 % of all  FOX tokens are reserved solely for Airdrop.', 2, 'en'),
(3, 'Is there any limit for how many people can register via my affiliate link?', 'No. You can invite as many people as you want.', 3, 'en');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `faq_affiliate`
--
ALTER TABLE `faq_affiliate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lang` (`lang`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `faq_affiliate`
--
ALTER TABLE `faq_affiliate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;