-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 19 2018 г., 13:36
-- Версия сервера: 5.7.20-log
-- Версия PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `unifox`
--

-- --------------------------------------------------------

--
-- Структура таблицы `meetings`
--

CREATE TABLE `meetings` (
  `id` int(11) NOT NULL,
  `mdate` date NOT NULL,
  `name` text NOT NULL,
  `link` text NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `meetings`
--

INSERT INTO `meetings` (`id`, `mdate`, `name`, `link`, `image`) VALUES
(1, '2018-05-17', 'Blockchain & Bitcoin Conference', 'https://prague.bc.events/', '\\images\\meetings\\1.png'),
(2, '2018-06-11', 'MoneyConf', 'https://moneyconf.com/', '\\images\\meetings\\2.png'),
(3, '2018-07-06', 'Crypto Economy ICO 2018', 'https://www.thecryptoeconomy.com/calendar/', '\\images\\meetings\\3.png'),
(4, '2018-08-17', 'BlockVienna', 'https://www.blackarrowconferences.com/blockvienna.html', '\\images\\meetings\\4.png'),
(5, '2018-08-24', 'Summit blockchain', 'http://blc-summit.com/en', '\\images\\meetings\\5.png'),
(6, '2018-08-28', 'Blockchain summit', 'http://blockchainsummitsingapore.com', '\\images\\meetings\\6.png'),
(7, '2018-11-16', 'Crypto Economy ICO 2018', 'https://www.thecryptoeconomy.com/calendar/', '\\images\\meetings\\7.png'),
(8, '2018-10-18', 'Blockchain summit', 'http://www.blockchainsummitsanfrancisco.com', '\\images\\meetings\\8.png');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `meetings`
--
ALTER TABLE `meetings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
