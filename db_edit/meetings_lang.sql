-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 19 2018 г., 13:36
-- Версия сервера: 5.7.20-log
-- Версия PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `unifox`
--

-- --------------------------------------------------------

--
-- Структура таблицы `meetings_lang`
--

CREATE TABLE `meetings_lang` (
  `id` int(11) NOT NULL,
  `meeting_id` int(11) NOT NULL,
  `city` varchar(255) NOT NULL,
  `lang` set('ru','en','cz') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `meetings_lang`
--

INSERT INTO `meetings_lang` (`id`, `meeting_id`, `city`, `lang`) VALUES
(2, 1, 'Прага', 'ru'),
(3, 1, 'Praha', 'cz'),
(4, 1, 'Prague', 'en'),
(5, 2, 'Дублин', 'ru'),
(6, 2, 'Dublin', 'en'),
(7, 2, 'Dublin', 'cz'),
(8, 3, 'Лондон', 'ru'),
(9, 3, 'London', 'en'),
(10, 3, 'Londýn', 'cz'),
(11, 4, 'Вена', 'ru'),
(12, 4, 'Vienna', 'en'),
(13, 4, 'Vídeň', 'cz'),
(14, 5, 'Шанхай', 'ru'),
(15, 5, 'Shanghai', 'en'),
(16, 5, 'Šanghaj', 'cz'),
(18, 6, 'Сингапур', 'ru'),
(20, 6, 'Singapore', 'en'),
(23, 7, 'Барселона', 'ru'),
(22, 6, 'Singapur', 'cz'),
(24, 7, 'Barcelona', 'en'),
(25, 7, 'Barcelona', 'cz'),
(26, 8, 'Сан-Франциско', 'ru'),
(27, 8, 'San Francisco', 'en'),
(28, 8, 'San Francisco', 'cz');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `meetings_lang`
--
ALTER TABLE `meetings_lang`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `meetings_lang`
--
ALTER TABLE `meetings_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
