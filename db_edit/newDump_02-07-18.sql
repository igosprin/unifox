-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 02 2018 г., 08:49
-- Версия сервера: 5.6.38
-- Версия PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- База данных: `unifox`
--

-- --------------------------------------------------------

--
-- Структура таблицы `affilate_stakes`
--

CREATE TABLE `affilate_stakes` (
  `id_user` bigint(20) NOT NULL,
  `stakes` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `cheking_user` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



-- --------------------------------------------------------

--
-- Структура таблицы `airpod`
--

CREATE TABLE `airpod` (
  `id` int(11) NOT NULL,
  `fb_link` varchar(255) NOT NULL,
  `telegram_link` varchar(255) NOT NULL,
  `twitter_link` varchar(255) NOT NULL,
  `youtube_link` varchar(255) NOT NULL,
  `rebit_link` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `date_update` int(11) NOT NULL,
  `wallets` varchar(255) NOT NULL,
  `checking_user` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



-- --------------------------------------------------------

--
-- Структура таблицы `airpod_error`
--

CREATE TABLE `airpod_error` (
  `id` int(11) NOT NULL,
  `fb_link` varchar(255) NOT NULL,
  `telegram_link` varchar(255) NOT NULL,
  `twitter_link` varchar(255) NOT NULL,
  `youtube_link` varchar(255) NOT NULL,
  `rebit_link` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



-- --------------------------------------------------------

--
-- Структура таблицы `bounty_list`
--

CREATE TABLE `bounty_list` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `comment` text NOT NULL,
  `stakes` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `row_key` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `bounty_list`
--

INSERT INTO `bounty_list` (`id`, `name`, `comment`, `stakes`, `type`, `row_key`) VALUES
(21, 'REPOST Unifox’s official Facebook posts', '200 - 999 friends – 2 stakes<br> 1000 - 2999 friends  - 4 stakes<br> 3000 - 4999 friends -   6 stakes<br> 5000+ friends – 10 stakes', 0, 'Facebook', 'facebook_1'),
(22, 'Post a comment under an official Facebook post', '1 comment = 5 stakes', 5, 'Facebook', 'facebook_2'),
(23, 'POST YOUR OWN POST TO CRYPTO RELATED GROUPS', '1 post = 2 stakes<br/>- write your own post about Unifox to the crypto related groups and earn 2 stakes per post<br> - low quality or fake posts will not be accepted<br> - no daily limits for posts<br> - feel free to use your unique referral link and earn even more stakes!<br>', 2, 'Facebook', 'facebook_3'),
(29, 'Change your profile picture to unifox logo', '50 stakes/one time\r\nChange your Facebook profile picture to Unifox logo and earn 30 stakes\r\nYou MUST NOT change the logo until the end of a bounty campaign\r\nThose who will change or remove UNIFOX logo before the end of the campaign will be disqualified', 50, 'Facebook', 'facebook_9'),
(30, 'Subscribe to our YouTube channel', 'Participant channel must have at least 2000 subscribers.<br>\r\nWatch our official video<br>\r\nParticipant MUST NOT remove his bounty video until the end of a bounty campaign<br>\r\n<br>\r\n2000+ subscribers will get 50 stakes.<br>\r\n4000+ subscribers will get 100 stakes.<br>\r\n8000+ subscribers will get 150 stakes.<br>\r\n16 000+ subscribers will get 200 stakes.<br>\r\nNote: 25 bonus stakes each 1500 views.', 0, 'Youtube', 'youtube_1'),
(33, 'ReTweet about Unifox', '200 followers – 2 stakes\r\n1000 followers  - 4 stakes\r\n3000 followers -   6 stakes\r\n5000 followers – 10 stakes', 0, 'Twitter', 'twitter_1'),
(34, 'Tweet about Unifox', '500 followers – 3 stakes\r\n1000 followers  - 6 stakes\r\n3000 followers -   9 stakes\r\n5000 followers – 15 stakes', 0, 'Twitter', 'twitter_2'),
(35, 'Change your profile picture to UNIFOX logo', '50 stakes/one time', 50, 'Twitter', 'twitter_3'),
(36, 'Add \"Unifox ICO\" to your Twitter name', '40 stakes/one time', 40, 'Twitter', 'twitter_4'),
(37, 'POST(For telegram channel owners)', '- Create a post about Unifox and pin it in your channel<br/>\r\n- The channel has to have at least 500 members<br/>\r\n- Any language is allowed<br/>\r\n- Share your channels URL<br/>\r\n500 members – 2 stakes/post<br/>\r\n2000 members – 4 stakes<br/>\r\n4000 members – 6 stakes<br/>\r\n8000 members – 8 stakes<br/>\r\n16 000 members – 15 stakes', 0, 'Telegram', 'telegram_1'),
(38, 'Join Unifox official telegram group', '2 stakes (one time)', 2, 'Telegram', 'telegram_2'),
(39, 'Add „Unifox ICO“  to your name (F.E. monkey55 - Unifox ICO)', '20 stakes (one time)', 20, 'Telegram', 'telegram_3'),
(40, 'Change your profile picture to Unifox logo', '50 stakes (one time)', 50, 'Telegram', 'telegram_4');

-- --------------------------------------------------------

--
-- Структура таблицы `bounty_program`
--

CREATE TABLE `bounty_program` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `row_key` varchar(10) NOT NULL,
  `val` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_update` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `stakes` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



-- --------------------------------------------------------

--
-- Структура таблицы `email_verification_code`
--

CREATE TABLE `email_verification_code` (
  `id_user` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `date` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `new` tinyint(3) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `email_verification_code`
--

INSERT INTO `email_verification_code` (`id_user`, `code`, `date`, `email`, `new`) VALUES
(9, '1C211BA8AF69138D4C12CC9F0072B58B', 1528985944, '', 0),
(17, '78CF2C6886D1FCB58B4C018EF8EFE003', 1529063904, 'richters@fason.gg', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `position` int(11) NOT NULL,
  `lang` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `faq`
--

INSERT INTO `faq` (`id`, `question`, `answer`, `position`, `lang`) VALUES
(2, 'Jak nakoupit FOX tokeny?', '<p>Je to jednoduché, registrujte se ZDE. Ve vašem backoffice si založíte peněženku pro držení FOX tokenů. Následně stačí jen odeslat libovolnou částku na uvedenou adresu.<br><br><strong>Pozor! Nikdy neposílejte peníze na jinou adresu než na tu, kterou uvidíte ve svém backoffice!</strong></p>', 1, 'cz'),
(3, 'Kde najdu více podrobných informací o tom, jak přesně funguje UniFox?', '<p>Veškeré souvislosti, které mohou zajímat toho, kdo investuje do FOX tokenu, naleznete v obecném Whitepaperu UniFox <a href=\"/cz/documents\" >zde</a>.\r\n<br><br>\r\nTechnické informace týkající se jednotlivých částí si můžete nalézt v záložce <a href=\"/cz/documents\" >dokumentace</a>.</p>', 2, 'cz'),
(4, 'Čím je dána hodnota FOX tokenů?', '<p>Tím, že FOX kryptoměna znamená podíl na celém projektu, lze její hodnotu brát jako digitální podíl na celém projektu.\r\n<br><br>\r\nZásadní růst ceny však nastává, když FOX kryptoměna začne být využívána na poplatky v blockchainu a veškeré služby – poptávka po službách společnosti vyvolá poptávku po FOX kryptoměně, která přímo způsobí růst tržní ceny.</p>', 3, 'cz'),
(5, 'V čem je UniFox ICO výjimečné?', '<p>FOX kryptoměna, která je předmětem UniFox ICO, má reálnou využitelnost pro placení\r\n						poplatků za služby, které společnost prostřednictvím svých technologií nabízí. A to dává\r\n						kryptoměně opravdový smysl!<br/><br/>\r\n\r\n						Kombinace vlastností security a Utility tokenu s vlastní blockchain technologií je rarita mezi\r\n						ICOs.\r\n					</p>', 4, 'cz'),
(6, 'Jaká je distribuce micní?', '<p>Tým zakladatelů se rozhodl uvolnit mezi partnery 75% veškerého podílu – veškerých mincí.<br/><br/>\r\n						\r\n						<img style=\"width:600px;margin-bottom:20px;\" src=\"/images/ansver_graf.jpg\" />		\r\n						\r\n					</p>', 5, 'cz'),
(7, 'Mohu se nějak přesvědčit o existenci projektu a jeho zakladatelů?', '<p>Jistě, je možné nás navštívit a prohlídnout si naše produkty osobně, přímo v naší kanceláři.\r\n<br><br>\r\nJe vhodné kontaktovat s touto žádostí náší podporu.</p>', 6, 'cz'),
(8, 'Mohu se účastnit ICO, i když nemám prostředky na nákup?', '<p>Ano, je to možné. ICO Unifox nabízí možnost participovat se skrze tyto programy:\r\n<br><br>\r\n<ul>\r\n <li>Bounty: umožňující získat FOX tokeny za propagaci projektu veřejně</li>\r\n <li>Airdrop: možnost získat nějaké FOX tokeny jako registrační bonus:</li>\r\n <li>Affiliate program (možnost získat tokeny za přímé doporučení)</li>\r\n</ul><br><br>\r\nVeškeré bonusy z těchto programů budou vypláceny až po ICO. Zaregistrujte se a zjistěte více informací ve <a href=\"/personal\">svém backoffice</a>.</p>', 7, 'cz'),
(9, 'How to buy FOX tokens?', '<p>It`s easy! Sign up here. In your back office create a wallet, where you will be able to hold FOX tokens. Then just send any amount to the address displayed in your back office.</p><p></p><p><strong>Attention! Never send money to a different address than the one you see in your back office!</strong></p>', 1, 'en'),
(10, 'Where can I find more detailed information on how UniFox exactly works?', '<p>All information that may interest those who want to invests in the FOX token can be found in the UniFox Whitepaper <a href=\"/documents\" >here</a>.</p><p>You can find technical information for individual parts on the <a href=\"/documents\" >DOCUMENTS</a> page</p>', 2, 'en'),
(11, 'What makes FOX tokens valuable?', '<p>FOX cryptocurrency is a share of the entire project. Therefore, its value can be considered as a digital share across of the entire project.</p><p>However, a fundamental rise in prices will occur as soon as FOX cryptocurrency starts to be used for paying blockchain fees, and all the services – the demand for the services of the company will trigger a demand for FOX cryptocurrency. This will directly cause the growth of the market price.</p>', 3, 'en'),
(12, 'What makes UniFox ICO unique?', '<p>\r\nThe FOX cryptocurrency, which is the subject of UniFox ICO, has a real utility for paying fees for all services that the company offers through its technologies. And that gives the cryptocurrency true meaning!</p><p>The combination of security features and utility token with own blockchain technology is a rarity among all ICOs.</p>', 4, 'en'),
(13, 'How are coins distributed?', '<p>The team of founders has decided to release 75% of all coins to partners.<br/><br/>\r\n						\r\n						<img style=\"width:600px;margin-bottom:20px;\" src=\"/images/ansver_graf_en.jpg\" />		\r\n						\r\n					</p>', 5, 'en'),
(14, 'How to make sure that the team and all and products are real?', '<p>It is possible to visit us and to take a look at our products personally, right in our office. If you are interested, contact our support team with this request in advance.</p>', 6, 'en'),
(15, 'Can I participate in ICO, even if I do not have any funds for buying FOX tokens?', '<p>\r\nYes, it is possible. ICO UniFox offers the opportunity to participate in the following programs:<br><br>\r\n<ul>\r\n<li>Bounty: opportunity to get FOX tokens by promoting the project publicly</li>\r\n<li>Airdrop: opportunity to get FOX tokens as a signup bonus</li>\r\n<li>Affiliate program (opportunity to get tokens for direct referrals)</li>\r\n</ul>\r\n<br><br>\r\nAll bonuses from these programs will be paid out as soon as the ICO is finished. Sign up and find out more information in your <a href=\"/personal\" >back office</a>.\r\n</p>', 7, 'en');

-- --------------------------------------------------------

--
-- Структура таблицы `gift_card`
--

CREATE TABLE `gift_card` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `wallet` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Структура таблицы `meetings`
--

CREATE TABLE `meetings` (
  `id` int(11) NOT NULL,
  `mdate` date NOT NULL,
  `name` text NOT NULL,
  `link` text NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `meetings`
--

INSERT INTO `meetings` (`id`, `mdate`, `name`, `link`, `image`) VALUES
(1, '2018-05-17', 'Blockchain & Bitcoin Conference', 'https://prague.bc.events/', '\\images\\meetings\\1.png'),
(2, '2018-06-11', 'MoneyConf', 'https://moneyconf.com/', '\\images\\meetings\\2.png'),
(3, '2018-07-06', 'Crypto Economy ICO 2018', 'https://www.thecryptoeconomy.com/calendar/', '\\images\\meetings\\3.png'),
(4, '2018-08-17', 'BlockVienna', 'https://www.blackarrowconferences.com/blockvienna.html', '\\images\\meetings\\4.png'),
(5, '2018-08-24', 'Summit blockchain', 'http://blc-summit.com/en', '\\images\\meetings\\5.png'),
(6, '2018-08-28', 'Blockchain summit', 'http://blockchainsummitsingapore.com', '\\images\\meetings\\6.png'),
(7, '2018-11-16', 'Crypto Economy ICO 2018', 'https://www.thecryptoeconomy.com/calendar/', '\\images\\meetings\\7.png'),
(8, '2018-10-18', 'Blockchain summit', 'http://www.blockchainsummitsanfrancisco.com', '\\images\\meetings\\8.png');

-- --------------------------------------------------------

--
-- Структура таблицы `meetings_lang`
--

CREATE TABLE `meetings_lang` (
  `id` int(11) NOT NULL,
  `meeting_id` int(11) NOT NULL,
  `city` varchar(255) NOT NULL,
  `lang` set('ru','en','cz') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `meetings_lang`
--

INSERT INTO `meetings_lang` (`id`, `meeting_id`, `city`, `lang`) VALUES
(2, 1, 'Прага', 'ru'),
(3, 1, 'Praha', 'cz'),
(4, 1, 'Prague', 'en'),
(5, 2, 'Дублин', 'ru'),
(6, 2, 'Dublin', 'en'),
(7, 2, 'Dublin', 'cz'),
(8, 3, 'Лондон', 'ru'),
(9, 3, 'London', 'en'),
(10, 3, 'Londýn', 'cz'),
(11, 4, 'Вена', 'ru'),
(12, 4, 'Vienna', 'en'),
(13, 4, 'Vídeň', 'cz'),
(14, 5, 'Шанхай', 'ru'),
(15, 5, 'Shanghai', 'en'),
(16, 5, 'Šanghaj', 'cz'),
(18, 6, 'Сингапур', 'ru'),
(20, 6, 'Singapore', 'en'),
(23, 7, 'Барселона', 'ru'),
(22, 6, 'Singapur', 'cz'),
(24, 7, 'Barcelona', 'en'),
(25, 7, 'Barcelona', 'cz'),
(26, 8, 'Сан-Франциско', 'ru'),
(27, 8, 'San Francisco', 'en'),
(28, 8, 'San Francisco', 'cz');

-- --------------------------------------------------------

--
-- Структура таблицы `recovery`
--

CREATE TABLE `recovery` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date` int(11) NOT NULL,
  `key_recovery` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `referal_point`
--

CREATE TABLE `referal_point` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `point` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




-- --------------------------------------------------------

--
-- Структура таблицы `referal_user`
--

CREATE TABLE `referal_user` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_referal_user` int(11) NOT NULL,
  `date_update` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `referal_user`
--


-- --------------------------------------------------------

--
-- Структура таблицы `roadmap`
--

CREATE TABLE `roadmap` (
  `id` int(11) NOT NULL,
  `lang` varchar(3) NOT NULL DEFAULT 'cz',
  `header` text NOT NULL,
  `text` text NOT NULL,
  `position` int(10) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `roadmap`
--

INSERT INTO `roadmap` (`id`, `lang`, `header`, `text`, `position`) VALUES
(2, 'cz', '10/2016 Zhotovení funkčního Kiosku', '<p>Fungující model automatizovaného platebního terminálu se schopností přijímat a vydávat hotovost. Základní softwarové vybavení a dlouhé testování.</p>', 1),
(3, 'cz', '4/2017 Vize decentralizovaného tržiště', '<p>Sepsání ekonomického konceptu tržiště s vlastnostmi blockchainu – 100% nezávislé, decentralizované bez možnosti zastavit transakce a zároveň 100% dostupnost.</p>', 2),
(4, 'cz', '11/2017 Zhotovení základní verze POS terminálů', '<p>Plně fungující verze POS terminálu.</p>', 3),
(5, 'cz', '12/2017 Dokončení Unicash.io a unifox blockchainu', '<p>Vytvoření stránek Unicash.io a veškerých prezentací + videí.<br>Vytvoření kryptoměny Unicash na Unifox blockchainu.</p>', 4),
(6, 'cz', '5/2018 Zveřejnění Whitepaperu', '<p>Umístění kompletní technické dokumentace na stránky.\r\n<br>\r\nPřidání Finančních vstupů a výstupů – to vše formou whitepaperu</p>', 5),
(7, 'cz', '25/5/2018 UniFox meeting pro investory a partnery', '<p></p>', 6),
(8, 'cz', '6/2018 Presale událost', '<p>UniFox projekt v měsíci červnu startuje výběr privátních investic do projektu do výše 2 milionů $. Souhrnná investovaná částka do projektu tak přesáhne 9 milionů dolarů.</p>', 7),
(9, 'cz', '7/2018 Propojení všech částí Unicash a start', '<p>Zapojení Unicash kryptoměny do všech složek projektu a oficiální spuštěni Unicash infrastruktury k Burze.</p>', 8),
(10, 'cz', 'Q4/2018 ICO UniFox', '<p>UniFox hlavní veřejný prodej mincí za účelem nabídnout uživatelům podíl v celé myšlence UniFox.\r\n<br><br>\r\nSoft Cap: 20 000 000 $<br>\r\nHard Cap: 100 000 000 $</p>', 9),
(11, 'cz', '12/2018 Zapojení UniFox peněženky', '<p>Veškeré tokeny jsou nahrány na UniFox blockchain peněženku a mohou tedy být i volně obchodovány.</p>', 10),
(12, 'cz', '3/2019 Dokončení testovací verze burzy', '<p>Spuštění prvního provozu systému za účelem odladit veškeré chyby a garantovat uživatelsky příjemné prostředí k dokonalosti.</p>', 11),
(13, 'cz', '6/2019 UniFoxBurza Plné spuštění', '<p>Otevření plné verze UniFox blockchainu zahrnující otevření největší burzy pro digitální měny na světě. Otevření burzy zahrnuje napojení celé infrastruktury Bankomaty, Směnárny, POS terminály, Unicash.</p>', 12),
(14, 'cz', '9/2019 Dokončení hardware burzy', '<p>Dokončení hardware části, která zajistí možnost uživatelům skladovat jakoukoliv kryptoměnu v jedné hardware peněžence a možnost tyto měny obchodovat (skrze aplikaci).</p>', 13),
(15, 'cz', '2020 Získání postu vedoucí světové burzy', '<p>Dosažení světové majority v rámci směn virtuálních měn na základě dokonalého zpracování\r\n<br><br>\r\nPodíl 10% na světovém trhu virtuálních měn.</p>', 14),
(18, 'en', '10/2016 Fabrication of a functional kiosk', '<p>A working model of an automated payment terminal with the ability to receive and issue cash. Basic software equipment and long testing.</p>', 1),
(19, 'en', '4/2017 The vision of the decentralized marketplace', '<p>A written form of the economic concept of a marketplace with blockchain features – 100% independent, decentralized, without the ability to stop transactions and 100% accessible.</p>', 2),
(20, 'en', '11/2017 Basic version of POS terminals', '<p>A fully functional version of the POS terminal</p>', 3),
(21, 'en', '12/2017 Unicash.io and Unifox blockchain completion', '<p>Unicash.io web page and all presentations + videos.\r\n<br><br>\r\nUnicash cryptocurrency and Unifox blockchain basic version.</p>', 4),
(22, 'en', '5/2018 Whitepaper release', '<p>Uploading of complete technical documentation to the website. Adding financial inputs and outputs – all within the whitepaper.</p>', 5),
(23, 'en', '25/5/2018 UniFox meeting for all investors and partners', '<p></p>', 6),
(24, 'en', '6/2018 Start of the presale event', '<p>Unifox presale is going to raise 2 million dollars from private investors. The total invested value in the project will be over 9 million dollars.</p>', 7),
(25, 'en', '7/2018 Interconnection of all Unicash parts and launch', '<p>Implementation of a Unicash cryptocurrency into all parts of the project and the official launch of Unicash infrastructure to the market.</p>', 8),
(26, 'en', 'Q4/2018 ICO UniFox', '<p>UniFox main public sale with the intent to offer users a share in the entire idea of UniFox.\r\n<br>Soft Cap: 20 000 000 $\r\n<br>Hard Cap: 100 000 000 $</p>', 9),
(27, 'en', '12/2018 UniFox Wallet Connection', '<p>All tokens are uploaded to the UniFox blockchain wallet. Therefore, they can be freely traded.</p>', 10),
(28, 'en', '3/2019 Completion of the test version of the Exchange', '<p>The launch of the first system in order to debug and improve the user-friendly environment to perfection.</p>', 11),
(29, 'en', '6/2019 UniFox Exchange Full Launch', '<p>The opening of a full version of the UniFox blockchain, including the opening of the biggest exchange for the digital currency. The opening of the Exchange includes the connection of the whole infrastructure – ATMs, Exchange Offices, POS terminals.</p>', 12),
(30, 'en', '9/2019 Completion of  Exchange hardware', '<p>Completion of a hardware part to ensure that users can store any cryptocurrency in one wallet + the ability to trade these currencies (using the app).</p>', 13),
(31, 'en', '2020 Gaining a lead position as the Worldwide Exchange', '<p>\r\nAchieving a worldwide majority in virtual exchange currencies thanks to perfect development.\r\n<br><br>\r\nThe 10% share of the global virtual currency market.\r\n</p>', 14);

-- --------------------------------------------------------

--
-- Структура таблицы `send_transaction`
--

CREATE TABLE `send_transaction` (
  `id_t` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `error` varchar(255) NOT NULL DEFAULT '',
  `txhash` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



-- --------------------------------------------------------

--
-- Структура таблицы `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `department` set('1','2','3','4','5','0') NOT NULL DEFAULT '0',
  `photo` text NOT NULL,
  `linkedin` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `team`
--

INSERT INTO `team` (`id`, `name`, `department`, `photo`, `linkedin`) VALUES
(1, 'Dalibor Malek', '1', '\\images\\team_people\\key-figures\\DSC_4582.jpg', 'https://www.linkedin.com/in/dalibor-malek-31788625/?lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_people%3B6XFF7fEjQCanODeykIQAIw%3D%3D&licu=urn%3Ali%3Acontrol%3Ad_flagship3_search_srp_people-search_srp_result&lici=gXzTbRPIS3ybEDN6X5EFmQ%3D%3D'),
(2, 'Vadim Bilousov', '1', '\\images\\team_people\\key-figures\\DSC_4381.jpg', 'https://www.linkedin.com/in/vadim-belousov/?lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_people%3B6XFF7fEjQCanODeykIQAIw%3D%3D&licu=urn%3Ali%3Acontrol%3Ad_flagship3_search_srp_people-search_srp_result&lici=PemMDHd3QcKL3pYQ8afTSg%3D%3D'),
(3, 'Jan Turek', '1', '\\images\\team_people\\key-figures\\DSC_4392.jpg', 'https://www.linkedin.com/in/jan-turek-88a6a4ab/?lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_people%3B6XFF7fEjQCanODeykIQAIw%3D%3D&licu=urn%3Ali%3Acontrol%3Ad_flagship3_search_srp_people-search_srp_result&lici=NHH0pzVcSWapmhiNzFmW3w%3D%3D'),
(15, 'Martin Král', '2', '/images/team_people/no-photo.png', '#'),
(4, 'Robert Breadon', '2', '/images/team_people/no-photo.png', '#'),
(5, 'Wes Carlson', '2', '\\images\\team_people\\Advisory\\wes.jpg', 'https://www.linkedin.com/in/wesleycarlson/'),
(6, 'Ruslan Sokolovsky', '2', '\\images\\team_people\\Advisory\\ruslansokolovsky.jpg', 'https://vk.com/sokolovsky'),
(7, 'Ugur Ozer', '2', '\\images\\team_people\\Advisory\\Ugur_Ozer.jpg', 'https://www.linkedin.com/in/uguro/'),
(8, 'Kenneth Otalor', '2', '\\images\\team_people\\Advisory\\20180305_092841.jpg', 'https://www.linkedin.com/in/kenneth-otalor-12ba08145/'),
(9, 'Michal Vlasak', '3', '\\images\\team_people\\managing\\DSC_4389.jpg', 'https://www.linkedin.com/in/michal-vlasak-4b407b157/'),
(10, 'Lukas Holas', '3', '\\images\\team_people\\managing\\DSC_4399.jpg', 'https://www.linkedin.com/in/lukas-holas-401261163/?lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_people%3B6XFF7fEjQCanODeykIQAIw%3D%3D&licu=urn%3Ali%3Acontrol%3Ad_flagship3_search_srp_people-search_srp_result&lici=OsicUsQiQimPzAAFNl6CIw%3D%3D'),
(11, 'Dominik Kadaně', '3', '\\images\\team_people\\managing\\DSC_4394.jpg', 'https://www.linkedin.com/in/dominik-kadan%C4%9B-559089149/'),
(12, 'Petra Kmetova', '3', '\\images\\team_people\\managing\\DSC_4590.jpg', 'https://www.linkedin.com/in/petra-kmetova-b6115384/'),
(13, 'Karol Suchánek', '3', '/images/team_people/no-photo.png', '#'),
(14, 'Vladislav Progrebnyak', '3', '\\images\\team_people\\managing\\Vladislav_Pogrebnyak.jpg', 'https://www.linkedin.com/in/vpogrebnyak/?lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_people%3B6XFF7fEjQCanODeykIQAIw%3D%3D&licu=urn%3Ali%3Acontrol%3Ad_flagship3_search_srp_people-search_srp_result&lici=vlbII%2FTbSiWzWGK8V4dJEw%3D%3D');

-- --------------------------------------------------------

--
-- Структура таблицы `team_lang`
--

CREATE TABLE `team_lang` (
  `id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `job` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `lang` set('ru','cz','en','') NOT NULL DEFAULT 'en'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `team_lang`
--

INSERT INTO `team_lang` (`id`, `team_id`, `job`, `comment`, `lang`) VALUES
(1, 1, 'CEO & Co-Founder', 'A very innovative and visionary strategic manager with rich experience in the printing and postal industry. Co-founder of one of the world\'s largest network of parcel lockers EASYPACK and a unique world\'s first transpromo hybrid post project CROMWELL.', 'en'),
(2, 1, 'CEO & spoluzakladatel', 'Inovativní a vizionářský strategický manažer s bohatými zkušenostmi v oblasti tiskových služeb a počtovního průmyslu. Spoluzakaldatel jedné z největších celosvětových sítí balíkomatů EASYPACK a unikátního projektu transpromo hybridní pošty CROMWELL.', 'cz'),
(3, 1, 'CEO & соучредитель', 'Новатор, визионер и стратегический менеджер с богатым опытом в области печатной и почтовой индустрии. Соучредитель одной из крупнейших в мире сетей почтоматов EASYPACK и уникального гибридного почтового проекта CROMWELL.', 'ru'),
(4, 2, 'CTO & Co-Founder', 'Developer of the Automatic bank terminals and Retail Payments Solutions with e-Kassir and Qiwi systems. Founder of the Software Company Arbitas.', 'en'),
(5, 2, 'CTO & spoluzakladatel', 'Vývojař automatových bankovních terminálů a finančních softwarových řešení spolu s e-Kassir a Qiwi systems. Zakladatel softwarové společnosti Arbitas.', 'cz'),
(6, 2, 'CTO & соучредитель', 'Разработчик автоматических банковских терминалов и програмных решений вместе с e-Kassir и Qiwi sytems. Основатель компании программного обеспечения Arbitas.', 'ru'),
(7, 3, 'Co-Founder', 'Author of the concept of Unicash.io and FOX token. Co-founder of partner online crypto/FIAT exchanger network', 'en'),
(8, 3, 'Spoluzakladatel', 'Autor konceptu Unicash.io a FOX kryptoměny. Spoluzakladatel partnerské sítě směnáren na kryptoměny.', 'cz'),
(9, 3, 'Cоучредитель', 'Автор концепции Unicash.io и токена FOX. Соучредитель партнёрской сети обменников для криптовалют.', 'ru'),
(10, 4, 'NLP Coaching, speaker', 'Successful entrepreneur, dollar millionaire, investor, speaker and coach in NLP.', 'en'),
(11, 4, 'NLP Coaching, speaker', 'Úspěšný podnikatel, dolarový milionář, investor a osobní coach NLP.', 'cz'),
(12, 4, 'НЛП коучинг, cпикер', 'Успешный предприниматель, долларовый миллионер, инвестор, спикер и НЛП тренер.', 'ru'),
(13, 5, 'Blockchain Advisor', 'Experienced Director with a demonstrated history of working in the real estate, Bitcoin & cryptocurrency industry. Founder of MyBitcoin Mauritius.', 'en'),
(14, 5, 'Blockchain Advisor', 'Zakladatel MyBitcoin Mauritius. Úspěšný podnikatel s bohatými zkušenostmi v oblasti nemovitostí, Bitcoinu a kryptoměn.', 'cz'),
(15, 5, 'Блокчейн эдвайзер', 'Опытный директор, с богатым опытом в сфере недвижимости, Bitcoin и крипто индустрии. Основатель MyBitcoin Mauritus.', 'ru'),
(16, 6, 'Media Advisor', 'Founder of crypto website Prometheus.ru and famous Russian video blogger.', 'en'),
(17, 6, 'Media Advisor', 'Zakladatel krypto stránky Prometheus.ru a slavný ruský video bloger.', 'cz'),
(18, 6, 'Эдвайзер по СМИ', 'Основатель криптовалютного СМИ Prometheus и известный российский видео-блогер.', 'ru'),
(19, 7, 'Strategic Advisor', 'As an Industrial Engineer and an Master of Business Administrator in Banking and Finance, Ugur is a seasoned Financial Services and Management Consulting professional.', 'en'),
(20, 7, 'Strategic Advisor', 'Jako průmyslový inženýr a MBA v oblasti bankovnictví a financí je Ugur zkušený odborník na finanční služby a manažerské poradenství.', 'cz'),
(21, 7, 'Стратегический эдвайзер', 'Будучи промышленным инженером и MBA в сфере банковских услуг и финансов, Угур – опытный специалист по финансовым услугам и управленческому консалтингу.', 'ru'),
(22, 8, 'Marketing Advisor', 'As a founder of Calero.io, Ken has become a well respected blockhain advocate, ICO advisor and investor. He possess over 15 years experience in social impact and global macro investments.', 'en'),
(23, 8, 'Marketing Advisor', 'Ken je zakladatel projektu Calero.io a uznávaným ICO advisorem, investorem a průkopníkem v oblasti blockchainu. Disponuje 15 letými zkušenosti v oblasti globálních makro investic a sociálních věd.', 'cz'),
(24, 8, 'Эдвайзер по маркетингу', 'Будучи основателем Calero.io, Кен стал уважаемым ICO эдвайзером, советником, инвестором и новатором в сфере блокчейн. Он обладает более чем 15-летним опытом в области общественных наук и глобальных макро-инвестиций.', 'ru'),
(25, 9, 'CMO', 'Talented marketing officer with main focus on online marketing. Has helped world blockchain companies to master their token sales. Co-founder of Cryptosvet.cz', 'en'),
(26, 9, 'CMO', 'Talentovaný marketingový stratég se zaměřením na online marketing. Pomohl světovým blockchainovým projektům k úspěšným ICOs. Spoluzakladatel krypto stránky Cryptosvět.cz.', 'cz'),
(27, 9, 'CMO', 'Талантливый специалист по маркетингу, со специализацией на онлайн-маркетинг. Помог всемирно изветным блокчейн проектам. Соучредитель Cryptosvet.cz', 'ru'),
(28, 10, 'Business Developer', 'He has experience with leading and creating offline teams. Team leader charged with the task of managing a group of Sales Managers to be as effective as possible.', 'en'),
(29, 10, 'Business Developer', 'Má dlouholeté zkušenosti s vedením a vytvářením obchodních týmů. Vedoucí týmu pověřený úkolem řídit skupinu prodejních manažerů, aby byla co nejúčinnější.', 'cz'),
(30, 10, 'Менеджер развития бизнеса', 'Опыт работы с создаванием и лидированем групы дюдей. В качестве руководителя управлял группой менеджеров максимально эффективным способом.', 'ru'),
(31, 11, 'Business Developer', 'He has wide experience with managing managerial staff and system implementation in new branches.', 'en'),
(32, 11, 'Business Developer', 'Má rozsáhlé zkušenosti s vedením mamažerských týmů a  implementováním systémových prvků do nově vzniklých poboček.', 'cz'),
(33, 11, 'Менеджер развития бизнеса', 'Имеет большой опыт управления управленческим персоналом и внедрением системы в новые филиалы.', 'ru'),
(34, 12, 'Head Project Manager', 'Experienced Project manager with focus on optimizing processes and innovative approaches. Most projects have concerned international business development and launching new markets.', 'en'),
(35, 12, 'Head Project Manager', 'Zkušená projektová manažerka se zaměřením na optimalizaci procesů a inovativní přístupy. Většina projektů se týkala mezinárodního rozvoje obchodu a otevírání nových trhů.', 'cz'),
(36, 12, 'Главный руководитель проекта', 'Опытный руководитель проекта с уклоном на оптимизацию процессов и инновационных подходов. Большинство проектов касалось развития международного бизнеса и входа на новые рынки.', 'ru'),
(37, 13, 'IT security', '', 'en'),
(38, 13, 'IT bezpečnost', '', 'cz'),
(39, 13, 'ИТ безопасность', '', 'ru'),
(40, 14, 'Project Manager', 'Experienced Project Manager with responsibility for delivering the individual development and marketing projects on time and within the budget while remaining aligned with strategy and goals of the organization.', 'en'),
(41, 14, 'Project Manager', 'Zkušený projektový manažer se zodpovědností za včasné dodání jednotlivých vývojových a marketingových projektů v souladu s rozpočtem, strategií a cíli organizace.', 'cz'),
(42, 14, 'Руководитель проекта', 'Опытный руководитель проекта отвечающий за предоставление отдельных проектов разработки и маркетинга в соответствии с бюджетом, стратегией и целями организации.', 'ru'),
(43, 15, 'Advisor', '', 'en'),
(44, 15, 'Advisor', '', 'cz'),
(45, 15, 'Advisor', '', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `activate` tinyint(1) NOT NULL DEFAULT '0',
  `img` varchar(255) NOT NULL,
  `fb_id` varchar(255) NOT NULL,
  `password_api` varchar(255) NOT NULL,
  `is_verification` tinyint(2) NOT NULL DEFAULT '0',
  `is_admin` tinyint(3) NOT NULL DEFAULT '0',
  `is_airdrop` tinyint(3) NOT NULL DEFAULT '0',
  `activate_code` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users_visitation`
--

CREATE TABLE `users_visitation` (
  `id_user` bigint(20) NOT NULL,
  `date` int(11) NOT NULL,
  `day` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users_visitation`
--



-- --------------------------------------------------------

--
-- Структура таблицы `verification`
--

CREATE TABLE `verification` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `birthdate` varchar(10) NOT NULL,
  `citizenship` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `issue_date` varchar(10) NOT NULL,
  `expaer_date` varchar(10) NOT NULL,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `adres` varchar(255) NOT NULL,
  `documents_1` varchar(255) NOT NULL,
  `documents_2` varchar(255) NOT NULL,
  `proff_adres` varchar(255) NOT NULL,
  `selfi` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1-новый запрос;2-отправлен на изменение; 3-одобрен',
  `date_update` int(11) NOT NULL,
  `wallets` varchar(255) NOT NULL,
  `checking_user` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




--
-- Структура таблицы `verification_error`
--

CREATE TABLE `verification_error` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `name` tinyint(1) NOT NULL DEFAULT '0',
  `last_name` tinyint(1) NOT NULL DEFAULT '0',
  `middle_name` tinyint(1) NOT NULL DEFAULT '0',
  `birthdate` tinyint(1) NOT NULL DEFAULT '0',
  `citizenship` tinyint(1) NOT NULL DEFAULT '0',
  `number` tinyint(1) NOT NULL DEFAULT '0',
  `issue_date` tinyint(1) NOT NULL DEFAULT '0',
  `expaer_date` tinyint(1) NOT NULL DEFAULT '0',
  `country` tinyint(1) NOT NULL DEFAULT '0',
  `city` tinyint(1) NOT NULL DEFAULT '0',
  `adres` tinyint(1) NOT NULL DEFAULT '0',
  `documents_1` tinyint(1) NOT NULL DEFAULT '0',
  `documents_2` tinyint(1) NOT NULL DEFAULT '0',
  `proff_adres` tinyint(1) NOT NULL DEFAULT '0',
  `selfi` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



--
-- Структура таблицы `verification_new`
--

CREATE TABLE `verification_new` (
  `id` bigint(20) NOT NULL,
  `id_user` int(11) NOT NULL,
  `is_email` tinyint(3) NOT NULL DEFAULT '0',
  `is_document` tinyint(3) NOT NULL DEFAULT '0',
  `is_adres` tinyint(3) NOT NULL DEFAULT '0',
  `is_personal_info` tinyint(3) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `airpod`
--
ALTER TABLE `airpod`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Индексы таблицы `airpod_error`
--
ALTER TABLE `airpod_error`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Индексы таблицы `bounty_list`
--
ALTER TABLE `bounty_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`);

--
-- Индексы таблицы `bounty_program`
--
ALTER TABLE `bounty_program`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `row_key` (`row_key`),
  ADD KEY `type` (`type`);

--
-- Индексы таблицы `email_verification_code`
--
ALTER TABLE `email_verification_code`
  ADD KEY `id_user` (`id_user`),
  ADD KEY `code` (`code`),
  ADD KEY `email` (`email`);

--
-- Индексы таблицы `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lang` (`lang`);

--
-- Индексы таблицы `gift_card`
--
ALTER TABLE `gift_card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `code` (`code`);

--
-- Индексы таблицы `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `meetings_lang`
--
ALTER TABLE `meetings_lang`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `recovery`
--
ALTER TABLE `recovery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `key_recovery` (`key_recovery`);

--
-- Индексы таблицы `referal_point`
--
ALTER TABLE `referal_point`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Индексы таблицы `referal_user`
--
ALTER TABLE `referal_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_referal` (`id_referal_user`);

--
-- Индексы таблицы `roadmap`
--
ALTER TABLE `roadmap`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lang` (`lang`);

--
-- Индексы таблицы `send_transaction`
--
ALTER TABLE `send_transaction`
  ADD PRIMARY KEY (`id_t`);

--
-- Индексы таблицы `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `team_lang`
--
ALTER TABLE `team_lang`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`);

--
-- Индексы таблицы `users_visitation`
--
ALTER TABLE `users_visitation`
  ADD KEY `id_user` (`id_user`),
  ADD KEY `day` (`day`);

--
-- Индексы таблицы `verification`
--
ALTER TABLE `verification`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `verification_error`
--
ALTER TABLE `verification_error`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `verification_new`
--
ALTER TABLE `verification_new`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `airpod`
--
ALTER TABLE `airpod`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `airpod_error`
--
ALTER TABLE `airpod_error`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `bounty_list`
--
ALTER TABLE `bounty_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT для таблицы `bounty_program`
--
ALTER TABLE `bounty_program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `gift_card`
--
ALTER TABLE `gift_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `meetings`
--
ALTER TABLE `meetings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `meetings_lang`
--
ALTER TABLE `meetings_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `recovery`
--
ALTER TABLE `recovery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `referal_point`
--
ALTER TABLE `referal_point`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `referal_user`
--
ALTER TABLE `referal_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `roadmap`
--
ALTER TABLE `roadmap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT для таблицы `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `team_lang`
--
ALTER TABLE `team_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `verification`
--
ALTER TABLE `verification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `verification_error`
--
ALTER TABLE `verification_error`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `verification_new`
--
ALTER TABLE `verification_new`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;
