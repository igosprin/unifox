-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 08 2018 г., 13:48
-- Версия сервера: 5.7.20-log
-- Версия PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `unifox`
--

-- --------------------------------------------------------

--
-- Структура таблицы `team_lang`
--

CREATE TABLE `team_lang` (
  `id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `job` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `lang` set('ru','cz','en','') NOT NULL DEFAULT 'en'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `team_lang`
--

INSERT INTO `team_lang` (`id`, `team_id`, `job`, `comment`, `lang`) VALUES
(1, 1, 'CEO & Co-Founder', 'A very innovative and visionary strategic manager with rich experience in the printing and postal industry. Co-founder of one of the world\'s largest network of parcel lockers EASYPACK and a unique world\'s first transpromo hybrid post project CROMWELL.', 'en'),
(2, 1, 'CEO & spoluzakladatel', 'Inovativní a vizionářský strategický manažer s bohatými zkušenostmi v oblasti tiskových služeb a počtovního průmyslu. Spoluzakaldatel jedné z největších celosvětových sítí balíkomatů EASYPACK a unikátního projektu transpromo hybridní pošty CROMWELL.', 'cz'),
(3, 1, 'CEO & соучредитель', 'Новатор, визионер и стратегический менеджер с богатым опытом в области печатной и почтовой индустрии. Соучредитель одной из крупнейших в мире сетей почтоматов EASYPACK и уникального гибридного почтового проекта CROMWELL.', 'ru'),
(4, 2, 'CTO & Co-Founder', 'Developer of the Automatic bank terminals and Retail Payments Solutions with e-Kassir and Qiwi systems. Founder of the Software Company Arbitas.', 'en'),
(5, 2, 'CTO & spoluzakladatel', 'Vývojař automatových bankovních terminálů a finančních softwarových řešení spolu s e-Kassir a Qiwi systems. Zakladatel softwarové společnosti Arbitas.', 'cz'),
(6, 2, 'CTO & соучредитель', 'Разработчик автоматических банковских терминалов и програмных решений вместе с e-Kassir и Qiwi sytems. Основатель компании программного обеспечения Arbitas.', 'ru'),
(7, 3, 'Co-Founder', 'Author of the concept of Unicash.io and FOX token. Co-founder of partner online crypto/FIAT exchanger network', 'en'),
(8, 3, 'Spoluzakladatel', 'Autor konceptu Unicash.io a FOX kryptoměny. Spoluzakladatel partnerské sítě směnáren na kryptoměny.', 'cz'),
(9, 3, 'Cоучредитель', 'Автор концепции Unicash.io и токена FOX. Соучредитель партнёрской сети обменников для криптовалют.', 'ru'),
(10, 4, 'NLP Coaching, speaker', 'Successful entrepreneur, dollar millionaire, investor, speaker and coach in NLP.', 'en'),
(11, 4, 'NLP Coaching, speaker', 'Úspěšný podnikatel, dolarový milionář, investor a osobní coach NLP.', 'cz'),
(12, 4, 'НЛП коучинг, cпикер', 'Успешный предприниматель, долларовый миллионер, инвестор, спикер и НЛП тренер.', 'ru'),
(13, 5, 'Blockchain Advisor', 'Experienced Director with a demonstrated history of working in the real estate, Bitcoin & cryptocurrency industry. Founder of MyBitcoin Mauritius.', 'en'),
(14, 5, 'Blockchain Advisor', 'Zakladatel MyBitcoin Mauritius. Úspěšný podnikatel s bohatými zkušenostmi v oblasti nemovitostí, Bitcoinu a kryptoměn.', 'cz'),
(15, 5, 'Блокчейн эдвайзер', 'Опытный директор, с богатым опытом в сфере недвижимости, Bitcoin и крипто индустрии. Основатель MyBitcoin Mauritus.', 'ru'),
(16, 6, 'Media Advisor', 'Founder of crypto website Prometheus.ru and famous Russian video blogger.', 'en'),
(17, 6, 'Media Advisor', 'Zakladatel krypto stránky Prometheus.ru a slavný ruský video bloger.', 'cz'),
(18, 6, 'Эдвайзер по СМИ', 'Основатель криптовалютного СМИ Prometheus и известный российский видео-блогер.', 'ru'),
(19, 7, 'Strategic Advisor', 'As an Industrial Engineer and an Master of Business Administrator in Banking and Finance, Ugur is a seasoned Financial Services and Management Consulting professional.', 'en'),
(20, 7, 'Strategic Advisor', 'Jako průmyslový inženýr a MBA v oblasti bankovnictví a financí je Ugur zkušený odborník na finanční služby a manažerské poradenství.', 'cz'),
(21, 7, 'Стратегический эдвайзер', 'Будучи промышленным инженером и MBA в сфере банковских услуг и финансов, Угур – опытный специалист по финансовым услугам и управленческому консалтингу.', 'ru'),
(22, 8, 'Marketing Advisor', 'As a founder of Calero.io, Ken has become a well respected blockhain advocate, ICO advisor and investor. He possess over 15 years experience in social impact and global macro investments.', 'en'),
(23, 8, 'Marketing Advisor', 'Ken je zakladatel projektu Calero.io a uznávaným ICO advisorem, investorem a průkopníkem v oblasti blockchainu. Disponuje 15 letými zkušenosti v oblasti globálních makro investic a sociálních věd.', 'cz'),
(24, 8, 'Эдвайзер по маркетингу', 'Будучи основателем Calero.io, Кен стал уважаемым ICO эдвайзером, советником, инвестором и новатором в сфере блокчейн. Он обладает более чем 15-летним опытом в области общественных наук и глобальных макро-инвестиций.', 'ru'),
(25, 9, 'CMO', 'Talented marketing officer with main focus on online marketing. Has helped world blockchain companies to master their token sales. Co-founder of Cryptosvet.cz', 'en'),
(26, 9, 'CMO', 'Talentovaný marketingový stratég se zaměřením na online marketing. Pomohl světovým blockchainovým projektům k úspěšným ICOs. Spoluzakladatel krypto stránky Cryptosvět.cz.', 'cz'),
(27, 9, 'CMO', 'Талантливый специалист по маркетингу, со специализацией на онлайн-маркетинг. Помог всемирно изветным блокчейн проектам. Соучредитель Cryptosvet.cz', 'ru'),
(28, 10, 'Business Developer', 'He has experience with leading and creating offline teams. Team leader charged with the task of managing a group of Sales Managers to be as effective as possible.', 'en'),
(29, 10, 'Business Developer', 'Má dlouholeté zkušenosti s vedením a vytvářením obchodních týmů. Vedoucí týmu pověřený úkolem řídit skupinu prodejních manažerů, aby byla co nejúčinnější.', 'cz'),
(30, 10, 'Менеджер развития бизнеса', 'Опыт работы с создаванием и лидированем групы дюдей. В качестве руководителя управлял группой менеджеров максимально эффективным способом.', 'ru'),
(31, 11, 'Business Developer', 'He has wide experience with managing managerial staff and system implementation in new branches.', 'en'),
(32, 11, 'Business Developer', 'Má rozsáhlé zkušenosti s vedením mamažerských týmů a  implementováním systémových prvků do nově vzniklých poboček.', 'cz'),
(33, 11, 'Менеджер развития бизнеса', 'Имеет большой опыт управления управленческим персоналом и внедрением системы в новые филиалы.', 'ru'),
(34, 12, 'Head Project Manager', 'Experienced Project manager with focus on optimizing processes and innovative approaches. Most projects have concerned international business development and launching new markets.', 'en'),
(35, 12, 'Head Project Manager', 'Zkušená projektová manažerka se zaměřením na optimalizaci procesů a inovativní přístupy. Většina projektů se týkala mezinárodního rozvoje obchodu a otevírání nových trhů.', 'cz'),
(36, 12, 'Главный руководитель проекта', 'Опытный руководитель проекта с уклоном на оптимизацию процессов и инновационных подходов. Большинство проектов касалось развития международного бизнеса и входа на новые рынки.', 'ru'),
(37, 13, 'IT security', '', 'en'),
(38, 13, 'IT bezpečnost', '', 'cz'),
(39, 13, 'ИТ безопасность', '', 'ru'),
(40, 14, 'Project Manager', 'Experienced Project Manager with responsibility for delivering the individual development and marketing projects on time and within the budget while remaining aligned with strategy and goals of the organization.', 'en'),
(41, 14, 'Project Manager', 'Zkušený projektový manažer se zodpovědností za včasné dodání jednotlivých vývojových a marketingových projektů v souladu s rozpočtem, strategií a cíli organizace.', 'cz'),
(42, 14, 'Руководитель проекта', 'Опытный руководитель проекта отвечающий за предоставление отдельных проектов разработки и маркетинга в соответствии с бюджетом, стратегией и целями организации.', 'ru'),
(43, 15, 'Advisor', '', 'en'),
(44, 15, 'Advisor', '', 'cz'),
(45, 15, 'Advisor', '', 'ru');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `team_lang`
--
ALTER TABLE `team_lang`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `team_lang`
--
ALTER TABLE `team_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 08 2018 г., 13:48
-- Версия сервера: 5.7.20-log
-- Версия PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `unifox`
--

-- --------------------------------------------------------

--
-- Структура таблицы `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `department` set('1','2','3','4','5','0') NOT NULL DEFAULT '0',
  `photo` text NOT NULL,
  `linkedin` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `team`
--

INSERT INTO `team` (`id`, `name`, `department`, `photo`, `linkedin`) VALUES
(1, 'Dalibor Malek', '1', '\\images\\team_people\\key-figures\\DSC_4582.jpg', 'https://www.linkedin.com/in/dalibor-malek-31788625/?lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_people%3B6XFF7fEjQCanODeykIQAIw%3D%3D&licu=urn%3Ali%3Acontrol%3Ad_flagship3_search_srp_people-search_srp_result&lici=gXzTbRPIS3ybEDN6X5EFmQ%3D%3D'),
(2, 'Vadim Bilousov', '1', '\\images\\team_people\\key-figures\\DSC_4381.jpg', 'https://www.linkedin.com/in/vadim-belousov/?lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_people%3B6XFF7fEjQCanODeykIQAIw%3D%3D&licu=urn%3Ali%3Acontrol%3Ad_flagship3_search_srp_people-search_srp_result&lici=PemMDHd3QcKL3pYQ8afTSg%3D%3D'),
(3, 'Jan Turek', '1', '\\images\\team_people\\key-figures\\DSC_4392.jpg', 'https://www.linkedin.com/in/jan-turek-88a6a4ab/?lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_people%3B6XFF7fEjQCanODeykIQAIw%3D%3D&licu=urn%3Ali%3Acontrol%3Ad_flagship3_search_srp_people-search_srp_result&lici=NHH0pzVcSWapmhiNzFmW3w%3D%3D'),
(15, 'Martin Král', '2', '/images/team_people/no-photo.png', '#'),
(4, 'Robert Breadon', '2', '/images/team_people/no-photo.png', '#'),
(5, 'Wes Carlson', '2', '\\images\\team_people\\Advisory\\wes.jpg', 'https://www.linkedin.com/in/wesleycarlson/'),
(6, 'Ruslan Sokolovsky', '2', '\\images\\team_people\\Advisory\\ruslansokolovsky.jpg', 'https://vk.com/sokolovsky'),
(7, 'Ugur Ozer', '2', '\\images\\team_people\\Advisory\\Ugur_Ozer.jpg', 'https://www.linkedin.com/in/uguro/'),
(8, 'Kenneth Otalor', '2', '\\images\\team_people\\Advisory\\20180305_092841.jpg', 'https://www.linkedin.com/in/kenneth-otalor-12ba08145/'),
(9, 'Michal Vlasak', '3', '\\images\\team_people\\managing\\DSC_4389.jpg', 'https://www.linkedin.com/in/michal-vlasak-4b407b157/'),
(10, 'Lukas Holas', '3', '\\images\\team_people\\managing\\DSC_4399.jpg', 'https://www.linkedin.com/in/lukas-holas-401261163/?lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_people%3B6XFF7fEjQCanODeykIQAIw%3D%3D&licu=urn%3Ali%3Acontrol%3Ad_flagship3_search_srp_people-search_srp_result&lici=OsicUsQiQimPzAAFNl6CIw%3D%3D'),
(11, 'Dominik Kadaně', '3', '\\images\\team_people\\managing\\DSC_4394.jpg', 'https://www.linkedin.com/in/dominik-kadan%C4%9B-559089149/'),
(12, 'Petra Kmetova', '3', '\\images\\team_people\\managing\\DSC_4590.jpg', 'https://www.linkedin.com/in/petra-kmetova-b6115384/'),
(13, 'Karol Suchánek', '3', '/images/team_people/no-photo.png', '#'),
(14, 'Vladislav Progrebnyak', '3', '\\images\\team_people\\managing\\Vladislav_Pogrebnyak.jpg', 'https://www.linkedin.com/in/vpogrebnyak/?lipi=urn%3Ali%3Apage%3Ad_flagship3_search_srp_people%3B6XFF7fEjQCanODeykIQAIw%3D%3D&licu=urn%3Ali%3Acontrol%3Ad_flagship3_search_srp_people-search_srp_result&lici=vlbII%2FTbSiWzWGK8V4dJEw%3D%3D');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
