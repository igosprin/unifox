<?php
session_start();
header("Content-Type: text/html;charset=utf-8");
header("Expires: " . gmdate("D, d M Y H:i:s", time() + 60*60) . " GMT");


ini_set('display_errors','on');
error_reporting(E_ALL & ~ E_NOTICE);
//error_reporting(1);



$temp_uri=explode('.',$_SERVER['HTTP_HOST']);


//include 'library/redirect.php';
include 'config.php';
include 'library/db_clean.php';
include 'library/userinfo_detect.php';
include 'library/library.php';

$db_connectors = db_provider();

// определение языка по IP
//ipDetector();


include 'library/uri_parse.php';
include 'library/referral.php';

$params = getParams();

define('MODEL',$params['model']);
define('IS_ADMIN',$params['is_admin']);

if($params['language']!='')
	$its_lang=$params['language'];
else $its_lang=DEFAULT_LANG;

define('LANG',$its_lang);

if(LANG==DEFAULT_LANG)
	define('LINK_LANG','');
else
	define('LINK_LANG','/'.LANG);	
include 'library/queryString.php';
include PATH_SCRIPT.'/'.MODEL.'/main.php';
include 'library/date.php';
include 'lang/'.LANG.'.php';

include 'library/language_creater.php';
include 'library/app_cookie.php';

include 'library/mobile-detect.php';
$detect = new Mobile_Detect;

$AUTH_APP=new App_Auth;
$AUTH_APP->hasIdentity();

$body_def=array(
	1=>array('class'=>'mainer','slider'=>1),
	2=>array('class'=>'document','slider'=>2)
);

localGet();
$content=array();
$content['title']='';
$content['DOMAIN']=DOMEN_NAME;
$content['new_layout']='';
$content['LANG_DEFINE']=$_constant_languages;
$content['url_active']='/';
$content['url_active_sub']='';
$content['page_body']=$body_def;
$content['page_body_id']=1;
$content['desc']='';
$content['key']='';
$content['h1']='';
$content['namesaite']=NAME_SAITE;
$content['meta:face:img']='';
$content['meta:face:title']='';
$content['meta:face:url']='';
$content['meta:face:description']='';
$content['LAYOUT']='';
$content['script_load']=array();
$content['style_load']=array();
$content['mobile_detector'] = $detect;
$content['script_code']='';
$XHT=false;
$fer=explode('/',$_SERVER['HTTP_REFERER']);
$fer=@$fer[0].'//'.@$fer[2];
if($fer==$content['DOMAIN'])
	$XHT=true;
if($fer=='https://ico.unifox.io')
{
	header('Access-Control-Allow-Origin: *');
	$XHT=true;	
}	
$content['XMLHttpRequest']=$XHT;

 
if(!isset($_SESSION['last_uri']))
$_SESSION['last_uri']='';
/*if(!isset($_SESSION['autorismus']))
	header('location:/login.php');*/
//$params['controller']=START_PAGE;
if(isset($_GET['p']) and $_GET['p']!='')
	$params['controller']=$_GET['p'];

//echo '<div style="display:none;">'.REGION_USER.'</div>';

if(MODEL!='personal')
{
	//var_dump($params);
	if($params['controller']=='error')
		$content['content']=include PATH_SCRIPT.'/'.MODEL.'/404.php';
	else
	{	
		$patToFiles=PATH_SCRIPT.'/'.MODEL.'/'.$params['controller'].'Page.php';
		if(file_exists($patToFiles)==true)
			$content['content']=include $patToFiles;
		else
			$content['content']=include PATH_SCRIPT.'/'.MODEL.'/404.php';
	}
}
else
{
	//var_dump($_SESSION['confirm_redirect']);
	if(isset($_SESSION['iplocation_tISO']) and $_SESSION['iplocation_tISO']=='US')
		$params['controller']='develop';
	if(!isset($_SESSION[USER_SESSION_NAME]) and $params['controller']!='login')
	{
		if($params['controller']!='develop')
		{
			if($params['controller']=='confirm_transaction')
				$_SESSION['confirm_redirect']=$_SERVER['REQUEST_URI'];
			if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) and $_SERVER["HTTP_X_REQUESTED_WITH"]=="XMLHttpRequest")
				die(json_encode(array('AuthorizationError'=>true)));
			header('Location:/signin/');
		}			
	}
	
	include 'library/api/api.php';
	$API_WALET=new ApiWalet(PATH_ROOT.'qr_code/');
	
	include 'library/api/cache_api.php';
	include 'library/user_visitation.php';
	if($params['controller']=='error')
		$content['content']=include PATH_SCRIPT.'/'.MODEL.'/404.php';
	else
	{
		$content['personal_user_info']=getUserInfo($db_connectors);
		if($content['personal_user_info'])
		{
			get_tokenUser($API_WALET,$content['personal_user_info']);
			$VISITATION=new UsersVisitation($content['personal_user_info']['id'],$db_connectors);
			$VISITATION->put();
		//var_dump($content['personal_user_info']);
			/*if($content['personal_user_info']['activate']==0)
				$params['controller']='develop';*/
		}	
		elseif($params['controller']!='login' and $params['controller']!='develop')
		{
			if(isset($_SESSION[USER_SESSION_NAME.'_t']))
				unset($_SESSION[USER_SESSION_NAME.'_t']);
			if(isset($_SESSION[USER_SESSION_NAME]))
				$AUTH_APP->deleteIdentity();
			header('Location:/signin/');
		}
		
		
		if(IS_ADMIN)
		{
			if($content['personal_user_info']['is_admin']==0)
				$params['controller']=include PATH_SCRIPT.'/'.MODEL.'/404.php';
		}
		/*if($content['personal_user_info']['activate']==0 and $params['controller']!='login')
			$params['controller']='develop';*/
		
		
		$patToFiles=PATH_SCRIPT.'/'.MODEL.'/'.$params['controller'].'Page.php';
		if(file_exists($patToFiles)==true)
			$content['content']=include $patToFiles;
		else
			$content['content']=include PATH_SCRIPT.'/'.MODEL.'/404.php';
	}
}


		
if(!isset($_SERVER["HTTP_X_REQUESTED_WITH"]))
{
		$_SESSION['last_uri']=$_SERVER['REQUEST_URI'];			
		RenderPage($content,$content['LAYOUT']);
		
}
elseif($_SERVER["HTTP_X_REQUESTED_WITH"]=="XMLHttpRequest")
{
	echo $content['content'];
}

ob_end_flush();
?>