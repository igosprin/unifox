$(function () {		
		$('#task-modal').on('hidden.bs.modal', function (event) { 
			$('#task-modal div.modal-body').html('<div class="loader-modal" ></div>');	
		});
		$('a.task-show').each(function(){
			var id=$(this).data('id');
			$(this).click(function(){
				show_tasks(id);
			});
			
		});
		
	});
	function show_tasks(id)
	{
		$('#task-modal').modal('show', {
            backdrop: 'static'
        });
		$.post('/admin/bounty/?ID='+id,{w:''},function(data){
			$('#task-modal .modal-body').html(data.html);
			$('#task-modal .modal-title').html(data.title);
				
		},"json");
	}
	function confirm_(status_)
	{
		$('.error').html('');
		$('form div.loader-page').show();
		$('form #button-b').hide();
		if(!status_)
			var status_=3;
		
		$.post('/admin/bounty/?status='+status_,$('form#bounty').serialize(),function(j){
			if(j.result)
			{
				var n=$('.list tr[data-id="'+j.id+'"] td[data-name="status"]');
				n.html(j.status_text);
				n.removeClass('val1 val2 val3');
				n.addClass('val'+j.status_id);
				$('#task-modal').modal('hide');
			}
			else
			{
				$('form div.loader-page').hide();
				$('form #button-b').show();
				$('.error').html(j.error);
			}	
		},"json");
	}
	function decline_()
	{
		if(confirm('Are you sure you want to decline task'))
			confirm_(2);
	}
	