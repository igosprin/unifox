var RequestFOX={
	modalElement:'#wallets-send',
	init:function(){
		$('table.request tr').each(function(){
			var id_=$(this).data('id');
			console.log(id_);
			$('tr[data-id="'+id_+'"] button#sender').click(function(){
				RequestFOX.sendShow(id_);
			});
		});
	},
	sendShow:function(id){
		$(this.modalElement).modal('show', {
            backdrop: 'static'
        });
		var data=$('tr[data-id="'+id+'"] form#sender_fox').serialize();
		$.post('/admin/requestbuy/?action=form_send',data,function(data){
			$(RequestFOX.modalElement+' .modal-body').html(data.html);
			$(RequestFOX.modalElement+' .modal-title').html(data.title);				
		},"json");
	},
	
};
var RequestModalFOX={
	form:'',
	init:function(id){
	
		this.form='form#send_fox_'+id;
		
		$('div.result').hide();
		$(this.form+' div.loader-page').hide();
		$(this.form+' span.error').html('');	
		$(this.form+' input').each(function(){			
			$(this).attr('readonly','true');
		});	
			
		$(this.form+' button#send').click(function(){
			RequestModalFOX.send();
		});
		
		$(this.form+' input[name="value"]').keyup(function(){
			RequestModalFOX.totalIn();
		});
		$(this.form+' input[name="wallet2"]').keyup(function(){
			RequestModalFOX.wallet();
		});
		
	},
	
	send:function(){		
		$(this.form+' span.error').html('');		
		$(this.form+' button#send').hide();
		$(this.form+' div.loader-page').show();
		var data=$(this.form).serialize();
		
		$.post('/admin/requestbuy/?action=send_fox',data,function(j){
			if(j.result)
			{				
				$('#mideModalForm').hide();				
				$('#txhash').html(j.txhash);
				$('div.result').show();				
			}
			else
			{
				if(j.error)
				{
					if(j.error.wallet2)
						$(RequestModalFOX.form+' span#wallet2').html(j.error.wallet2);
					if(j.error.value)
						$(RequestModalFOX.form+' span#value').html(j.error.value);
					if(j.error.wallet)
						$(RequestModalFOX.form+' span#wallet').html(j.error.wallet);
					
				}
				$(RequestModalFOX.form+' button#send').show();
				$(RequestModalFOX.form+' div.loader-page').hide();
			}
		},"json");
	},
	totalIn:function(){
		$(this.form+' span#value').html('');
		var ufox=parseFloat($('form#buy_fox input[name="value"]').val());		
		if(ufox<=0)
		{
			$(this.form+' span#value').html('Please input correct amount');			
		}
	},
	wallet:function()
	{
		var w2=$(this.form+' input[name="wallet2"]').val();
		console.log(w2.length);
		if(w2.length==0)
		{
			$(this.form+' span#wallet2').html('Please input correct wallet');
		}	
	},
	
}
$(function (){
	RequestFOX.init();	
	$(RequestFOX.modalElement).on('hidden.bs.modal', function (event) { 
				$(RequestFOX.modalElement+' div.modal-body').html('<div class="loader-modal" ></div>');	
			});
});