var UserAdminFOX={
	init:function(){
		$('table.list tr').each(function(){
			var id_user=$(this).data('id');
			$('tr[data-id="'+id_user+'"] a.show').click(function(){
				UserAdminFOX.inputs(id_user);
			});
			$('tr[data-id="'+id_user+'"] a.wallets').click(function(){
				UserAdminFOX.wallets(id_user);
			});
		});
	},
	inputs:function(id){
		$('#user-modal').modal('show', {
            backdrop: 'static'
        });
		$.post('/admin/users/?action=activ_user',{id:id},function(data){
			$('#user-modal .modal-body').html(data.html);
			$('#user-modal .modal-title').html(data.title);
				
		},"json");
	},
	wallets:function(id){
		$('#user-modal').modal('show', {
            backdrop: 'static'
        });
		$.post('/admin/users/?action=wallets',{id:id},function(data){
			$('#user-modal .modal-body').html(data.html);
			$('#user-modal .modal-title').html(data.title);
				
		},"json");
	},
	
};

$(function (){
	UserAdminFOX.init();	
});