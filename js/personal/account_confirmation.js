var ConfirmationFOXDocument={
	form:'form#document_send_form',	
	fileSizeMax:3000000,
	fileError:{},
	init:function(){
		$(ConfirmationFOXDocument.form+' button#save').click(function(){
			ConfirmationFOXDocument.send();
		});
		
		$(ConfirmationFOXDocument.form+' input.hide').each(function(e){
			//var n=$(this);
			if (window.FileReader)
			{
				var name=$(this).attr('name');
				$(this).attr('name',name+'_old');
				$(this).after('<input type="hidden" name="'+name+'">');
				$(this).after('<input type="hidden" name="'+name+'-name">');
				var label=$('label[for="'+name+'"]');
				
				$(label).after('<span id="'+name+'-name" style="margin-left:20px;"><span>');
				if($('span#'+name+'-error').length==0)
					$(label).after('<span id="'+name+'-error" style="margin-left:20px;" class="error"><span>');
				var iset=ConfirmationFOXDocument.issetInput(name);
				if(iset)
					$('span#'+name+'-name').html(iset);						
				
				$(this).change(function(){
					ConfirmationFOXDocument.changeFileInput(this,name);
				});
			}
			
		});		
		$(ConfirmationFOXDocument.form+' input[type="text"]').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});
		
		$(ConfirmationFOXDocument.form+' select').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});
		
	},
	
	changeFileInput:function(obj,name){
		
		ConfirmationFOXDocument.fileError[name]=false;
		$('span#'+name+'-name').html('');
		$('span#'+name+'-error').html('');
		var fileReader = new FileReader(),
		files = obj.files,
		file;
		if (!files.length) return false;
        if(ConfirmationFOXDocument.issetInput(name))
			$('input[name="'+name+'-file"]').remove();            
		file = files[0];
		//console.log(file);
		if (/^image\/\w+$/.test(file.type))
		{
			if(file.size>ConfirmationFOXDocument.fileSizeMax)
			{
				ConfirmationFOXDocument.fileError[name]=true;
				$('span#'+name+'-error').html(PersonalFox.getError('Max size of 3MB'));
				return false;
			}	
			
			fileReader.readAsDataURL(file);
			fileReader.onload = function () {
                    //$inputImage.val();
                   
                $('input[name="'+name+'"]').val(this.result);
                $('input[name="'+name+'-name"]').val(file.name);
				$('span#'+name+'-name').html(file.name);				
			}
                    
			
		}
		else{
			ConfirmationFOXDocument.fileError[name]=true;
			$('span#'+name+'-error').html(PersonalFox.getError('Please chose the image'));
		}
	},
	
	send:function(){
		if(this.getfileError())
			return false;
		this.hideError();		
		var data=$(ConfirmationFOXDocument.form).serialize();
		$(ConfirmationFOXDocument.form+' .loader-page').show();
		$(ConfirmationFOXDocument.form+' button#save').hide();
		$.post('/personal/account-confirmation',data,function(j){
			if(j.result)
			{
				window.location.href='/personal/account-confirmation';
			}
			else
			{
				$(ConfirmationFOXDocument.form+' .loader-page').hide();
				if(j.error)
				{
					var k=Object.keys(j.error);
					for(var i in k)
					{
						$('span#'+k[i]+'-error').html(j.error[k[i]]);						
					}
				}
				$(ConfirmationFOXDocument.form+' button#save').show();		
			}	
		},"json");
	},
	
	hideError:function(){
		$(ConfirmationFOXDocument.form+' span.errorv').html('');
		$(ConfirmationFOXDocument.form+' span.error').html('');
	},
	issetInput:function(name){
		if($('input[name="'+name+'-file"]').length>0)
			return $('input[name="'+name+'-file"]').val();
		return false;
	},
	getfileError:function(){
		var k=Object.keys(this.fileError);
		for(var i in k)
		{
			if(this.fileError[k[i]])
				return true;
							
		}
	}
};
var ConfirmationFOXadres={
	form:'form#adres_send_form',	
	fileSizeMax:3000000,
	fileError:{},
	init:function(){
		$(ConfirmationFOXadres.form+' button#save').click(function(){
			ConfirmationFOXadres.send();
		});
		
		$(ConfirmationFOXadres.form+' input.hide').each(function(e){
			//var n=$(this);
			if (window.FileReader)
			{
				var name=$(this).attr('name');
				$(this).attr('name',name+'_old');
				$(this).after('<input type="hidden" name="'+name+'">');
				$(this).after('<input type="hidden" name="'+name+'-name">');
				var label=$('label[for="'+name+'"]');
				
				$(label).after('<span id="'+name+'-name" style="margin-left:20px;"><span>');
				if($('span#'+name+'-error').length==0)
					$(label).after('<span id="'+name+'-error" style="margin-left:20px;" class="error"><span>');
				
				var iset=ConfirmationFOXadres.issetInput(name);
				if(iset)
					$('span#'+name+'-name').html(iset);						
				
				$(this).change(function(){
					ConfirmationFOXadres.changeFileInput(this,name);
				});
			}
			
		});		
		$(ConfirmationFOXadres.form+' input[type="text"]').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});
		
		$(ConfirmationFOXadres.form+' select').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});
		
	},
	
	changeFileInput:function(obj,name){
		
		ConfirmationFOXadres.fileError[name]=false;
		$('span#'+name+'-name').html('');
		$('span#'+name+'-error').html('');
		var fileReader = new FileReader(),
		files = obj.files,
		file;
		if (!files.length) return false;
        if(ConfirmationFOXadres.issetInput(name))
			$('input[name="'+name+'-file"]').remove();            
		file = files[0];
		//console.log(file);
		if (/^image\/\w+$/.test(file.type))
		{
			if(file.size>ConfirmationFOXadres.fileSizeMax)
			{
				ConfirmationFOXadres.fileError[name]=true;
				$('span#'+name+'-error').html(PersonalFox.getError('Max size of 3MB'));
				return false;
			}	
			
			fileReader.readAsDataURL(file);
			fileReader.onload = function () {
                    //$inputImage.val();
                   
                $('input[name="'+name+'"]').val(this.result);
                $('input[name="'+name+'-name"]').val(file.name);
				$('span#'+name+'-name').html(file.name);				
			}
                    
			
		}
		else{
			ConfirmationFOXadres.fileError[name]=true;
			$('span#'+name+'-error').html(PersonalFox.getError('Please chose the image'));
		}
	},
	
	send:function(){
		if(this.getfileError())
			return false;
		this.hideError();		
		var data=$(ConfirmationFOXadres.form).serialize();
		$(ConfirmationFOXadres.form+' .loader-page').show();
		$(ConfirmationFOXadres.form+' button#save').hide();
		$.post('/personal/account-confirmation',data,function(j){
			if(j.result)
			{
				window.location.href='/personal/account-confirmation';
			}
			else
			{
				$(ConfirmationFOXadres.form+' .loader-page').hide();
				if(j.error)
				{
					var k=Object.keys(j.error);
					for(var i in k)
					{
						$('span#'+k[i]+'-error').html(j.error[k[i]]);						
					}
				}
				$(ConfirmationFOXadres.form+' button#save').show();		
			}	
		},"json");
	},
	
	hideError:function(){
		$(ConfirmationFOXadres.form+' span.errorv').html('');
		$(ConfirmationFOXadres.form+' span.error').html('');
	},
	issetInput:function(name){
		if($('input[name="'+name+'-file"]').length>0)
			return $('input[name="'+name+'-file"]').val();
		return false;
	},
	getfileError:function(){
		var k=Object.keys(this.fileError);
		for(var i in k)
		{
			if(this.fileError[k[i]])
				return true;
							
		}
	}
};
var ConfirmationFOXPersonal={
	form:'form#personal_send_form',	
	init:function(){
		$(this.form+' button#save').click(function(){
			ConfirmationFOXPersonal.send();
		});		
		$(this.form+' input[type="text"]').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});
		$(this.form+' input[type="date"]').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});
	},
	send:function(){
		
		this.hideError();		
		var data=$(this.form).serialize();
		$(this.form+' .loader-page').show();
		$(this.form+' button#save').hide();
		$.post('/personal/account-confirmation',data,function(j){
			if(j.result)
			{
				window.location.href='/personal/account-confirmation';
			}
			else
			{
				$(ConfirmationFOXPersonal.form+' .loader-page').hide();
				if(j.error)
				{
					var k=Object.keys(j.error);
					for(var i in k)
					{
						$('span#'+k[i]+'-error').html(j.error[k[i]]);						
					}
				}
				$(ConfirmationFOXPersonal.form+' button#save').show();		
			}	
		},"json");
	},
	
	hideError:function(){
		$(this.form+' span.errorv').html('');
		$(this.form+' span.error').html('');
	},
	
};
var ConfirmationFOXMail={
	form:'form#email_verified',	
	init:function(){
		$(this.form+' button#get-codes').click(function(){
			
			ConfirmationFOXMail.getCode();
		});	
		$(this.form+' #authenticate button').click(function(){
			
			ConfirmationFOXMail.setCode();
		});
		$(this.form+' button#show-input').click(function(){
			
			ConfirmationFOXMail.showInput();
		});
		$(this.form+' button#save-mail').click(function(){
			
			ConfirmationFOXMail.setMail();
		});
		$(this.form+' #authenticate-new button').click(function(){
			
			ConfirmationFOXMail.setCodeNewMail();
		});
		$(this.form+' a.edit-mail').click(function(){
			
			ConfirmationFOXMail.showEditForm();
		});
		$(this.form+' input[type="text"]').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});	
		
	},
	getCode:function(){		
			
		$(this.form+' div#get-authenticate-code .loader-page').show();
		$(this.form+' button#get-codes').hide();
		$(this.form+' button#show-input').hide();
		var data={createcode:'true'};
		
		$.post('/personal/account-confirmation',data,function(j){
			if(j.result)
			{
				ConfirmationFOXMail.showInput();
				$(ConfirmationFOXMail.form+' div#authenticate .loader-page').hide();
				$(ConfirmationFOXMail.form+' #authenticate button').show();				
			}
			else
			{
				$(ConfirmationFOXMail.form+' div#get-authenticate-code .loader-page').hide();
				$(ConfirmationFOXMail.form+' button#get-codes').show();	
				$(ConfirmationFOXMail.form+' button#show-input').show();	
			}	
		},"json");
	},
	setCode:function(){		
		this.hideError();	
		$(this.form+' div#authenticate .loader-page').show();
		$(this.form+' #authenticate button').hide();
		$.post('/personal/account-confirmation',{emailverif:'true',code_mail:$(this.form+' input[name="code_mail"]').val()},function(j){
			if(j.result)
			{
				window.location.href='/personal/account-confirmation';
			}
			else
			{
				$(ConfirmationFOXMail.form+' div#authenticate .loader-page').hide();
				$(ConfirmationFOXMail.form+' #authenticate button').show();	
				$(ConfirmationFOXMail.form+' #code_mail-error').html(j.error);
			}	
		},"json");
	},	
	setCodeNewMail:function(){		
		this.hideError();	
		$(this.form+' div#authenticate-new .loader-page').show();
		$(this.form+' #authenticate-new button').hide();
		$.post('/personal/account-confirmation',{emailverifnew:'true',code_mail:$(this.form+' input[name="code_mail_new"]').val(),email:$(this.form+' input[name="new_mail_adres"]').val()},function(j){
			if(j.result)
			{
				window.location.href='/personal/account-confirmation';
			}
			else
			{
				$(ConfirmationFOXMail.form+' div#authenticate-new .loader-page').hide();
				$(ConfirmationFOXMail.form+' #authenticate-new button').show();	
				$(ConfirmationFOXMail.form+' #code_mail_new-error').html(j.error);
			}	
		},"json");
	},
	showInput:function(){
		$(ConfirmationFOXMail.form+' div#get-authenticate-code').hide();
		$(ConfirmationFOXMail.form+' div#authenticate').show();
	},
	showEditForm:function(){
		$(ConfirmationFOXMail.form+' div#get-authenticate-code').hide();
		$(ConfirmationFOXMail.form+' div#authenticate').hide();
		$(ConfirmationFOXMail.form+' div#authenticate-new').hide();
		$(ConfirmationFOXMail.form+' div#edit-mail-form').show();
		$(ConfirmationFOXMail.form+' a.edit-mail').hide();
		
	},
	hideError:function(){
		$(this.form+' span.errorv').html('');		
	},
	setMail:function(){
		this.hideError();
		$(this.form+' button#save-mail').hide();
		$('#edit-mail-form .loader-page').show();
		$.post('/personal/account-confirmation',{newemailcreate:'',email:$('input[name="new_mail_adres"]').val()},function(j){
			if(j.result)
			{
				$('#edit-mail-form').hide();
				$('#authenticate-new').show();
			}
			else
			{
				$(ConfirmationFOXMail.form+' button#save-mail').show();
				$('#edit-mail-form .loader-page').hide();
				if(j.error)
					$('#new_mail_adres-error').html(j.error);					
			}
		},"json");
	},
	
	
};

var ConfirmationFOXCompany={
	form:'form#company_send_form',	
	fileSizeMax:3000000,
	fileError:{},
	init:function(){
		$(ConfirmationFOXCompany.form+' button#save').click(function(){
			ConfirmationFOXCompany.send();
		});
		
		$(ConfirmationFOXCompany.form+' input.hide').each(function(e){
			//var n=$(this);
			if (window.FileReader)
			{
				var name=$(this).attr('name');
				$(this).attr('name',name+'_old');
				$(this).after('<input type="hidden" name="'+name+'">');
				$(this).after('<input type="hidden" name="'+name+'-name">');
				var label=$('label[for="'+name+'"]');
				
				$(label).after('<span id="'+name+'-name" style="margin-left:20px;"><span>');
				if($('span#'+name+'-error').length==0)
					$(label).after('<span id="'+name+'-error" style="margin-left:20px;" class="error"><span>');
				
				var iset=ConfirmationFOXCompany.issetInput(name);
				if(iset)
					$('span#'+name+'-name').html(iset);						
				
				$(this).change(function(){
					ConfirmationFOXCompany.changeFileInput(this,name);
				});
			}
			
		});		
		$(ConfirmationFOXCompany.form+' input[type="text"]').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});		
	},
	
	changeFileInput:function(obj,name){
		
		ConfirmationFOXCompany.fileError[name]=false;
		$('span#'+name+'-name').html('');
		$('span#'+name+'-error').html('');
		var fileReader = new FileReader(),
		files = obj.files,
		file;
		if (!files.length) return false;
        if(ConfirmationFOXCompany.issetInput(name))
			$('input[name="'+name+'-file"]').remove();            
		file = files[0];
		//console.log(file);
		if (/^image\/\w+$/.test(file.type))
		{
			if(file.size>ConfirmationFOXCompany.fileSizeMax)
			{
				ConfirmationFOXCompany.fileError[name]=true;
				$('span#'+name+'-error').html(PersonalFox.getError('Max size of 3MB'));
				return false;
			}	
			
			fileReader.readAsDataURL(file);
			fileReader.onload = function () {
                    //$inputImage.val();
                   
                $('input[name="'+name+'"]').val(this.result);
                $('input[name="'+name+'-name"]').val(file.name);
				$('span#'+name+'-name').html(file.name);				
			}
                    
			
		}
		else{
			ConfirmationFOXCompany.fileError[name]=true;
			$('span#'+name+'-error').html(PersonalFox.getError('Please chose the image'));
		}
	},
	
	send:function(){
		if(this.getfileError())
			return false;
		this.hideError();		
		var data=$(ConfirmationFOXCompany.form).serialize();
		$(ConfirmationFOXCompany.form+' .loader-page').show();
		$(ConfirmationFOXCompany.form+' button#save').hide();
		$.post('/personal/account-confirmation',data,function(j){
			if(j.result)
			{
				window.location.href='/personal/account-confirmation';
			}
			else
			{
				$(ConfirmationFOXCompany.form+' .loader-page').hide();
				if(j.error)
				{
					var k=Object.keys(j.error);
					for(var i in k)
					{
						$('span#'+k[i]+'-error').html(j.error[k[i]]);						
					}
				}
				$(ConfirmationFOXCompany.form+' button#save').show();		
			}	
		},"json");
	},
	
	hideError:function(){
		$(ConfirmationFOXCompany.form+' span.errorv').html('');
		$(ConfirmationFOXCompany.form+' span.error').html('');
	},
	issetInput:function(name){
		if($('input[name="'+name+'-file"]').length>0)
			return $('input[name="'+name+'-file"]').val();
		return false;
	},
	enableElement:function()
	{
		ConfirmationFOX.enableElement(ConfirmationFOXCompany.form);
	},
	getfileError:function(){
		var k=Object.keys(this.fileError);
		for(var i in k)
		{
			if(this.fileError[k[i]])
				return true;
							
		}
	}
};

var ConfirmationFOX={
	enableElement:function(form){
		$(form+' input[type="text"]').each(function(){
			$(this).attr('readonly','true');			
		});
		
		$(form+' select').each(function(){
			$(this).attr('readonly','true');
		});
		$(form+' input[type="date"]').each(function(){
			$(this).attr('readonly','true');			
		});
	},
};
$(function () {		
	ConfirmationFOXDocument.init();
	ConfirmationFOXadres.init();
	ConfirmationFOXPersonal.init();
	ConfirmationFOXMail.init();			
	ConfirmationFOXCompany.init();			
	$('.loader-page').css('width:100px;');
	$('.loader-page').css('height:100px;');
});