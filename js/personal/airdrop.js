var Airdrop={
	get_:'',
	init:function(){		
		$('input[type="text"]').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});
		$('form.airdrop-form').each(function(){		
			var id=$(this).attr('id');
			
			var w=$('form#'+id+' button#send').width();
			console.log(w);
			$('form#'+id+' div.loader-page').width(w);
			$('form#'+id+' div.loader-page').height(40);
			$('form#'+id+' button#send').click(function(){
				Airdrop.send(id);
			});
		});
		
		
	},
	addGet:function(get){
		this.get_=get;
	},
	send:function(id){
			
		this.hideError(id);		
		var data=$('form#'+id).serialize();
		$('form#'+id+' div.loader-page').show();
		$('form#'+id+' button#send').hide();
		$.post(Language_prefix+'/personal/airdrop'+this.get_,data,function(j){
			if(j.result)
			{
				//Airdrop.disabledElement(id);
				$('form#'+id+' div.loader-page').hide();
				PersonalFox.goToUrl('/personal/airdrop'+Airdrop.get_);
			}
			else
			{
				$('form#'+id+' div.loader-page').hide();
				if(j.error)
				{
					var k=Object.keys(j.error);
					for(var i in k)
					{
						$('span#'+k[i]+'-error').html(j.error[k[i]]);						
					}
				}
				$('form#'+id+' button#send').show();			
			}	
		},"json");
	},
	/*disabledElement:function(id){
		$($('form#'+id+' input[type="text"]').each(function(){
			$(this).attr('disabled','disabled');
		});	
	},*/
	hideError:function(id){
		$('form#'+id+' span.errorv').html('');
		
	}
	
};
var GiftSection={
	init:function(){		
		$('form#gift input[type="text"]').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});	
	},
	send:function(){
		$('form#gift span.errorv').html('');
		var data=$('#gift').serialize();
		$('form#gift div.loader-page').show();
		$('#send-gift').hide();
		$.post(Language_prefix+'/personal/airdrop',data,function(j){
			if(j.result)
			{
				$('form#gift div.loader-page').hide();
				$('#send-gift').remove();
				PersonalFox.getBalance();	
			}
			else
			{
				$('form#gift div.loader-page').hide();
				if(j.error)
				{
					var k=Object.keys(j.error);
					for(var i in k)
					{
						$('span#'+k[i]+'-error').html(j.error[k[i]]);						
					}
				}
				$('#send-gift').show();			
			}	
		},"json");
	}
};

$(function () {		
	Airdrop.init();
	GiftSection.init();
	$(document).on('click', '#send-airpod', function (e) {
		Airdrop.preloader();
	});
	$(document).on('click', '#send-gift', function (e) {
		GiftSection.send();
	});
});