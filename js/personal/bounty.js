var BountyFox={
	pageIn:{},
	init:function(){		
		$('form.bounty-form').each(function(){
			var type_f=$(this).attr('id');
			BountyFox.initError(type_f);
			BountyFox.pageIn[type_f]=1;
			BountyFox.getTask(type_f);
			$('#'+type_f+'_taskBlock button#next_tasks').click(function(){
				BountyFox.getTask(type_f);
			});
			$('form#'+type_f+' button#send-bounty').click(function(){
				BountyFox.send(type_f);
			});
			$("#foxBounty li:eq(0) a").tab('show');
		});
		
		
	},
	initError:function(type)
	{
		$('form#'+type+' input[type="text"]').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});
	},
	
	send:function(type){		
		this.hideError();		
		var data=$('form#'+type).serialize();
		$('form#'+type+' div.loader-page').show();
		$('form#'+type+' #send-bounty').hide();
		$.post(Language_prefix+'/personal/bounty',data,function(j){
			if(j.result)
			{
				$('form#'+type+' div.loader-page').hide();
				$('form#'+type+' #send-bounty').show();
				BountyFox.addToTask(type,j.html);
				BountyFox.emptyElement(type);
			}
			else
			{
				$('form#'+type+' div.loader-page').hide();
				if(j.error)
				{
					var k=Object.keys(j.error);
					for(var i in k)
					{
						$('span#'+k[i]+'-error').html(j.error[k[i]]);						
					}
				}
				$('form#'+type+' #send-bounty').show();			
			}	
		},"json");
	},
	addToTask:function(type,data)
	{
		for(var i in data)
		{
			$('table#'+type+'_user-task').prepend(data[i]);
		}
	},
	disabledElement:function(type){
		$('form#'+type+' input[type="text"]').each(function(){
			$(this).attr('disabled','disabled');
		});	
	},
	emptyElement:function(type){
		$('form#'+type+' input[type="text"]').each(function(){
			$(this).val('');
		});	
	},
	hideError:function(){
		$('span.errorv').html('');
		$('span.error').html('');
	},
	disabledElementAll:function(type){
		$('#bodybounty input[type="text"]').each(function(){
			$(this).attr('disabled','disabled');
		});	
	},
	bountyDisabled:function(){
		this.disabledElementAll();	
		var w=$('#bodybounty').width();
		var h=$('#bodybounty').height();
		$('.nonecontent').css('height',h+'px').css('width',w+'px');
		$('.nonecontent').show();
		$('#airdrop-none').modal('show', {
            backdrop: 'static'
        });
	},
	getTask:function(type)
	{
		$('#'+type+'_taskBlock button#next_tasks').hide();
		$('#'+type+'_taskBlock div.loader-page').show();
		$.post(Language_prefix+'/personal/bounty',{get_task:1,'page':this.pageIn[type],'type':type},function(j){
			if(j.result)
			{
				$('#'+type+'_taskBlock div.loader-page').hide();
				$('table#'+type+'_user-task tbody').append(j.html);
				if(j.next){
					$('#'+type+'_taskBlock button#next_tasks').show();
					BountyFox.pageIn[type]++;
				}
			}
			else
			{
				$('#'+type+'_taskBlock div.loader-page').hide();
				if(j.html)
					$('table#'+type+'_user-task tbody').append(j.html);
			}			
		},"json");
		
	},
	
	
};

$(function () {		
	BountyFox.init();	
});