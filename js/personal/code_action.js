var CodeActionFOX={
	isset:false,
	obj:null,
	objBody:null,
	init:function(param){
		if(!this.isset)
		{
			if($(param.idObject).length > 0)
			{
				this.obj=param.idObject;
				this.objBody=this.obj+' '+param.objBody;
				$(this.obj).hide();
				this.view();
				
			}	
		}
	},
	view:function(){
		$.post('/personal/codesale',{type:'show'},function(j){
			if(j.result)
			{
				$(CodeActionFOX.objBody).html(j.html);
				$(CodeActionFOX.obj).show();
				CodeActionFOX.isset=true;
				$(CodeActionFOX.objBody+" button#send").bind( "click", function() {
					CodeActionFOX.send();
				});
				$(CodeActionFOX.objBody+' span.errorv').html('');	
			}
			else CodeActionFOX.isset=false;			
		},"json");
	},
	send:function(){
		$(CodeActionFOX.objBody+' span.errorv').html('');
		var data=$(this.objBody+' form').serialize();
		$(this.obj+' div.loader-page').show();
		$(CodeActionFOX.objBody+" button#send").hide();
		$.post('/personal/codesale',data,function(j){
			if(j.result)
			{
				$(CodeActionFOX.objBody+' div.loader-page').hide();				
				$(CodeActionFOX.objBody+' input[name="code_sale"]').hide();				
				$(CodeActionFOX.objBody+' div.text-info').hide();				
				$(CodeActionFOX.objBody+' div.result-text').html(j.txt);				
			}
			else
			{
				$(CodeActionFOX.objBody+' div.loader-page').hide();	
				$(CodeActionFOX.objBody+" button#send").show();
				if(j.error)
				{
					var k=Object.keys(j.error);
					for(var i in k)
					{
						$('span#'+k[i]+'-error').html(j.error[k[i]]);						
					}
				}
			}	
		},"json");
	},
	
};
$(function () {	
	CodeActionFOX.init({idObject:'.codesale',objBody:'div.content-body'});
	
});