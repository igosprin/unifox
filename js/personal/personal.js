var PersonalFox={
	balanceElement:'#my_total_balance',
	takesElement:'#my_stakes_balance',
	process:false,
	getBalance:function()
	{
		$.post(Language_prefix+'/personal/event?type=my_balance',{time:'strict'},function(j){
			if(j.AuthorizationError)
			{
				this.goToUrl("/signin");
				return;
			}
			if(j.result)
			{
				$(PersonalFox.balanceElement).html(j.balance);
				if(j.w_balance)
				{
					var k=Object.keys(j.w_balance);
					for(var i in k)
					{
						if($('#wallet_balance_'+k[i]).length>0)
							$('#wallet_balance_'+k[i]).html(j.w_balance[k[i]]);
					}
				}
				setTimeout(PersonalFox.getBalance.bind(PersonalFox),30000);
				
			}
			
		},"json");
	},
	getStakes:function(){
		$.post(Language_prefix+'/personal/event?type=my_stakes',{time:'strict'},function(j){
			$(PersonalFox.takesElement).html(j.stakes);
		},"json");
	},
	anchorList:function(){
		$('div[data-anchor="link"]').click( function (event) {
			
			var id  = $(this).attr('anchor-id');			
			var top = $('#'+id).offset().top;
			
			$('body,html').animate({scrollTop: top-50}, 800);
			
		});
	},
	goToUrl:function(url){
		window.location.href=Language_prefix+url;
	},
	linkList:function(){
		$('div[data-link="href"]').click( function () {
			
			var url = $(this).attr('data-href');
			if(url!='')	
				PersonalFox.goToUrl(url);
			
		});
	},
	getError:function(phrase)
	{
		if(Translate[phrase])
			return Translate[phrase];
		else return phrase;
	}
	
};

$(function () {		
	var clipboard = new ClipboardJS('#copy-to-buffer');
	PersonalFox.getStakes();
	PersonalFox.anchorList();
	PersonalFox.linkList();
	PersonalFox.getBalance();
	
});

