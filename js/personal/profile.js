var Profile={
	init:function(){
		$('input.hide').each(function(e){
			//var n=$(this);
			if (window.FileReader)
			{
				var name=$(this).attr('name');
				$(this).attr('name',name+'_old');
				$(this).after('<input type="hidden" name="'+name+'">');
				$(this).after('<input type="hidden" name="'+name+'-name">');
				var label=$('label[for="'+name+'"]');
				
				$(label).after('<span id="'+name+'-name"><span>');
				$(label).after('<span id="'+name+'-error" class="error"><span>');
				var iset=Profile.issetInput(name);
				if(iset)
				{
					//console.log(iset);
					$('span#'+name+'-name').html(iset);						
				}	
				
				
				$(this).change(function(){
					Profile.changeFileInput(this,name);
				});
			}
			
		});
		$('input[type="text"]').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});
		$('input[type="date"]').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});
		$('select').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});
		
	},
	changeFileInput:function(obj,name){
		
		$('span#'+name+'-name').html('');
		$('span#'+name+'-error').html('');
		var fileReader = new FileReader(),
		files = obj.files,
		file;
		if (!files.length) return false;
        if(Profile.issetInput(name))
			$('input[name="'+name+'-file"]').remove();            
		file = files[0];
		//console.log(file);
		if (/^image\/\w+$/.test(file.type))
		{
			fileReader.readAsDataURL(file);
			fileReader.onload = function () {
                    //$inputImage.val();                    
                $('input[name="'+name+'"]').val(this.result);
                $('input[name="'+name+'-name"]').val(file.name);
				$('span#'+name+'-name').html(file.name);				
			}
                    
			
		}
		else
			$('span#'+name+'-error').html('Please chose the image');
	},
	
	preloader:function(){
		this.hideError();		
		var data=$('#preloader').serialize();
		$('.loader-page').show();
		$('#send-verification').hide();
		$.post('/personal/profile',data,function(j){
			if(j.result)
			{
				$('.loader-page').hide();
				$('#button-verification-block').remove();
				$('#verifed').show();				
				Profile.disabledElement();	
				$('#moderator_comment').hide();
				$('.fa-warning').hide();
			}
			else
			{
				$('.loader-page').hide();
				if(j.error)
				{
					var k=Object.keys(j.error);
					for(var i in k)
					{
						$('span#'+k[i]+'-error').html(j.error[k[i]]);						
					}
				}
				$('#send-verification').show();			
			}	
		},"json");
	},
	disabledElement:function(){
		$('input[type="text"]').each(function(){
			$(this).attr('disabled','disabled');
		});
		$('input[type="date"]').each(function(){
			$(this).attr('disabled','disabled');
		});
		$('select').each(function(){
			$(this).attr('disabled','disabled');
		});
		$('label.file').each(function(){
			$(this).attr('disabled','disabled');
		});
		
	},
	hideError:function(){
		$('span.errorv').html('');
		$('span.error').html('');
	},
	issetInput:function(name){
		if($('input[name="'+name+'-file"]').length>0)
			return $('input[name="'+name+'-file"]').val();
		return false;
	}
	
};

$(function () {		
	Profile.init();
	$(document).on('click', '#send-verification', function (e) {
		Profile.preloader();
	});
});