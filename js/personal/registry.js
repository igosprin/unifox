/*
* Copyright 2017, Ihor Sprinchan
*/
var RegistryUnifox={
	activePostQuery:false,
	activeObject:false,
	activeObjectId:false,
	elementHideClass:'hide-block',
	errorIsset:{},	
	trueCount:0,
	passwordTrueCount:6,
	userNameTrueCount:1,
	prefixError:'✘ ',	
	prefixTrue:'✔ ',	
	init:function(id)
	{
		this.getIdFormNoModal(id);
		//console.log(this.activeObjectId);
		if(this.activeObjectId)
		{	
			this.recompliteError();
			$('div#'+this.activeObjectId+' form input.ng-pristine').each(function(indx){				
				var name=$(this).attr('name');
				
				$(this).keyup(function(){
					RegistryUnifox.validation(name,$(this).val(),'key');
				});
				$(this).blur(function(){
					RegistryUnifox.validation(name,$(this).val(),'blur');
				});
				//console.log($(this).val());
				/*if($(this).val()!=""){					
					RegistryUnifox.validation(name,$(this).val(),'blur');
				}*/
				
			});			
			
			if(this.activeObject=='Repaswd')
				RegistryUnifox.validation('email',$('input[name="email"]').val());
		}
	},
	
	getIdFormNoModal:function(idModalHTML)
	{
		this.activeObject=idModalHTML;
		
		if(this.activeObject)
		{			
			
			this.activeObjectId=$('div[modal-id="'+this.activeObject+'"]').attr('id');
			
			if(idModalHTML=='SignIn')
				this.trueCount=2;
			if(idModalHTML=='SignUp')
				this.trueCount=4;
			if(idModalHTML=='Repaswd')
				this.trueCount=1;
			if(idModalHTML=='Repaswd_new')
				this.trueCount=2;
			
			this.errorIsset={};
		}
	},
	recompliteError:function(name)
	{
		if(!name)
		{
			$('div#'+this.activeObjectId+' form span.error').each(function(indx){
				
				$(this).addClass(RegistryUnifox.elementHideClass);
				$(this).html('');
			});
			
			$('div#'+this.activeObjectId+' form span.success').each(function(indx){
				$(this).addClass(RegistryUnifox.elementHideClass);
				$(this).html('');
			});
		}
		else
		{			
			
			$('div#'+this.activeObjectId+' form span.success[rel="'+name+'"]').addClass(this.elementHideClass);			
			$('div#'+this.activeObjectId+' form span.error[rel="'+name+'"]').addClass(this.elementHideClass);
		}
		//$('div#'+this.activeObjectId+' form button[type="submit"]').attr('disabled','disabled');	
		
	},
	validation:function(name,value,events)
	{
		
		if(!events) events='blur';
		if(events=='blur'){
			if(value.length==0)
			return;
		}
		
		this.recompliteError(name);
		var error=true;
		var codeError=false;
		
		switch(name)
		{
			case 'email':
				var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
				if(!pattern.test(value))
				{
					error=false;
					this.errorIsset['email']=false;	
					codeError='Registration-error-email';	
				}
				else
					this.errorIsset['email']=true;										
			break;
			case 'password':			
				if(value.length<this.passwordTrueCount)
				{
					error=false;
					this.errorIsset['password']=false;	
					codeError='Registration-error-passord';	
				}
				else
					this.errorIsset['password']=true;
				
				if(this.activeObjectId=='SignUp-modal')
				{
					
					var pasw_re=$('div#'+this.activeObjectId+' input[name="password_confirmation"]').val();					
					this.validation('password_confirmation',pasw_re);
				}
				if(this.activeObjectId=='Repaswd_new-modal')
				{
					
					var pasw_re=$('div#'+this.activeObjectId+' input[name="password_confirmation"]').val();
						
					this.validation('password_confirmation',pasw_re);
				}
				
					
			break;
			case 'name':			
				if(value.length<this.userNameTrueCount)
				{
					error=false;
					this.errorIsset['name']=false;	
					codeError='Registration-error-name';	
				}
				else
					this.errorIsset['name']=true;			
					
			break;
			case 'password_confirmation':
				var pasw=$('div#'+this.activeObjectId+' input[name="password"]').val();
				
				if(value!=pasw)
				{
					error=false;
					this.errorIsset['password_confirmation']=false;	
					codeError='Registration-error-password2';	
				}
				else
					this.errorIsset['password_confirmation']=true;
										
			break;
		}
		
		if(!error)
			this.errorView(name,codeError);
		else
			this.successView(name);	
		
	},
	errorView:function(name,errorKey,errorText)
	{
		//console.log(errorKey);
		if(errorKey)
			$('div#'+this.activeObjectId+' form span.error[rel="'+name+'"]').html(this.prefixError+Translate[errorKey]);
		if(errorText)
			$('div#'+this.activeObjectId+' form span.error[rel="'+name+'"]').html(this.prefixError+errorText);	
		$('div#'+this.activeObjectId+' form span.error[rel="'+name+'"]').removeClass(this.elementHideClass);
		this.viewedButton();
	},
	successView:function(name)
	{		
		$('div#'+this.activeObjectId+' form span.success[rel="'+name+'"]').html(this.prefixTrue);
		$('div#'+this.activeObjectId+' form span.success[rel="'+name+'"]').removeClass(this.elementHideClass);		
		
		this.viewedButton();
	},
	viewedButton:function(form){		
		var k=Object.keys(this.errorIsset);
		var trueCount=0;
		for(var item in k)
		{
			if(this.errorIsset[k[item]])
				trueCount++;			
		}
		
		
		if(trueCount==this.trueCount)
		{
			//$('div#'+this.activeObjectId+' form button').removeAttr('disabled');
			if(form)
				return false;
		}
			
		if(form)
			return true;
		
		
	},
	createEr:function(data){		
		var k=Object.keys(data);
		var trueCount=0;
		for(var item in k)
		{
			this.validation(k[item],data[k[item]]);					
		}
		
		return this.viewedButton(true);
			
	},
	createAcp:function()
	{
		this.recompliteError();
		var data=$('#'+this.activeObjectId+' form').serialize();
		/*if(!this.createEr(data))
			return false;*/
		$.post(Language_prefix+'/signup/',data,function(json){
			if(!json.result)
			{
				var k=Object.keys(json.error);
				for(var item in k)
				{
					RegistryUnifox.errorIsset[k[item]]=false;	
					RegistryUnifox.errorView(k[item],json.error[k[item]]);
				}				
			}
			else
			{
				if(json.activation)
				{
					RegistryUnifox.activateLinkSend();
				}	
				else
					window.location.href=json.acp_url;
				//FangoModal.set_view('Welcome',json.welcome);
				
			}
			
		},"json");
	},
	
	enterAcp:function()
	{
		this.recompliteError();
	    //$('#result').addClass(elementHideClass);
		$.post(Language_prefix+'/signin/',$('#'+this.activeObjectId+' form').serialize(),function(json){
			if(!json.result)
			{
				var k=Object.keys(json.error);				
				for(var item in k)
				{
					RegistryUnifox.errorIsset[k[item]]=false;	
					RegistryUnifox.errorView(k[item],json.error[k[item]]);
				}
				/*if(json.error.result)
				{
					$('div#'+RegistryUnifox.activeObjectId+' form button').addAttr('disabled');
				}*/
			}
			else
			{
				window.location.href=json.acp_url;
			}
			
		},"json");
	},
	
	reset:function()
	{
		$('div#repass_succes').css('display','none');
		var email_val=$('div#'+this.activeObjectId+' input[name="email"]').val();
		this.recompliteError('email');
		var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
		
		if(!pattern.test(email_val))	
			this.errorIsset['email']=false;	
		else this.errorIsset['email']=true;
		
		
		if(RegistryUnifox.errorIsset['email'])
		{
			$.post(Language_prefix+'/recovery/',$('#'+this.activeObjectId+' form').serialize(),function(json){
				if(!json.result)
				{
					var k=Object.keys(json.error);
					for(var item in k)
					{
						RegistryUnifox.errorIsset[k[item]]=false;	
						RegistryUnifox.errorView(k[item],json.error[k[item]]);
					}				
				}
				else
				{
					$('div#repass_succes').css('display','block');
					$('div#'+RegistryUnifox.activeObjectId).hide();
				}
				
			},"json");
		}
		else this.errorView('email','Registration-error-1');	
	},
	repasswordCreater:function()
	{
		this.recompliteError();
		this.errorIsset['email']=true;
		$.post(Language_prefix+'/recovery_access/',$('#'+this.activeObjectId+' form').serialize(),function(json){
				if(!json.result)
				{
					var k=Object.keys(json.error);
					for(var item in k)
					{
						RegistryUnifox.errorIsset[k[item]]=false;	
						RegistryUnifox.errorView(k[item],json.error[k[item]]);
					}				
				}
				else
				{
					window.location.href=json.acp_url;
				}
				
			},"json");
	},
	ModalAcpView:function(idView)
	{
		$('div.card-login').addClass(this.elementHideClass);
		if(idView)
		{
			$('div [modal-id="'+idView+'"]').removeClass(this.elementHideClass);
			RegistryUnifox.getIdFormNoModal(idView);
			RegistryUnifox.init();
		}
			
	},
	activateLinkSend:function()
	{
		var data=$('#'+this.activeObjectId+' form').serialize();
		
		$.post(Language_prefix+'/activate_link/',data,function(json){			
			$('div#activate p').html(json.text);
			$('div#activate').css('display','block');
			$('div#'+RegistryUnifox.activeObjectId).hide();	
		},"json");
	}
	
}