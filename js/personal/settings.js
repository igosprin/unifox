var SettingsFOX={
	fileSizeMax:800000,
	fileError:{},
	AvatarElement:'div.avatar-img-wrapper img',
	init:function(){
		$('.loader-page').css('width','100px');	
		$('input.hide').each(function(e){
			//var n=$(this);
			if (window.FileReader)
			{
				var name=$(this).attr('name');
				$(this).attr('name',name+'_old');
				$(this).after('<input type="hidden" name="'+name+'">');
				$(this).after('<input type="hidden" name="'+name+'-name">');
				var label=$('label[for="'+name+'"]');
				SettingsFOX.fileError[name]=false;
				$(label).after('<span id="'+name+'-name" style="margin-left:20px;"><span>');
				$(label).after('<span id="'+name+'-error" class="error" style="margin-left:20px;"><span>');
				/*var iset=SettingsFOX.issetInput(name);
				if(iset)
				{
					
					$('span#'+name+'-name').html(iset);						
				}*/	
				$('#change_saved').hide();
				
				$(this).change(function(){
					SettingsFOX.changeFileInput(this,name);
				});
			}
			
		});
		$('input[type="text"]').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});
		$('input[type="password"]').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});
		
		
	},
	changeFileInput:function(obj,name){
		
		SettingsFOX.fileError[name]=false;
		$('span#'+name+'-name').html('');
		$('span#'+name+'-error').html('');
		var fileReader = new FileReader(),
		files = obj.files,
		file;
		if (!files.length) return false;
        if(SettingsFOX.issetInput(name))
			$('input[name="'+name+'-file"]').remove();            
		file = files[0];
		//console.log(file);
		if (/^image\/\w+$/.test(file.type))
		{
			if(file.size>SettingsFOX.fileSizeMax)
			{
				SettingsFOX.fileError[name]=true;
				$('span#'+name+'-error').html(PersonalFox.getError('Max size of 800K'));
				return false;
			}	
			
			fileReader.readAsDataURL(file);
			fileReader.onload = function () {
                    //$inputImage.val();
                   
                $('input[name="'+name+'"]').val(this.result);
                $('input[name="'+name+'-name"]').val(file.name);
				$('span#'+name+'-name').html(file.name);
				//SettingsFOX.setAvatarSrc(this.result);	
			}
                    
			
		}
		else{
			SettingsFOX.fileError[name]=true;
			$('span#'+name+'-error').html(PersonalFox.getError('Please chose the image'));
		}
	},
	setAvatarSrc:function(val){
		if(val.length)
			$(this.AvatarElement).attr('src',val);
		
	},
	save:function(){
		if(this.getfileError())
			return false;
		this.hideError();		
		var data=$('#settings').serialize();
		$('.loader-page').show();
		$('#save').hide();
		$('#change_saved').hide();
		$('#reset').hide();
		$.post('/personal/settings',data,function(j){
			if(j.result)
			{
				$('.loader-page').hide();
				$('#save').show();
				$('#reset').show();	
				SettingsFOX.showChangeOk();
				if(j.data)
				{
					if(j.data.name)
						$('.profiler-info a span').html(j.data.name);
					if(j.data.img){
						$('.profiler-info img[alt="user-image"]').attr('src',j.data.img);
						$('.avatar-img-wrapper img').attr('src',j.data.img);
						$('.avatar-img-wrapper img').css('padding','0px');
						$('span#img-name').html('');
					}
				}					
			}
			else
			{
				$('.loader-page').hide();
				if(j.error)
				{
					var k=Object.keys(j.error);
					for(var i in k)
					{
						$('span#'+k[i]+'-error').html(j.error[k[i]]);						
					}
				}
				$('#save').show();
				$('#reset').show();		
			}	
		},"json");
	},
	disabledElement:function(){
		$('input[type="text"]').each(function(){
			$(this).attr('disabled','disabled');
		});
		$('input[type="password"]').each(function(){
			$(this).attr('disabled','disabled');
		});
		$('label.file').each(function(){
			$(this).attr('disabled','disabled');
		});
		
	},
	hideError:function(){
		$('span.errorv').html('');
		$('span.error').html('');
	},
	issetInput:function(name){
		if($('input[name="'+name+'-file"]').length>0)
			return $('input[name="'+name+'-file"]').val();
		return false;
	},
	showChangeOk:function()
	{
		$('#change_saved').show();
		$("#change_saved").fadeOut(3000);
		
	},
	getfileError:function(){
		var k=Object.keys(this.fileError);
		for(var i in k)
		{
			if(this.fileError[k[i]])
				return true;
							
		}
	},
	
};

var SettingsPasswordFOX={
	initForm:function(){
		this.emptyInput();
		$('input[type="password"]').each(function(){
			var name=$(this).attr('name');
			$(this).after('<span id="'+name+'-error" class="errorv"><span>');
		});
		$('button#save_pwd').click(function(){
			SettingsPasswordFOX.save();
		});
		$('button#cancel_pwd').click(function(){
			$('#password-modal').modal('hide');
		});
	},
	emptyInput:function(){
		$('input[type="password"]').each(function(){
			$(this).val('');
		});
	},
	save:function(){
		
		this.hideError();		
		var data=$('#passwordchange-form').serialize();
		$('#passwordchange-form .loader-page').show();
		$('button#save_pwd').hide();		
		$('button#cancel_pwd').hide();
		$.post('/personal/settings',data,function(j){
			if(j.result)
			{
				/*$('.loader-page').hide();
				$('button#save_pwd').show();		
				$('button#cancel_pwd').show();	*/		
				window.location.href="/personal/settings";	
			}
			else
			{
				$('.loader-page').hide();
				if(j.error)
				{
					var k=Object.keys(j.error);
					for(var i in k)
					{
						$('span#'+k[i]+'-error').html(j.error[k[i]]);						
					}
				}
				$('button#save_pwd').show();		
				$('button#cancel_pwd').show();	
			}	
		},"json");
	},
	hideError:function(){
		$('span.errorv').html('');
	},
	
};

$(function () {		
	SettingsFOX.init();
	$(document).on('click', '#save', function (e) {
		SettingsFOX.save();
	});
	$(document).on('click', '#password-form', function (e) {
		
		$('#password-modal').modal('show', {backdrop: 'static'});
		$.post('/personal/settings/',{passwordform:'true'},function(data){
			$('#password-modal .modal-body').html(data.html);
			$('#password-modal .modal-title').html(data.title);
				
		},"json");
	});
	$('#password-modal').on('hidden.bs.modal', function (event) { 
				$('#password-modal div.modal-body').html('<div class="loader-modal" ></div>');	
			});	
	
});