var TransactionFox={
	bodyElement:'#my_transaction',	
	tableElement:'.list',	
	process:false,
	type:'send',
	w:'',
	init:function(wallet){
		if(wallet)
			this.w=wallet;
		$('a.type').each(function(){
			TransactionFox.setType($(this).data('type'));
		});
		this.get();
	},
	get:function(page)
	{
		$(TransactionFox.bodyElement+' .loader-page').show();
		$(TransactionFox.bodyElement+' '+TransactionFox.tableElement).hide();
		var get_='?type='+this.type;
		if(page)
			get_=get_+'&page='+page;	
		$.post(Language_prefix+'/personal/transaction/'+get_,{w:this.w,time:'strict'},function(j){
			if(j.result)
			{
				$(TransactionFox.bodyElement+' '+TransactionFox.tableElement).html(j.html);
				$(TransactionFox.bodyElement+' .loader-page').hide();
				$(TransactionFox.bodyElement+' '+TransactionFox.tableElement).show();
			}
			
		},"json");
	},
	setType:function(type){
		this.type=type;
		TransactionFox.get();
	},
	
	
};