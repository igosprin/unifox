var WalletFOX={
	modal:'#wallets-modal',
	modalShow:false,
	showModal:function(title,html)
	{
		$(this.modal).modal('show', {
            backdrop: 'static'
        });
		this.modalShow=true;
		$(this.modal+' .modal-body').html(html);
		$(this.modal+' .modal-title').html(title);
		
	},
	blockBalance:function(){
		var h_act=$('#action_wallet_block div.content-body').height();
		var h_info=$('#info_wallet_block div.content-body').height();
		var h_q=$('#info_wallet_block div.content-body img#img_q').height();		
		if(h_act>h_info)
			$('#info_wallet_block div.content-body').css('height',h_act+36+'px');
		if(h_info>h_act)
			$('#action_wallet_block div.content-body').css('height',h_info+36+'px');
	},
	
	
};
var WalletsBuy={
	active:false,
	minBuy:0,
	init:function(){
		//this.minBuy=minBuy;
		this.active=false;	
		$('ul#method_buy_list a').each(function(e){
			var t=$(this).data('type');	
			$(this).click(function(){
				WalletsBuy.showType(t);
			});	
		});
		$('form#buy_fox span.error').html('');
		this.clearTotalAmount();
		$('form#buy_fox button#buy').click(function(){
			WalletsBuy.buy();
		});
		$('form#buy_fox input[name="value"]').blur(function(){
			WalletsBuy.rate(WalletsBuy.active,true);
		});
		$('form#buy_fox input[name="value"]').keyup(function(){
			WalletsBuy.totalIn();
		});
	},
	showType:function(type){		
		$('form#buy_fox span#type').html('');
		if(this.active==type)
			return false;
		var name=$('ul#method_buy_list a[data-type="'+type+'"]').html();		
		this.active=type;
		$('form#buy_fox input[name="t_type"]').val(type);
		this.diselect();		
		$('[data-row="buy_selected"]').html(name);		
		this.rate(type);
	},
	rate:function(type,curse)
	{
		var ufox=parseFloat($('form#buy_fox input[name="value"]').val());		
		if(!type){ this.clearTotalAmount();return false;}
		if(!curse)
		{
			$.post(Language_prefix+'/personal/event?type=rate',{type:type,ufox:ufox},function(data){
				if(data.result)
				{	WalletsBuy.clearTotalAmount();
					$('form#buy_fox input[name="rate"]').val(data.rate);			
					$('#rate_show').html(data.string);
					if(data.price)	
						WalletsBuy.setTotalAmount(data.price);	
						
					if(data.currency_info)
					{
						if(data.currency_info.amount_label)
							$('[data-row="amount_label"]').html(data.currency_info.amount_label);
						if(data.currency_info.input_placeholder)
							$('[data-row="input_placeholder"]').attr('placeholder',data.currency_info.input_placeholder);					
						
						if(data.currency_info.icon)
						{
							$('[data-row="buy_icon"]').html(data.currency_info.icon);
							$('[data-row="buy_icon_input"]').html(data.currency_info.icon);
						}
						if(data.currency_info.min_buy)
						{
							$('[data-row="buy_minvalue"]').html(data.currency_info.min_buy+' '+data.currency_info.currency);
							WalletsBuy.setMinBuy(data.currency_info.min_buy);
						}
						
					}						
				}
							
				
			},"json");
		}
		else
		{
			$('form#buy_fox span#value').html('');
			
			if(ufox!='')
			{
				$.post(Language_prefix+'/personal/event?type=total_currency',{rate:parseFloat($('form#buy_fox input[name="rate"]').val()),ufox:ufox,type:$('form#buy_fox input[name="t_type"]').val()},function(data){
					if(data.result)
					{
						WalletsBuy.clearTotalAmount();
						if(data.price)				
							WalletsBuy.setTotalAmount(data.price);		
					}
					else{
						WalletsBuy.clearTotalAmount();
						$('form#buy_fox span#value').html(data.error);
					}					
					
				},"json");
			}
			else
			{$('form#buy_fox span#value').html(PersonalFox.getError('Please input correct amount'));WalletsBuy.clearTotalAmount();}
				
		}	
	},
	buy:function(){
		return false;
		$('form#buy_fox span.error').html('');
		var data=$('form#buy_fox').serialize();
		data.value=parseFloat(data.value);
		data.rate=parseFloat(data.rate);
		$.post(Language_prefix+'/personal/event?type=buy_fox',data,function(j){
			if(j.result)
			{
				WalletFOX.showModal(j.title,j.html);
				if(!j.verification_error)
					$('form#buy_fox input[name="value"]').val('');	
			}
			else
			{
				if(j.error)
				{
					var k=Object.keys(j.error);
					for(var item in k)
					{
						
						$('form#buy_fox span#'+k[item]).html(j.error[k[item]]);
						//$('form#buy_fox span#'+k[item]).show();
					}				
				}				
			}
		},"json");
	},
	totalIn:function(){
		$('form#buy_fox span#value').html('');
		var ufox=parseFloat($('form#buy_fox input[name="value"]').val());
		var rate=parseFloat($('form#buy_fox input[name="rate"]').val());
		var type=$('form#buy_fox input[name="t_type"]').val();
		if(ufox>=this.minBuy)
		{
			if(type!='')
			{
				var t=Math.round(rate*ufox);
				WalletsBuy.setTotalAmount(t);	
			}
			else
				WalletsBuy.clearTotalAmount();	
		}
		else{ $('form#buy_fox span#value').html(PersonalFox.getError('Please input correct amount'));WalletsBuy.clearTotalAmount();}
	},
	diselect:function(){
		$('[data-row="buy_selected"]').html('');
	},
	setMinBuy:function(val)
	{
		this.minBuy=parseFloat(val);
	},
	clearTotalAmount:function(){
		$('[data-row="total_amount_value"]').html('');
	},
	setTotalAmount:function(val,currency){
		if(!currency)
			var currency='FOX';
			
		$('[data-row="total_amount_value"]').html(val+' '+currency);
	},
	
};
var WalletsSend={
	
	init:function(){		
		console.log('s');
		$('form#send_fox div#adres_ins').hide();
		
		$('form#send_fox span.error').html('');
		
		$('form#send_fox button#send').click(function(){
			WalletsSend.send();
		});
		
		$('form#send_fox a#addres').click(function(){
			
			WalletsSend.showCode();
		});
		$('form#send_fox input[name="adress"]').keyup(function(){			
			WalletsSend.parseurl();
		});
		$('form#send_fox input[name="adress"]').blur(function(){			
			WalletsSend.parseurl();
		});
		
		$('form#send_fox input[name="value"]').keyup(function(){
			WalletsSend.totalIn();
		});
		$('form#send_fox input[name="wallet2"]').keyup(function(){
			WalletsSend.wallet();
		});
		
	},
	showCode:function(){
		if($('form#send_fox div#adres_ins').css('display')=='none')
		{
			$('form#send_fox div#adres_ins').show();
			$('form#send_fox input[name="wallet2"]').attr('disabled','disabled');
			$('form#send_fox input[name="value"]').attr('disabled','disabled');
		}
		else
		{
			$('form#send_fox div#adres_ins').hide();
			$('form#send_fox input[name="wallet2"]').removeAttr('disabled','disabled');
			$('form#send_fox input[name="value"]').removeAttr('disabled','disabled');
		}			
		
	},
	parseurl:function(){
		$('form#send_fox span#adress').html('');	
		var d=$('form#send_fox input[name="adress"]').val();
		$.post(Language_prefix+'/personal/event?type=send_pars',{url:d},function(j){
			if(j.result)
			{				
				$('form#send_fox input[name="wallet2"]').val(j.wallet2);
				$('form#send_fox input[name="value"]').val(j.value);
			}
			else
			{
				$('form#send_fox span#adress').html('Please input qr-code data');
				$('form#send_fox input[name="wallet2"]').val('');
				$('form#send_fox input[name="value"]').val('');
				
			}
		},"json");
	},
	send:function(){
		$('form#send_fox span.error').html('');
		
		var data=$('form#send_fox').serialize();
			
		
		$.post(Language_prefix+'/personal/event?type=send_fox',data,function(j){
			if(j.result)
			{
				
				/*$('#mideModalForm').hide();				
				//$('#txhash').html(j.txhash);
				//$('.result').show();	
				$('.result_confirmation').show();	
				$('.result_confirmation .loader-page').show();	
				*/
				WalletFOX.showModal(j.title,j.html);				
				if(!j.verification_error)
				{	
					WalletsSend.sendStatus(j.result_id);
					WalletFOX.modalShow=true;
					WalletsSend.reloadForm();
					$('.result_confirmation').show();
				}
			}
			else
			{
				if(j.error)
				{
					if(j.error.wallet2)
						$('form#send_fox span#wallet2').html(j.error.wallet2);
					if(j.error.value)
						$('form#send_fox span#value').html(j.error.value);
					if(j.error.wallet)
						$('form#send_fox span#wallet').html(j.error.wallet);		
				}
			}
		},"json");
	},
	sendStatus:function(id){
		$.post(Language_prefix+'/personal/event?type=send_status',{code_p:id},function(j){
			if(j.result)
			{
				if(j.status)
				{
					$('#txhash').html(j.txhash);
					$('.result_confirmation').hide();
					$('#transaction_result').hide();
					$('.result').show();
				}
				else
				{ 
					if(WalletFOX.modalShow) 
						setTimeout(WalletsSend.sendStatus.bind(WalletsSend),3000,id);	
				}
			}
			else
			{
				$('#mideModalForm').hide();				
				$('#txhash').html(j.txhash);
				$('.result_confirmation').hide();				
				$('.result').hide();
				$('#transaction_result').hide();
				$('.result_confirmation_error').show();
				if(j.error)
				{
					$('.result_confirmation_error .text').html(j.error);
				}
			}		
		},"json");
	},
	totalIn:function(){
		$('form#buy_fox span#value').html('');
		var ufox=parseFloat($('form#buy_fox input[name="value"]').val());		
		if(ufox<=0)
		{
			$('form#send_fox span#value').html(PersonalFox.getError('Please input correct amount'));			
		}
	},
	wallet:function()
	{
		var w2=$('form#send_fox input[name="wallet2"]').val();
		console.log(w2.length);
		if(w2.length==0)
		{
			$('form#send_fox span#wallet2').html(PersonalFox.getError('Please input correct wallet'));
		}	
	},
	resultModal:function(id){
		$.post(Language_prefix+'/personal/event?type=send_fox_result',{id:id},function(j){
			WalletFOX.showModal(j.title,j.html);
			$('#transaction_result').show();
			WalletsSend.sendStatus(id);
		},"json");
	},
	reloadForm:function(){
		$('form#send_fox input[name="wallet2"]').val('');
		$('form#send_fox input[name="value"]').val('');
		$('form#send_fox input[name="adress"]').val('');
		$('form#send_fox div#adres_ins').hide();
		$('form#send_fox input[name="wallet2"]').removeAttr('disabled','disabled');
		$('form#send_fox input[name="value"]').removeAttr('disabled','disabled');
	},
	
};
var WalletsResive={
	
	init:function(){	
		
		$('form#r_fox span.error').html('');
		
		$('form#r_fox input[name="value"]').keyup(function(){
			WalletsResive.totalIn();
		});
		$('form#r_fox input[name="value"]').blur(function(){
			WalletsResive.totalIn();
		});
		
		
	},
	totalIn:function(){
		$('.result').hide();
		$('form#r_fox span#value').html('');
		var ufox=parseFloat($('form#r_fox input[name="value"]').val());		
		if(ufox<=0)
		{
			$('form#r_fox span#value').html(PersonalFox.getError('Please input correct amount'));			
		}
		else
		{
			$.post(Language_prefix+'/personal/event?type=resive_code',$('form#r_fox').serialize(),function(j){
				if(j.result)
				{
					
					
					$('#qr_mode').html('<img src="'+j.qr_mode+'" style="width:100px;height:100px;"/>'); 
					$('#transaction').html(j.transaction);
					$('.result').show();	
					
				}
				else
				{
					if(j.error)
					{
						var k=Object.keys(j.error);
						for(var item in k)
						{
							$('form#r_fox span#'+k).html(j.error[k]);
							$('form#r_fox span#'+k).show();
						}				
						
						
					}
				}
			},"json");
		}
	},
	
	
};


$(function () {
	var streem=true;
	WalletsBuy.showType('btc');	
	$(WalletFOX.modal).on('hidden.bs.modal', function (event) { 
		$(WalletFOX.modal+' div.modal-body').html('<div class="loader-modal" ></div>');	
		WalletFOX.modalShow=false;
	});	
});

window.onload=function(){
	WalletFOX.blockBalance();
};


