(function ($) {
	"use strict";
	$('[data-nav-menu]').on('click', function(event){var $this = $(this),visibleHeadArea = $this.data('nav-menu');$(visibleHeadArea).toggleClass('visible');});
	Unifox.init();
	Unifox.createVisitorCookie();
	Unifox.menuMobile();
	Unifox.anchorList();
	
	var hC=0;	
	if(isExists('#top-message'))
		hC=$('#top-message').height();	
	$('header').css('top',hC+8+'px');
	
	
	$('a.closedcookies').click(function(){
		Unifox.AcceptCookieVisitor();
	});	
	
	
	var winWidth = $(window).width();
	
	dropdownMenu(winWidth);
	if(isExists('.visitor-message'))
	{
		var h=$('.visitor-message').height();
		if(!isMob())
		{
			if(winWidth>1100)
				$('.visitor-message div.rightb').css('line-height',h+'px');
		}
					 
	}
	$(window).on('resize', function(){dropdownMenu(winWidth);});

	
	
	
	//Функция показа-срытия элементов RoadMap
	$('span#hide-road').hide();
    $('.button-roadmap > button').on('click', function (ev) {
		if ($('div.additionally-box').hasClass('invisible-box')){
			$('span#hide-road').show();
			$('span#show-road').hide();
			$('div.additionally-box').removeClass('invisible-box');
		}
		else {$('div.additionally-box').addClass('invisible-box');$('span#hide-road').hide();
			$('span#show-road').show();}
    })
	// показываем (скрываем) текст в разделе "Наша команда"
	$('.advisor-more > span').on('click', function(event) {
		var $elem = $(event.currentTarget);
		var $textblock = $elem.parents('.advisor-text').find('.advisor-main');
		$textblock.toggleClass('show');
		if($textblock.hasClass('show')) {$elem.html(less);} else {$elem.html(more);}
	});

	$('a[href="#support"]').on('click', function (event) {event.preventDefault();document.querySelector('a.crisp-113f7m5').click();return false;});


	var items = (isMob()) ? 3 : 5;
	var partnerCarusel=$("#partners-top-slider");
	partnerCarusel.owlCarousel({items : items, nav:true, navText:['',''], dots: false,autoplay:true,autoplayTimeout:2500,loop:true,smartSpeed:2000});	
	//NewsCarusel.carousel();
    //Owl-curusel
    items = (isMob()) ? 1 : 3;
	var partnerCarusel2=$("#partner-slider");
    partnerCarusel2.owlCarousel({ items : items, nav:true, navText:['','']});


	$('.mob-hui').each(
		function()
		{
			$(this).jScrollPane();
			var api = $(this).data('jsp');
			var throttleTimeout;

			$('.advisor-more span').bind(
				'click',
				function(event)
				{
					if (!throttleTimeout) {
						throttleTimeout = setTimeout(
							function()
							{
								var $elem = $(event.currentTarget);
								var $jps = $elem.parents('.jspContainer');
								var $container = $elem.parents('.advisor-container');
								var height = $container.height() + 50;
								console.log(height);
								$jps.css('height', height+'px');
								api.reinitialise();
								throttleTimeout = null;
							},
							400
						);
					}
				}
			);
		}
	);


})(jQuery);

function dropdownMenu(winWidth) {
	var hC=0;	
	if(isExists('#top-message'))
		hC=$('#top-message').height();	
	$('header').css('top',hC+8+'px');
	var winWidthnew = $(window).width();
	var winHeight = $(window).height();
	if(winWidthnew<=1100 && winWidthnew > winHeight)
	{
		//console.log(winWidth+' h');
		$('.visitor-message').addClass('mini');
	}
	else
	{
		$('.visitor-message').removeClass('mini');
	}
	
	Unifox.menuMobileViewer();
	if(winWidth > 767){		
		$('.main-menu li.drop-down').on('mouseover', function(){
			var $this = $(this),menuAnchor = $this.children('a');menuAnchor.addClass('mouseover');
		}).on('mouseleave', function(){
			var $this = $(this),menuAnchor = $this.children('a');				
			menuAnchor.removeClass('mouseover');
		});
	}else{		
		$('.main-menu li.drop-down > a').on('click', function(){
			if($(this).attr('href') == '#') return false;
			if($(this).hasClass('mouseover')){ $(this).removeClass('mouseover'); }
			else{ $(this).addClass('mouseover'); }
			return false;
		});
	}
			
}

function isExists(elem){
	if ($(elem).length > 0) { 
		return true;
	}
	return false;
}



function isMob() {

	if($('body').width() == 375) {
		return true;
	}
	else return false;

}
function isWidthB() {
	return $('body').width();
}
$(function () {	
	var NewsCarusel=$("#news-top-slider");
	NewsCarusel.owlCarousel({items : 1, nav:false, dots: false,autoplay:true,autoplayTimeout:8000,loop:true,smartSpeed:3500});
	//var owl=
	$("a#news-top-slider-next").click(function() {
		NewsCarusel.trigger('next.owl.carousel');
	});
});



