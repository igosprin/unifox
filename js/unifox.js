var Unifox={		
	AccordionCount:0,	
	AccordionItem:'div.faq-element',
	CookieName:'Unifox_video',
	CookieVisitor: 'VisitorAccept',
	//CookieName:'ghhg',	
	CookieOption:{expires: 3500, path: '/'},	
	init:function(elem){
		var ac={};
		if(elem && elem.AccordionItem)		
			ac.AccordionItem=elem.AccordionItem;
		this.AccordionInit(ac);
		this.timerShow();
		//this.VideoInit();
	},
	AccordionInit:function(elem){
		if(elem.AccordionItem)
			this.AccordionItem	=elem.AccordionItem;
		if($(this.AccordionItem).length)
		{			
			var accs=$(this.AccordionItem);
			
			accs.each(function(){
				Unifox.AccordionCount++;
				var element=this;
				$(element).click(function(){
					Unifox.AccordionSelected(element);
				});
			});
			
			
		}	
	},
	AccordionSelected:function(obj){
		var sel=true;
		if($(obj).hasClass('active'))
			sel=false;
		this.AccordionDiselected();
		if(sel)
			$(obj).addClass('active');	
		
	},
	AccordionDiselected:function(){
		if(this.AccordionCount>0)
			$(this.AccordionItem).removeClass('active');	
	},
	createVideoCookie:function(){
		if(!$.cookie(this.CookieName))
		{
			$.cookie(this.CookieName,true,this.CookieOption);
			$('#videoModal').modal();
			$('#videoModal').on('hidden.bs.modal', function (event) { 
				$('#videoModal div.modal-body').html('');	
			});	
		}
	},
	Timer:function(){
		var today=new Date();
		var timeend= new Date('2018', '9', '28');
		var xsec = Math.floor((timeend-today)/1000);
		if(xsec>0)
		{   
			var tsec=xsec%60; xsec=Math.floor(xsec/60); if(tsec<10)tsec='0'+tsec;
			var tmin=xsec%60; xsec=Math.floor(xsec/60); if(tmin<10)tmin='0'+tmin;
			var	thour=xsec%24; xsec=Math.floor(xsec/24);
			var r={tday:xsec,tsec:tsec,tmin:tmin,thour:thour};
		}
		else false;
		
		return r;
	},
	timerShow:function(){
		var timex=Unifox.Timer();
		//console.log(timex);
		if(timex){
			var word=Unifox.wordTimer(timex);
			$('.timertdays').html(timex.tday);
			$('.wordtimerday').html(word.days);
			
			$('.timerthour').html(timex.thour);
			$('.wordtimerhors').html(word.hrs);
			
			$('.timertmin').html(timex.tmin);
			$('.wordtimermin').html(word.min);
			
			$('.timertsec').html(timex.tsec);
			$('.wordtimersec').html(word.sec);
			
			setTimeout(Unifox.timerShow.bind(Unifox),1000);			
		}			
	},
	wordTimer:function(timex)
	{
		var d={days:'days',hrs:'hours',min:'minutes',sec:'seconds'};
		//console.log(Language_prefix);
		switch(Language_prefix)
		{
			case '/ru':
				//days
				var dn=['день','дня','дней'];		
				d.days=dn[0];
				count = timex.tday % 100;
				var number=Unifox.wordTimerRu(count);
				d.days=dn[number];

				//hrs
				var dn=['час','часа','часов'];
				d.hrs=dn[0];
				count = timex.thour % 100;
				var number=Unifox.wordTimerRu(count);
				d.hrs=dn[number];	
				
				//min
				var dn=['минута','минуты','минут'];
				d.min=dn[0];
				count = timex.tmin % 100;
				var number=Unifox.wordTimerRu(count);
				d.min=dn[number];
				
				//sec
				var dn=['секунда','секунды','секунд'];
				d.sec=dn[0];
				count = timex.tsec % 100;
				var number=Unifox.wordTimerRu(count);
				d.sec=dn[number];
					
			break;
			case '/cz':
				//days
				d.days='dní';
				if(timex.tday==1)
					d.days='den';
				else if(timex.tday>1 && timex.tday<5)
				{
					d.days='dny';
				}
				
				//hrs
				d.hrs='hodin';
				if(timex.thour==1)
					d.hrs='hodina';
				else if(timex.thour>1 && timex.thour<5)
				{
					d.hrs='hodiny';
				}
				
				//min
				d.min='minut';
				if(timex.tmin==1)
					d.min='minutu';
				else if(timex.tmin>1 && timex.tmin<5)
				{
					d.min='minuty';
				}
				//sec
				d.sec='sekund';
				if(timex.tsec==1)
					d.sec='sekunda';
				else if(timex.tsec>1 && timex.tsec<5)
				{
					d.sec='sekundy';
				}				
			break;
			default:
				//days					
				if(timex.tday==1)
					d.days='day';
				//hrs					
				if(timex.thour==1)
					d.hrs='hour';
				//sec
				if(timex.tsec==1)
					d.sec='second';
				//min
				if(timex.tmin==1)
					d.min='minute';		
				
				
			break;			
		}
		return d;
	},
	wordTimerRu:function(count)
	{
		var n=0;
		if (count >= 5 && count <= 20) {
			n = 2;
		} else {
			count = count % 10;
			if (count == 1) {
				n = 0;
			} else if (count >= 2 && count <= 4) {
				n = 1;
			} else {
				n = 2;
			}
		}
		return n;
	},
    createVisitorCookie:function(){
        if (!$.cookie(this.CookieVisitor))
			$('.visitor-message').show();
		else $('.visitor-message').hide();	
    },
	AcceptCookieVisitor:function(){
		 $.cookie(this.CookieVisitor, true, this.CookieOption);
		 $('.visitor-message').hide();
	},
	menuMobile:function(){
		var old=$('.conteiner-menu').html();
		$('.mobile-menu .content-mobile-menu').html(old);		
		this.menuMobileViewer();			
	},
	menuMobileViewer:function(){
		var with_=$(window).width();
		if(with_<=1139){
			$('.mobile-menu').width(with_);
			$('.mobile-menu').css('min-height',$(window).height()+'px');
		}
		else $('.mobile-menu').removeClass('visible');
	},
	anchorList:function(){
		$('a[data-anchor="link"]').click( function (event) {
			$('#main-menu').removeClass('visible');	
			var id  = $(this).attr('anchor-id');			
			var top = $('#'+id).offset().top;			
			$('body,html').animate({scrollTop: top-50}, 800);			
		});
	},	
}