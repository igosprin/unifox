<?php

$_constant_languages = array();
//eng	
$_constant_languages['menu'] = array(    
    'home'=>'Домой',
	'our projects'=>'НАШИ ПРОЕКТЫ',	
	'about us'=>'КОМАНДА',
	'documents'=>'ДОКУМЕНТАЦИЯ',
    'backoffice' => 'КАБИНЕТ',
    'news' => 'НОВОСТИ',
);
$_constant_languages['word'] = array(
	'Invest in <span>ICO</span> with working ecosystem'=>'Идёт внутренняя <span>предпродажа</span>!',
	'<span>Documentation</span> in one place'=>'Вся <span>документация</span> в одном месте',
	'documents'=>'ДОКУМЕНТАЦИЯ',
	'doc_subtext1' => 'Краткое описание проекта',
	'doc_subtext2' => 'Полное описание проекта',
	'Decentralized exchange' => 'Децентрализованная биржа',
	'unfulfilled promise' => 'Технический whitepaper– <span>будет опубликован в августе 2018 года</span>',
    'Exchange Office' => 'Онлайн обменник',
    'Technical Whitepaper' => 'Технический Whitepaper',
    'Kiosk' => 'Киоск',
    'pos' => 'POS-система',
    'unidesc' => 'Описание проекта Unicash',
	'Buy online exchange' => 'Купить онлайн обменник',
	'Partnership offer' => 'Партнерское предложение',
	'Koupit FOX tokeny'=>'КУПИТЬ FOX ТОКЕНЫ',
	'UniFox is an autonomous ecosystem that implements blockchain technologies into the world economy and makes cryptocurrency accessible for ordinary people from all over the world'=>'<strong>UniFox</strong> – это автономная экосистема, которая внедряет технологии блокчейн в мировую экономику и делает криптовалюты доступными для обычных людей со всего мира.',
	'ICO ends in'=>'ICO начнется через',
	'contribute'=>'ВЛОЖИТЬ',
	'contribute2'=>'БАНК. ПЕРЕВОД',
	'about_us-left-top-title'=>'Почему UniFox',
	'about_us-left-top'=>'<br/>Мы считаем, что технология блокчейн вместе с компьютерами и Интернетом является одним из величайших изобретений человечества. Даже самые передовые технологии не будут использоваться массово, пока они не станут доступными для всех простых людей.<br/><br/>
<strong>UniFox</strong> решает это с помощью комбинации нескольких продуктов, которые обеспечивают отличную ликвидность между криптовалютами и местными FIAT валютами. Это делает нашу технологию блокчейн полностью пригодной для использования в экономике.<br/><br/>
Более того, эти продукты уже работают.',
	'about_us-right-top'=>'<br/>Команда <strong>UniFox</strong> и ряд частных инвесторов уже вложили миллионы долларов в покупку монет FOX. Есть также тысячи небольших инвесторов, которые приняли наше предложение.<br/><br/>
Как только будут внедрены продукты, финансируемые из источников этих инвесторов, упомянутые технологии начнут использоваться мгновенно. Это приведет к росту рыночной цены монет <strong>FOX</strong>. Её цена станет намного выше текущей цены монеты.<br/><br/>
Общее количество монет ограничено.',
						
	'about_us-right-top-title'=>'Почему стоит купить FOX прямо сейчас',
	'about_us-left-bottom-title'=>'Станьте обладателем виртуального богатства',
	'about_us-left-bottom'=>'<br><strong>FOX</strong> – это полноценная криптовалюта, которая представляет собой виртуальное богатство. Причиной этого является то, что <strong>FOX</strong> включает в себя самые большие преимущества',
	'about_us-left-bottom-li-1'=>'Собственная блокчейн технология и собственный кошелёк',
	'about_us-left-bottom-li-2'=>'Инвестиционный токен (секьюрити токен)– участвует во всей экосистеме',
	'about_us-left-bottom-li-3'=>'Внутренний токен протокола (утилити токен) – <strong>FOX</strong> токен используется для оплаты блокчейн комиссий, что обеспечивает рост токена',
	'play video'=>'УЗНАТЬ БОЛЬШЕ',
	'our projects title'=>'Наши проекты',
	'discover title 1'=>'Растущая инфраструктура',
	'discover text 1'=>'Сеть крипто банкоматов, обменников и <br>POS терминалов',
	'discover title 2'=>'FIAT и крипто <br>– одно целое',
	'discover text 2'=>'Мы cоздали стабильную валюту Unicash',
	'discover title 3'=>'Децентрализованная биржа',
	'discover text 3'=>'Доступна по всему <br>миру с поддержкой любой валюты',
	'zobrazit vice'=>'УЗНАТЬ БОЛЬШЕ',
	'CELA ROADMAPA'=>'ПОКАЗАТЬ БОЛЬШЕ',
	'zkratit roadmap'=>'СКРЫТЬ',
	'Active founders'=>'Команда',
	'Advisory board'=>'Advisory board',
	'Partneri'=>'Партнёры',
	'PREJIT NA WEB'=>'Перейти',
	'partner-1'=>'Стабильная криптовалюта и важная часть экосистемы UniFox',
	'partner-2'=>'Онлайн обменник, специализирующийся на обмене валюты частных лиц',
	'partner-3'=>'Exchange office with <br/>a unique transaction system.',
    'partner-4'=>'Компания, которая предоставляет все ИТ-услуги в рамках экосистемы UniFox',
	'Psali o nas'=>'Пресса о нас',
	'PREJIT CLANEK'=>'ЧИТАТЬ СТАТЬЮ',
	'ZOBRAZIT DALSI'=>'СЛЕДУЙЩИЙ',
	'faq-title'=>'FAQ',
    'Roadmap' => 'Дорожная карта',
    'days' => 'дня',
    'hrs' => 'часов',
    'mins' => 'минуты',
    'More' => 'Ещё',
    'Less' => 'Свернуть',
    'Close' => 'ЗАКРЫТЬ',
    'subheader1' => 'Зачем был создан UniFox?',

    'bosses' => 'Команда основателей',
    'advisors' => 'Наши эдвайзеры',
    'managing team' => 'Менеджмент',
    'Technical support' => 'Техническая поддержка',
    'Marketing team' => 'Маркетинговая команда',
    'raised' => 'Cобрано 156 000 $',
    'Copyright' => 'Copyright 2018 - Условия использования',
    'Privacy Policy' => 'Политика Конфиденциальности',
    'conf-bold' => 'Предстоящая конференция:',
    'conf-text' => 'Blockchain the World | 18/8/2017 in Taipei',
    'conf-button' => 'узнай больше',
	'Meet Unifox Team Personaly'=>'Познакомьтесь с командой UniFox лично',
    'cookiesOn' => 'Этот сайт использует файлы cookies для более комфортной работы пользователя. Продолжая просмотр страниц сайта, вы соглашаетесь с использованием  файлов cookies. Дополнительная информация о файлах cookies <a target="_blank" href="{LINK}">здесь</a>.',
    'cookiesOff' => 'Этот сайт использует файлы cookies для более комфортной работы пользователя. Продолжая просмотр страниц сайта, вы соглашаетесь с использованием  файлов cookies. Дополнительная информация о файлах cookies <a target="_blank" href="{LINK}">здесь</a>.',
);

$_constant_languages['error'] = array(
	'Registration-error-email'=>'Please enter the correct email',
	'Registration-error-password1'=>'Please enter the password',
	'Registration-error-password2'=>'Please сonfirm password',        
	'Registration-error-email2'=>'This email is exist',        
	'invalid e-mail or password'=>'Invalid e-mail or password',        
	'Registration-error-name'=>'Please enter your name',        
	'This email not found'=>'This email not found',        
	'Registration-error-passord'=>'Password consists of 6 or more characters',        
	'This user is not activated'=>'Этот пользователь не активирован. Если вы не получали код активации, нажмите <a href="javascript:void(0);" onclick="RegistryUnifox.activateLinkSend()" class="code_activate" >тут</a>',        
);

$_constant_languages['personal'] = array(

);
$_constant_languages['news-link'] =array(
	0=>array('img'=>'prometheus_min.png','link'=>'https://prometheus.ru/kak-unifox-menyaet-otnoshenie-obshhestva-k-kriptovalyutam/','name'=>'Prometheus.ru','text'=>'В отличие от Tether или любой другой «стабильной» криптовалюты, Unicash можно приобретать за местную валюту через специальные банкоматы — причем мгновенно и без лишних комиссий.'),
	1=>array('img'=>'btc_logo_min.png','link'=>'https://btcmanager.com/unifox-is-creating-a-platform-to-take-cryptocurrencies-mainstream/','name'=>'BTCmanager.com','text'=>'All revenues generated from the operations will be distributed back to the community. As more and more transactions take place on the Unifox blockchain using the Fox coin, the higher will the value of each coin rise.'),
	2=>array('img'=>'newsbtc_min.png','link'=>'https://www.newsbtc.com/press-releases/electra-eca-partners-with-unifox-to-provide-cryptocurrency-atm-pos-machines-worldwide/','name'=>'NewsBTC.com','text'=>'UniFox seeks to incorporate cryptocurrencies into everyday life through the introduction of their autonomous designs.')
	
	
);


?>