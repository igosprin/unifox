<?php
class Thumbnail
{
	protected $typeSave='png';
	protected $square=false;
	function __construct($typeSave='png')
	{
		$this->typeSave=$typeSave;
	}
	
	function set_square($square=false)
	{
		$this->square=$square;
	}
	function createImg($pathOriginal,$PathSave,$h=0)
	{
		$type_original='png';
		$sz = getimagesize($pathOriginal);
		{
			if (!$sz)
				return false;
			switch($sz[2])
			{
				case 1:
                    $ish=imagecreatefromgif($pathOriginal);
					$type_original='gif';
                break;
                case 2:
                    $ish=imagecreatefromjpeg($pathOriginal);
					$type_original='jpeg';
                break;
                case 3:
                   $ish=imagecreatefrompng($pathOriginal);
                break;
			}
			if(!$ish)
				return false;
			
			$center_left=0;
			$center_top=0;
			
			if(!$this->square)
				$resize=$this->calculationImgSize($h,$sz[1],$sz[0]);				
			else 
			{
				if($h>$sz[1]){
					$iw = $ih = $h;
					/*$center_left=floor(($h - $sz[1])   / 2);
					$center_top=floor(($h - $sz[0])   / 2);*/	
				}
				else
				{
					$resize=$this->calculationImgSizeSquare($h,$sz[1],$sz[0]);
					$iw =$resize['w'];
					$ih = $resize['h'];
				}	
						
			}
			$img_result=@imagecreatetruecolor($iw, $ih);
			
			if($type_original==$this->typeSave)
			{
				imageAlphaBlending($img_result, false);	
				imageSaveAlpha($img_result, true);
			}
			
						 
			imagecopyresampled($img_result, $ish, $center_left, $center_top, 0, 0, $iw, $ih, $sz[0], $sz[1]); 
			if($this->typeSave=='png')
				imagepng($img_result, $PathSave);
			if($this->typeSave=='jpeg')
				imagejpeg($img_result, $PathSave,85);
			if($this->typeSave=='gif')
				imagegif($img_result, $PathSave);
			imagedestroy($img_result);
			imagedestroy($ish);	
			return basename($PathSave);
		}
	}
	
	public function calculationImgSize($h=0,$ih_input=0,$iw_input=0)
	{
		
		$iw = $iw_input;
		$ih = $ih_input;
		if($h!=0)
		{					
			if($iw>$ih)
			{
				$ih=$h*($ih/$iw);
				$iw = $h;
				if($ih>$h)
				{
					$iw=$h*($iw/$ih);
					$ih = $h;
				}
			}
			else
			{
				if($ih > $h)
				{
					$iw=$h*($iw/$ih);
					$ih = $h;
					if($iw>$h)
					{
						$ih=$h*($ih/$iw);
						$iw = $h;
					}
				}
			}
			
				
			$iw = (int) $iw;
			$ih = (int) $ih;
		} 

		return array('w'=>$iw,'h'=>$ih);	
	}
	public function calculationImgSizeSquare($w=0,$ih_input=0,$iw_input=0)
	{
		
		$iw = $iw_input;
		$ih = $ih_input;
		
		$koef=$iw/$ih;
		
		if ($iw > $w && $w!=0 )
		{
			if($iw>$ih)
			{
				$iw=$w;
				$ih=$iw/$koef;										
			}				
			else
			{
				$ih=$w;	
				$iw = $ih*$koef;							
			}
		}
		elseif($ih>$w)
		{
			$ih=$w;
			$iw = $ih*$koef;			
		}
		$iw = (int) $iw;
		$ih = (int) $ih;

		return array('w'=>$iw,'h'=>$ih);
	}

	
	
}
