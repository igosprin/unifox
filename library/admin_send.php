<?php
class SendAdmin
{
	protected $path_request=null;
	protected $dir_request_verif='/request_verif/';
	protected $dir_request_airdrop='/request_airdrop/';
	function __construct($path)
	{
		$this->path_request=$path;
	}
    function setRequest($id_user,$type='verif')
	{
		$name_file='request_'.$id_user;		
		$p=$this->getRequestPath($type);
		if(!file_exists($p.$name_file))
		{			
			file_put_contents($p.$name_file,json_encode(array('time'=>time())));
			@chmod($p.$name_file,0777);
		}
		
	}
	function getRequestPath($type='verif')
	{
		$path_save=$this->path_request;
		switch($type)
		{
			case 'airdrop':
				$path_save.=$this->dir_request_airdrop;
			break;
			default:
				$path_save.=$this->dir_request_verif;
			break;
		}
		return $path_save;
	}
	function getRequest($type='verif')
	{
		$dir=$this->getRequestPath($type);
		$array=array();
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				
				while (($file = readdir($dh)) !== false) {
					if ($file != "." && $file != "..") {
						$array[]=$file;
					}
					
				}
				closedir($dh);
			}
		}
		return $array;
	}
	function removeRequest($name_file,$type='verif')
	{
		$dir=$this->getRequestPath($type);
		@unlink($dir.$name_file);
		
	}
}

