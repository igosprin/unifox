<?php 
class ApiWalet
{
	protected $login='web_unifox';	
	protected $pwd='fHLyqwu1djPA';
	protected $url='http://apibtc.arbitas.com/api/json/unicash/';
	protected $keys='415b3ad43352a879e0525bb21a82ef99';
	protected $token_temp='';
	protected $qr_url='https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=';	
	protected $qr_path_save='';		
	protected $token_saves=true;		
	protected $currency='';		
	function __construct($qr_path_save='/',$currency='FOX')
	{
		$this->qr_path_save=$qr_path_save;		
		$this->currency=$currency;		
	}
	protected function query($url,$data,$base_pwd,$dev=false,$limitation=true)
	{
		$data['key']=$this->keys;		
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		if($limitation)
			curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		if($base_pwd)
		{	
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
			curl_setopt($ch, CURLOPT_USERPWD, "$this->login:$this->pwd");
		}
		
		
		if($dev)
			var_dump($data);
		$response = curl_exec($ch);
		$this->set_log($url,$data,$response);	
		return json_decode($response,true); 
	}
	protected function query_get($url,$base_pwd,$dev=false)
	{
			
		$ch = curl_init($url);		
		if($base_pwd)
		{	
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
			curl_setopt($ch, CURLOPT_USERPWD, "$this->login:$this->pwd");
		}
		if($dev)
			var_dump($data);
		$response = curl_exec($ch);		
		return json_decode($response,true); 
	}
	protected function auth_user($data)
	{
		$url=$this->url.'Auth/';
		$data['client_ip']=USER_IP_ADDRESS;	
		$data['browser']=BROWSER_NAME;	
		$data['browser_info']=$_SERVER['HTTP_USER_AGENT'];	
		$answ=$this->query($url,$data,true);
		return $answ;
	}
	protected function reg_user($data)
	{
		$url=$this->url.'Register/';
		$data['repassword']=$data['password'];
		$data['client_ip']=USER_IP_ADDRESS;	
		$data['browser']=BROWSER_NAME;	
		$data['browser_info']=$_SERVER['HTTP_USER_AGENT'];
		$answ=$this->query($url,$data,true);
		return 	$answ;		
	}
	public function get_user($data,$tokenReturn=false)
	{
		$error=false;
		if($data['login']=="")
			$error=true;
		if($data['password']=="")
			$error=true;
		if(!$error)
		{
			$isset=$this->auth_user($data);		
			if($isset["type"]=="error")
			{
				$answer=$this->reg_user($data);
				$this->token_temp=$answer['answer']['user_token'];
				if($tokenReturn) return $answer['answer']['user_token'];
				else return $answer;
			}
			else
			{
				$this->token_temp=$isset['answer']['user_token'];
				if($tokenReturn) return $isset['answer']['user_token'];
				else return $isset;
			}
		}
		else return array('Unifox_type'=>'No Aauthorization data');
			
	}
	
	public function get_user_balnace($data)
	{
		
		$return=array('balance'=>0);
		if($data['token']=='')
		{
			$n=$this->auth_user($data);
			$data['token']=$n['answer']['user_token'];
		}
		$get=array('token'=>$data['token']);
		$walet=$this->get_user_wallets($get);
		if($walet['type']=='success')
		{
			if(isset($walet['answer']['wallets']))
			{
				$return['wallets']=$walet['answer']['wallets'];
				foreach($walet['answer']['wallets'] as $k=>$w)
				{
					//var_dump($w);die();
					$return['balance']=$return['balance']+$w['balance'];
					$return['wallets'][$k]['qr_code']=$this->generate_qr($w['wallet']);
				}
			}	
		}
		$return['type']=$walet['type'];
		return $return;
		
	}
	
	public function get_user_wallets($data)
	{
		if(!isset($data['token']))
			return false;
		$url=$this->url.'GetWallets/';	
			
		$answ=$this->query($url,$data,true);
		//var_dump($answ);die();
		return 	$answ;
	}
	public function get_user_wallet($data)
	{
		
		//http://apibtc.arbitas.com/api/json/unicash/GetWalletBalance/?key=415b3ad43352a879e0525bb21a82ef99&token=14b1d7aa1bce6474aa7abe902a187e63&wallet=0x590a11a68acc12f4a9d091ac2098ae1581beae2f
		if(!isset($data['token']))
			return false;
		$url=$this->url.'GetWalletBalance/';		
		$answ=$this->query($url,$data,true);
		return 	$answ;
	}
	public function set_user_wallet($data)
	{
		if(!isset($data['token']))
			return false;
		$url=$this->url.'NewWallet/';		
		$answ=$this->query($url,$data,true);
		$answ['qr_code']=$this->generate_qr($answ['answer']);
		return 	$answ;
	}
	public function get_user_token_session()
	{
		return $this->token_temp;
	}
	
	
	public function pars_data_user($data)
	{
		$return=array();
		$return['login']=$data['email'];
		if($data['email']=='')
		{
			$return['login']=$data['fb_id'];
		}
		$return['password']=$data['password_api'];
		return $return;	
	}
	public function get_rate($data)
	{
		//http://apibtc.arbitas.com/api/json/unicash/GetRate/?key=415b3ad43352a879e0525bb21a82ef99&token=86059bab267a862251d48bd94b334c09&currency_from=uxc&currency_to=btc
		if(!isset($data['token']))
			return false;
		$url=$this->url.'GetRate/';	
		$data['currency_from']=strtolower($this->currency);	
		$data['currency_to']=$data['type'];	
		unset($data['type']);
		$answ=$this->query($url,$data,true);
		//var_dump($answ);
		return $answ;
	}
	public function buy_currency($data)
	{
		if(!isset($data['token']))
			return false;
		
		//http://apibtc.arbitas.com/api/json/unicash/BuyUxcByBtc/?key=415b3ad43352a879e0525bb21a82ef99&token=86059bab267a862251d48bd94b334c09&currency=UXC&amount=10&wallet=0xasdawdwqwdswdawd&rate=8000&amount_btc=0.00001
		$url=$this->url.'BuyUxcByBtc/';	
		$data['currency']=$this->currency;
		//$data['currency']='UXC';
		
		$answ=$this->query($url,$data,true);
		
		return $answ;
	}
	public function send_currency($data)
	{
		if(!isset($data['token']))
			return false;
		
		//http://apibtc.arbitas.com/api/json/unicash/Send/?key=415b3ad43352a879e0525bb21a82ef99&token=98384ef5f7ae89be51d6577927992837&wallet[from]=0x590a11a68acc12f4a9d091ac2098ae1581beae2f&wallet[to]=0xab6d6f69f14421e53b07c6a8c3da73f511d88972&amount=1&token=98384ef5f7ae89be51d6577927992837 
		$url=$this->url.'Send/';
		
		$answ=$this->query($url,$data,true);
		
		return $answ;
	}
	public function send_currency_get($data)
	{
		if(!isset($data['token']))
			return false;
		$url='?key='.$this->keys.'&wallet[from]='.$data['wallet']['from'].'&wallet[to]='.$data['wallet']['to'].'&amount='.$data['amount'].'&token='.$data['token'];
		
		//http://apibtc.arbitas.com/api/json/unicash/Send/?key=415b3ad43352a879e0525bb21a82ef99&token=98384ef5f7ae89be51d6577927992837&wallet[from]=0x590a11a68acc12f4a9d091ac2098ae1581beae2f&wallet[to]=0xab6d6f69f14421e53b07c6a8c3da73f511d88972&amount=1&token=98384ef5f7ae89be51d6577927992837 
		$url=$this->url.'Send/'.$url;
		
		$answ=$this->query($url,$data,true);
		
		return $answ;
	}
	public function generate_qr($wallet)
	{
		$type=strtolower($this->currency);
		$sold='UNIFOX_PICY';
		$name=md5($sold.strrev($type).':'.$wallet).'.png';
		$path=$this->qr_path_save.$name;
		
		if(file_exists($path))
			return $name;
		else
		{
			$res=@file_get_contents($this->qr_url.$type.':'.$wallet);
			if($res){
				file_put_contents($path,$res);				
				return $name;
			}		
		}
	}
	public function set_gift_card($data)
	{
		//http://apibtc.arbitas.com/api/json/unicash/SendGiftCard/?key=415b3ad43352a879e0525bb21a82ef99&token=86059bab267a862251d48bd94b334c09&giftcard_code=123asd&wallet=15mK6S3b6R9mXFWXeKEMzZ5gcqobirK5yz 
		if(!isset($data['token']))
			return false;
		
		$url=$this->url.'SendGiftCard/';
		
		$answ=$this->query($url,$data,true);
		
		return $answ;
	}
	public function get_transaction($data)
	{
		//http://apibtc.arbitas.com/api/json/unicash/GetTransactions/?key=415b3ad43352a879e0525bb21a82ef99&token=14b1d7aa1bce6474aa7abe902a187e63  
		if(!isset($data['token']))
			return false;
		
		//var_dump($data);
		$url=$this->url.'GetTransactions/';		
		$answ=$this->query($url,$data,true);
		//var_dump($answ);
		$list=array();
		if($answ and isset($answ['answer']) and $answ['type']=='success')
		{
			if(isset($data['index']))
			{
				$l=1;
				foreach($answ['answer']['transactions'] as $m)
				{
					if($l<3)
					{
						$list[]=$m;
						$l++;
					}
					else break;
				}
			}
			else $list=$answ['answer'];
				
		}		
		unset($answ);	
		//var_dump($list);die();
		return $list;
	}
	public function affilate_transaction_user($data)
	{
		if(!isset($data['token']))
			return false;
		//http://apibtc.arbitas.com/api/json/unicash/GetReferTransactions/?key=415b3ad43352a879e0525bb21a82ef99&token=8a03d078e27c076ae756c6a6733188c8&accounts[]=unifox@unifox.io&accounts[]=unifox1@unifox.io
		$url=$this->url.'GetReferTransactions/?key='.$this->keys.'&token='.$data['token'].'&'.implode('&',$data['accounts']);
		
		
		$answ=$this->query($url,$data,true);			
		
		return $answ;
	}
	protected function set_log($url,$data,$answer='')
	{
		$file_name=date('Ymd');
		$path=str_replace('/qr_code/','/log/',$this->qr_path_save).$file_name;
		$text="[".$url."]".json_encode($data);
		if($answer!='')
			$text.="[answer]".$answer;
		
		$text.="\r\n";	
		
		if(!file_exists($path)){
			file_put_contents($path,$text);
			chmod($path,0777);
		}
		else
		{
			$f=fopen($path,'a');
			fwrite($f, $text);
			fclose($f);
		}
	}
	public function change_login($data)
	{
		if(!isset($data['token']))
			return false;		
		
		//http://apibtc.arbitas.com/api/json/unicash/UserLoginChange/?key=415b3ad43352a879e0525bb21a82ef99&token=8e8737472c5de5b0d8b61ba8ca72f47e&login=web1
		
		$url=$this->url.'UserLoginChange/';		
		$answ=$this->query($url,$data,true);
		return $answ;		
	}
	public function send_currency_getConfirm($data)
	{
		if(!isset($data['token']))
			return false;
		$lang='en';
		if(isset($data['lang']))
			$lang=$data['lang'];
		$url='?key='.$this->keys.'&wallet[from]='.$data['wallet']['from'].'&wallet[to]='.$data['wallet']['to'].'&amount='.$data['amount'].'&token='.$data['token'].'&lang='.$lang.'&email='.$data['email'];
		
		//http://apibtc.arbitas.com/api/json/unicash/SendFoxTransaction/?key=415b3ad43352a879e0525bb21a82ef99&token=0ab5c6dd0df70f5ebde5e76dee1d3338&wallet[from]=0xe5babd2fc6a61b135937436e910e349907503dc0&wallet[to]=0xfd00470cb56c932edf7db59b5704896b0d3fd4e1&amount=1&lang=en&email=dima@dima.dima 
		
		$url=$this->url.'SendFoxTransaction/'.$url;
		
		$answ=$this->query($url,$data,true);
		
		return $answ;
	}
	public function send_currency_confirmResult($data)
	{
		if(!isset($data['token']))
			return false;
		
		//http://apibtc.arbitas.com/api/json/unicash/ConfirmSendFoxTransaction/?key=415b3ad43352a879e0525bb21a82ef99&token=0ab5c6dd0df70f5ebde5e76dee1d3338&code=c15a4712985ee4b1827e2849d98c112a
		
		$url=$this->url.'ConfirmSendFoxTransaction/'.$url;
		
		$answ=$this->query($url,$data,true,false,false);
		
		return $answ;
	}
	public function get_request_buy_all($data)
	{
		if(!isset($data['token']))
			return false;
		/*http://apibtc.arbitas.com/api/json/unicash/GetFoxRequests?key=415b3ad43352a879e0525bb21a82ef99&token=3007d06d6112bad701242a6037da7109*/
		$url=$this->url.'GetFoxRequests/';		
		$answ=$this->query($url,$data,true);
		return $answ;		
	}
	public function set_request_buy_crypto($data)
	{
		if(!isset($data['token']))
			return false;
		
		/*		http://apibtc.arbitas.com/api/json/unicash/BuyFoxByAnotherCurrency?key=415b3ad43352a879e0525bb21a82ef99&token=3007d06d6112bad701242a6037da7109&currency=btc&amount=0.0005&wallet_fox=qweqweqweqweqweqwe&amount_fox=10&rate=100
		*/
		$url=$this->url.'BuyFoxByAnotherCurrency/';				
		$answ=$this->query($url,$data,true);		
		return $answ;	
	}
	public function get_wallet($data)
	{
		//http://apibtc.arbitas.com/api/json/unicash/GetWalletBalance/?key=415b3ad43352a879e0525bb21a82ef99&token=14b1d7aa1bce6474aa7abe902a187e63&wallet=0x590a11a68acc12f4a9d091ac2098ae1581beae2f
		$url=$this->url.'GetWalletBalance/';				
		$answ=$this->query($url,$data,true);		
		return $answ;	
	}
	public function get_transaction_wallet($data)
	{
		//http://apibtc.arbitas.com/api/json/unicash/GetWalletTransaction/?key=415b3ad43352a879e0525bb21a82ef99&token=14b1d7aa1bce6474aa7abe902a187e63&wallet= 0x53f9e229b651eab67d439ba6e10f6f6f71ca4c6fpage=0&limit=5 
		if(!isset($data['token']))
			return false;
		
		
		$url=$this->url.'GetWalletTransaction/';		
		$answ=$this->query($url,$data,true);
		/*var_dump($answ);
		die();*/
		$list=array();
		if($answ and isset($answ['answer']) and $answ['type']=='success')
		{
			 $list=$answ['answer'];				
		}		
		unset($answ);	
		//var_dump($list);die();
		return $list;
	}
	
	public function set_request_buy_bank($data)
	{
		if(!isset($data['token']))
			return false;
		
		/*		http://apibtc.arbitas.com/api/json/unicash/BuyFoxByBank?key=6691763dc16a94dbc24e29e3cd26411c&token=3007d06d6112bad701242a6037da7109&currency=eur&amount=100&wallet_fox=qweqweqweqweqweqwe&amount_fox=10&rate=100
		*/
		$url=$this->url.'BuyFoxByBank/';				
		$answ=$this->query($url,$data,true);		
		return $answ;	
	}
	public function set_public_card($data)
	{
		//http://apibtc.arbitas.com/api/json/unicash/SendPublicGiftCard/?key=6691763dc16a94dbc24e29e3cd26411c&token=86059bab267a862251d48bd94b334c09&giftcard_code=123asd&wallet=15mK6S3b6R9mXFWXeKEMzZ5gcqobirK5yz
		if(!isset($data['token']))
			return false;
		
		$url=$this->url.'SendPublicGiftCard/';
		
		$answ=$this->query($url,$data,true);
		
		return $answ;
	}
}

?>