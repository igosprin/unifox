<?
class Cache_Api
{
	protected $saveCachePath=false;		
	protected $time_life=360;
	protected $file_type='.unf';
	function __construct($saveCachePath=false,$name_dir='cache')
	{
		if($saveCachePath)
		{
			$this->issetDir($saveCachePath);
			$this->issetDir($saveCachePath.'/'.$name_dir);
			$this->saveCachePath=$saveCachePath.'/'.$name_dir.'/';
			
		}
		
	}
	
	protected function get_queryFilename($command_uri)
	{
		return str_replace(array('/',':','&',','),'_',$command_uri).$this->file_type;
	}
	public function setDataCache($url,$data,$timeLife=false)
	{
		if(!$timeLife)
			$timeLife=$this->time_life;
		$file_name=$this->get_queryFilename($url);
		if($this->saveCachePath){
			$data=json_encode($data);
			file_put_contents($this->saveCachePath.$file_name,$data);
			$time_life=array('date_created'=>time(),'date_update'=>time()+$timeLife,'file_name'=>$file_name,'url'=>$url);
			file_put_contents($this->saveCachePath.'timeLife_'.$file_name,json_encode($time_life));
			chmod($this->saveCachePath.$file_name,0777);
			chmod($this->saveCachePath.'timeLife_'.$file_name,0777);
		}
	}
	public function getDataCache($url)
	{
		if($this->saveCachePath)
		{
			
			$file_name=$this->get_queryFilename($url);
			if(file_exists($this->saveCachePath.'timeLife_'.$file_name))
			{
				$time_life=json_decode(file_get_contents($this->saveCachePath.'timeLife_'.$file_name),true);
				if(time()< $time_life['date_update'])
				{
					if(file_exists($this->saveCachePath.$file_name))
					{
						$data=file_get_contents($this->saveCachePath.$file_name);
						$data=json_decode($data,true);						
						return $data;
					}	
				}
				else
				{
					@unlink($this->saveCachePath.$file_name);
					@unlink($this->saveCachePath.'timeLife_'.$file_name);
				}	
			}
			
		}
		return false;
	}
	
	protected function issetDir($dirpath)
	{
		if(!file_exists($dirpath))
		{
			mkdir($dirpath,0777);
			@chmod($dirpath,0777);
		}
		elseif(!is_writable($dirpath))
			@chmod($dirpath,0777);
	}
	public function removeCache($url)
	{
		$file_name=$this->get_queryFilename($url);
		if($this->saveCachePath)
		{
			if(file_exists($this->saveCachePath.'timeLife_'.$file_name))
				@unlink($this->saveCachePath.'timeLife_'.$file_name);
			if(file_exists($this->saveCachePath.$file_name))
				@unlink($this->saveCachePath.$file_name);
		}
	}
}