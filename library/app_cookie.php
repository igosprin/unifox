<?php
	class App_Auth
	{
		protected $cookie=null;
		protected $lifeTimeCook=31536000;
		protected $nameCookie=USER_COOKIE_NAME;
		function __construct()
		{
			$this->cookie=$this->hasCookie();
		}
		
		public function hasIdentity()
		{
			if(isset($_SESSION[USER_SESSION_NAME]))
				return true;
			elseif($this->cookie)
			{	
				$this->setNamespace($this->cookie);
				return true;
			}				
			
			return false;	
		}
		protected function hasCookie()
		{
			if(!isset($_COOKIE[$this->nameCookie]))
				return false;		
			return base64_decode($_COOKIE[$this->nameCookie]);
		}
		public function getCookie()
		{
			if(!isset($_COOKIE[$this->nameCookie]))
				return false;		
			return base64_decode($_COOKIE[$this->nameCookie]);
		}
		public function setCookie($id)
		{
			$id=base64_encode($id);
			setCookie($this->nameCookie,$id,time()+$this->lifeTimeCook,'/');
		}
		public function setNamespace($data)
		{
			$_SESSION[USER_SESSION_NAME]=$data;
		}
		public function setIdentity($data,$cookie=false)
		{
			if($cookie)
				$this->setCookie($data);
			
			$this->setNamespace($data);			
		}
		public function getId()
		{
			return $_SESSION[USER_SESSION_NAME];
		}
		public function deleteIdentity()
		{
			unset($_SESSION[USER_SESSION_NAME]);
			setCookie($this->nameCookie,$id,time()-3600,'/');			
		}
			
	}
