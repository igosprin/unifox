<?php
function returnDateString($unix_time,$format='%m %d %Y')
{
	return strftime($format,$unix_time);
}

function returnLocal()
{	
	switch(LANG)
	{
		case 'ru':
			$local='ru_RU';
		break;
		case 'en':
			$local='en_EN';
		break;
		case 'sk':
			$local='sk_SK';
		break;
		default:
			$local='ru_RU';
		break;
	}
	return $local.'.UTF-8';
}
function localGet()
{
	setlocale(LC_ALL,returnLocal());
}

?>