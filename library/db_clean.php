<?php

class Database {

    protected $db = null;

    function __construct($settings=array()) {
		if(count($settings)==0)
			$settings=array('host'=>DB_HOST,'user'=>DB_LOGIN,'pwd'=>DB_PASS,'db_name'=>DB_NAME);
			
        $this->db = $this->connection($settings);
		
		//var_dump(get_class_methods($this->db)); 
		$charset=CHARSET_DB;
        if (isset($settings['charset']) and $settings['charset'] != '')
			$charset=$settings['charset'];
		$this->db->set_charset($charset);
		
		return $this;
      
    }

    /**
     * ���������� ��������� ������� Mysqli
     * @param $settings (��������� ��� �����������) 
     * @return Object
     */
    public function connection($settings) {
        $t = new mysqli($settings['host'], $settings['user'], $settings['pwd'], $settings['db_name']);
        if (!$t->connect_error)
            return $t;
        else {            
            return false;
        }
    }

    /**
     * ���������� ������ �� ������ ������� Mysqli
     * @param $sql  String 
     * @return Links-Object
     */
    public function Query($sql) {
      
        $t = $this->db->query($sql);
        if (!$t) {
            trigger_error('Errno: ' . $this->db->errno . ' ' . $this->db->error . "\nQuery: " . $sql, E_USER_ERROR);
            return false;
        }
        return $t;
    }

    /**
     * ���������� ��������� ������� (���������� ������)
     * @param $sql  String 
     * @return Array
     */
    public function FetchRow($sql) {
        $result = false;
        $t = $this->Query($sql);
        if ($t)
            $result = $t->fetch_assoc();
        $t->close();
        return $result;
    }

    /**
     * ���������� ��������� ������� (����������� ������)
     * @param $sql  String 
     * @return Array
     */
    public function FetchAll($sql) {
        $result = array();
        $t = $this->Query($sql);
        if ($t) {
            while ($row = $t->fetch_assoc()) {
                $result[] = $row;
            }
        }
        $t->close();
        return $result;
    }

    /**
     * ���������� ���-�� ����� 
     * @param $table  String  - ��� �������
     * @param $row  String  - ��� ����
     * @param $where Array  - �������
     * @return Integer
     */
    public function CountRow($table, $row = '', $where = array(), $whereAdditional = '') {
        $table = '`' . $table . '`';
        $Sqlwhere = 'where 1';
        if (count($where) > 0) {
            $w = $this->getKeyValueWhere($where);
            $Sqlwhere = ' where ' . implode(' and ', $w);
        }
        $sql = 'select ';
        if ($row != '')
            $sql.='count(' . $table . '.`' . $row . '`) as count_' . $row;
        $sql.=' from ' . $table;
        $sql.=$Sqlwhere . ' ' . $whereAdditional;
        $count = $this->FetchRow($sql);
        return $count['count_' . $row];
    }

    /**
     * ���������� ������������� ����������� ������ 
     * @param $table  String  - ��� �������	
     * @param $data Array  - ������
     * @return Integer
     */
    public function InsertRow($table, $data = array()) {
        $sql = 'insert into `' . $table . '` set ';
        $insert = array();
        if (count($data) > 0) {
            $insert = $this->getKeyAndValue($data);
            $sql.=implode(', ', $insert);
            if ($s = $this->Query($sql))
                return $this->db->insert_id;
        }
        return false;
    }

    /**
     * ��������� ������
     * @param $table  String  - ��� �������	
     * @param $data Array  - ������
     * @param $where Array/String  - �������
     * @return Boolean(false/true)
     */
    public function UpdateRow($table, $data = array(), $where = array()) {
        $sql = 'update `' . $table . '` set ';
        $Sqlwhere = '';
        $insert = array();

        if (!is_array($where))
            $Sqlwhere = ' where id=' . intval($where);
        else {
            $w = $this->getKeyValueWhere($where);
            $Sqlwhere = ' where ' . implode(' and ', $w);
        }

        if (count($data) > 0) {
            $insert = $this->getKeyAndValue($data);
            $sql.=implode(', ', $insert) . $Sqlwhere;
            if ($this->Query($sql))
                return true;
        }


        return false;
    }

    /**
     * �������� ������
     * @param $table  String  - ��� �������		
     * @param $where Array/String  - �������
     * @return Boolean(false/true)
     */
    public function DeleteRow($table, $where = array()) {

        $sql = 'delete from `' . $table . '`';

        $w = array();

        if (!is_array($where) and $where != '')
            $w[] = 'id=' . intval($where);
        elseif (count($where) > 0)
            $w = $this->getKeyValueWhere($where);

        if (count($w) > 0) {
            $Sqlwhere = ' where ' . implode(' and ', $w);
            $sql.=$Sqlwhere;
            if ($this->Query($sql))
                return true;
        }

        return false;
    }

    /**
     * �������������� ������ escape_string
     * @param $str String

     * @return String
     */
    public function sql_escape($str) {
        return $this->db->real_escape_string(trim($str));
    }

    /**
     * 
     * @param $data Array  - ������	
     * @param $escape Boolean  - �������
     * @return Array
     */
    public function getKeyAndValue($data = array(), $escape = true) {
        $result = array();
        if (count($data) > 0) {
            foreach ($data as $k => $v) {
                if ($escape)
                    $v = $this->sql_escape($v);

                $result[] = "`" . $k . "`='" . $v . "'";
            }
        }
        return $result;
    }

    /**
     * 
     * @param $where Array  - ������	
     * @return Array
     */
    public function getKeyValueWhere($where = array()) {
        $w = array();
        foreach ($where as $key => $val) {
            if (is_array($val)) {
                $w[] = $key . ' in (' . implode(',', $val) . ') ';
            } else
                $w[] = $key . '="'. $val.'"' ;
        }
        return $w;
    }

    public function InsertTest($table, $data = array()) {
        $sql = 'insert into `' . $table . '` set ';
        $insert = array();
        if (count($data) > 0) {
            $insert = $this->getKeyAndValue($data);
            $sql.=implode(', ', $insert);
            return $sql;
        }
        return false;
    }

    public function DeleteRowTest($table, $where = array()) {

        $sql = 'delete from `' . $table . '`';

        $w = array();

        if (!is_array($where) and $where != '')
            $w[] = 'id=' . intval($where);
        elseif (count($where) > 0)
            $w = $this->getKeyValueWhere($where);

        if (count($w) > 0) {
            $Sqlwhere = ' where ' . implode(' and ', $w);
            $sql.=$Sqlwhere;
            return $sql;
        }

        return false;
    }

}
?>