<?php

function parserPage($tpl_files,$mas=array(),$content=array())
{   $strims='';
    $pathToTpl=PATH_TPL.'/'.MODEL.'/'.$tpl_files;
   	$view=array();

   	if($tpl_files!='' or (file_exists($pathToTpl)==true))
   	{
   		ob_start();
        if(count($mas)>0)
        	$view=$mas;
		$content=$content;
	    include $pathToTpl;
	    $strims=ob_get_contents();
	   	ob_end_clean();

   	}
   	else
   	{
   		$strims='Файл('.$tpl_files.') не существуетdddd';
   	}
     return $strims;
}

function RenderPage($content,$tpl='')
{
   $content=$content;
   if($tpl=='')
		$pathToTpl=PATH_TPL.MODEL.'/'.NAME_LAYOUT;
   else	
		$pathToTpl=PATH_TPL.MODEL.'/'.$tpl;
   if(file_exists($pathToTpl)==true)
   {
     	ob_start();
        include $pathToTpl;
	    $html=ob_get_contents();
	   	ob_end_clean();
   }
   else
   	  $html='Файл('.$pathToTpl.') не существует';

   echo $html;

}
function IncludeView($tpl_files)
{
	$pathToTpl=PATH_TPL.'/'.$tpl_files;
	if($tpl_files!='' or (file_exists($pathToTpl)==true))
	include $pathToTpl;	
}




function formsCreate($tbl_name)
{
    $s=SelectAll('SHOW FIELDS FROM '.$tbl_name);
      echo '<table width=650px>';
      echo "\r\n";
    $i=0;
    while($i<count($s))
    {
       if(preg_match('/[0-9,]{1,3}/',$s[$i]['Type'],$t))
        $type=str_replace('('.$t[0].')','',$s[$i]['Type']);
       else
		$type='text';
		switch($type)
		{
			case 'int':
			case 'varchar':
			case 'double':
			case 'smallint':
				$field='<input id="'.$s[$i]['Field'].'" name="'.$s[$i]['Field'].'" type="text" value="">';
			break;
			case 'text':
				$field='<textarea id="'.$s[$i]['Field'].'" name="'.$s[$i]['Field'].'" rows=5 cols=20 wrap="off"></textarea>';
			break;

		}

         echo '	<tr>
	     <td width="150px"></td>
	     <td width="450px">'.$field.'</td>
	</tr>';
         echo "\r\n";
        $i++;
    }
    echo '</table>';

}
function dateStr($date,$tims='')
{
   if($tims=='')
   		$tims='00:00';
   	$d=preg_replace('/(\d{2})\W(\d{2})\W(\d{2}|\d{4})/', '$2/$1/$3', $date);

	return strtotime("$d $tims");
}


function __tr($lang_code) {

    global $_constant_languages;
	
    list($division, $code) =  explode('.', $lang_code);

    if(isset($_constant_languages[$division][$code])) {
        return $_constant_languages[$division][$code];
    }
    else {
		saveNoneTR($code);
        return $code;
    }

}
function saveNoneTR($phrase)
{
	$path=PATH_ROOT.'translate_none/none_tr';
	$pathTemp=PATH_ROOT.'translate_none/phrase_temp/'.md5($phrase);
	$text="'".$phrase."'=>'".$phrase."',";
	$text.="\r\n";	
	if(!file_exists($pathTemp))
	{
		if(!file_exists($path))
		{
			file_put_contents($path,$text);
			chmod($path,0777);
		}
		else
		{
			$f=fopen($path,'a');
			fwrite($f, $text);
			fclose($f);
		}
		file_put_contents($pathTemp,'none');
	}
}
// определение языка по IP
function ipDetector() {

//$ip = $_SERVER['REMOTE_ADDR'];
$ip = USER_IP_ADDRESS;

if(isset($_SESSION['iplocation_t'])) return;
if($_SERVER['REQUEST_URI'] != "/") return;

$answer = file_get_contents("http://api.sypexgeo.net/json/{$ip}");
if(!$answer) return;

$answer = json_decode($answer);

    if(isset($answer->country)) {
		$_SESSION['iplocation_tISO']=$answer->country->iso;
		define('REGION',$answer->country->iso);
        if($answer->country->iso == 'UA' OR $answer->country->iso == 'RU') {
            // RU
            $_SESSION['iplocation_t'] = 'ru';

            header("Location: /ru");
            exit;

        }
        elseif($answer->country->iso == 'CZ') {
            // CZ
            $_SESSION['iplocation_t'] = 'cz';

            header("Location: /cz");
            exit;


        } else {
            // EN
            $_SESSION['iplocation_t'] = 'en';
        }

    }
	
}



// определение email адреса для чата
// get user email
function getUserEmail() {
    $user_id = (int)$_SESSION[USER_SESSION_NAME];
    if(!$user_id) return;

    $db_connectors = db_provider();

    $res = $db_connectors->FetchRow("SELECT `email` FROM `users` WHERE `id` = {$user_id}");

    if($res['email']) return $res['email'];
}


// getUserName
function getUserName() {
    $user_id = (int)$_SESSION[USER_SESSION_NAME];
    if(!$user_id) return;

    $db_connectors = db_provider();

    $verifyNameRes = $db_connectors->FetchRow("SELECT `name`  FROM `verification` WHERE `id_user` = {$user_id} ");

    if($verifyNameRes) {
        $verifyName  = $verifyNameRes['name'];
        return $verifyName;
    }

    $res = $db_connectors->FetchRow("SELECT `name` FROM `users` WHERE `id` = {$user_id}");

    if($res['name']) return $res['name'];
}

function db_provider() {

    static $db;

    if(!$db) {
        $db =  new Database;
    }

    return $db;

}


// getUserVerificationData
function verificationData() {

    $user_id = (int)$_SESSION[USER_SESSION_NAME];
    if(!$user_id) return;

    $db_connectors = db_provider();

    $verifyData = $db_connectors->FetchRow("SELECT * FROM `verification` WHERE `id_user` = {$user_id} LIMIT 1");

    if(!$verifyData) return;

    $res = array();

    $res['birthdate'] = $verifyData['birthdate'];
    //$res['citizenship'] = $verifyData['citizenship'];
    $res['country'] = $verifyData['country'];
    $res['city'] = $verifyData['city'];
    $res['address'] = $verifyData['country'].', '.$verifyData['city'].' '.$verifyData['adres'];
    //$res['wallets'] = $verifyData['wallets'];
	return json_encode($res);
}



// получаем все встречи
function getMeetings() {

    $sql = "SELECT 
        meetings.name,
        meetings.mdate,
        meetings.link,
        meetings.image,
        meetings_lang.city
        
        FROM `meetings`
        LEFT JOIN `meetings_lang` ON meetings.id = meetings_lang.meeting_id  
        WHERE meetings_lang.lang = '".LANG."' ORDER BY meetings.mdate ";

    $res = db_provider()->FetchAll($sql);

    foreach($res AS &$one) {

        $meetTime = strtotime($one['mdate']);
        $month = date("F", $meetTime);
        $month = strtoupper($month);

        $one['mdate'] = $month.date(" j, Y", $meetTime);
        $one['dis'] = ($meetTime < time()) ? true : false;
        $one['firstdate'] = date("d/m/Y", $meetTime);

    }

    usort($res, function($a, $b) {

        if($a['dis'] == $b['dis']) return 0;
        return ($a['dis'] < $b['dis']) ? -1 : 1;
    });

    //echo '<pre>';
    //var_dump($res);die();

    return $res;
}
