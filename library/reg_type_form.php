<?php	

	$NAME_OF_REG_PARAMS=array();
	$NAME_OF_REG_PARAMS['ru']=array();
	$NAME_OF_REG_PARAMS['sk']=array();
	$NAME_OF_REG_PARAMS['en']=array();
	
	//form business_name
	$NAME_OF_REG_PARAMS['ru']['business_name']=array(
		'forms'=>array(
			'name_input_pars'=>'PF',
			'params'=>array(
				1=>'state(-owned) enterprise',
				2=>'joint-stock company',
				3=>'private limited liability company',
				4=>'general partnership',
				5=>'limited partnership',
				6=>'self-employed individual',
				7=>'cooperative',
				8=>'branch of an enterprise',
				9=>'other legal person',
				10=>'foreign legal person',
				11=>'branch of a foreign person',
				14=>'municipal enterprise',
				15=>'municipal authority',
				16=>'European Economic Interest Grouping',
				17=>'Societas Europaea',
				18=>'state(-owned) enterprise',
				19=>'societas cooperativa Europaea'
				)		
		
		),
		'name'=>array(
			'name_input_pars'=>'OBMENO',
			'params'=>'',
			'encode'=>'true'	
		),	
		'court'=>array(
			'name_input_pars'=>'SID',
			'params'=>array(				
				3=>'District Court Banská Bystrica',
				2=>'District Court Bratislava I',
				4=>'District Court Košice I',
				9=>'District Court Nitra',
				8=>'District Court Prešov',
				6=>'District Court Trenčín',
				7=>'District Court Trnava',
				5=>'District Court Žilina'
			)
		)
	
	);
	
	
	$NAME_OF_REG_PARAMS['ru']['identification_number']=array(
		
		'name'=>array(
			'name_input_pars'=>'ICO',
			'params'=>'',	
		),	
		'court'=>array(
			'name_input_pars'=>'SID',
			'params'=>array(				
				3=>'District Court Banská Bystrica',
				2=>'District Court Bratislava I',
				4=>'District Court Košice I',
				9=>'District Court Nitra',
				8=>'District Court Prešov',
				6=>'District Court Trenčín',
				7=>'District Court Trnava',
				5=>'District Court Žilina'
			)
		)
	
	);
	$NAME_OF_REG_PARAMS['ru']['registered_seat']=array(
		
		'street'=>array(
			'name_input_pars'=>'ULICA',
			'params'=>'',	
			'encode'=>'true'	
		),
		'num'=>array(
			'name_input_pars'=>'CISLO',
			'params'=>'',	
		),
		'city'=>array(
			'name_input_pars'=>'OBEC',
			'params'=>'',
			'encode'=>'true',	
		),	
		
		'court'=>array(
			'name_input_pars'=>'SID',
			'params'=>array(				
				3=>'District Court Banská Bystrica',
				2=>'District Court Bratislava I',
				4=>'District Court Košice I',
				9=>'District Court Nitra',
				8=>'District Court Prešov',
				6=>'District Court Trenčín',
				7=>'District Court Trnava',
				5=>'District Court Žilina'
			)
		)
	
	);
	
	$NAME_OF_REG_PARAMS['ru']['registration_number']=array(
		
		'num'=>array(
			'name_input_pars'=>'VLOZKA',
			'params'=>'',	
			'encode'=>'true'	
		),
		'section'=>array(
			'name_input_pars'=>'ODD',
			'params'=>array(
				1=>'Pš',
				2=>'Sa',
				3=>'Sro',
				4=>'Sr',
				5=>'Firm',
				6=>'Dr',
				7=>'Po',
				8=>'Pšn'				
			),	
		),	
		
		'court'=>array(
			'name_input_pars'=>'SID',
			'params'=>array(				
				3=>'District Court Banská Bystrica',
				2=>'District Court Bratislava I',
				4=>'District Court Košice I',
				9=>'District Court Nitra',
				8=>'District Court Prešov',
				6=>'District Court Trenčín',
				7=>'District Court Trnava',
				5=>'District Court Žilina'
			)
		)
	
	);
	$NAME_OF_REG_PARAMS['ru']['name_of_a_person']=array(
		
		'last_name'=>array(
			'name_input_pars'=>'PR',
			'params'=>'',	
			'encode'=>'true'	
		),
		'first_name'=>array(
			'name_input_pars'=>'MENO',
			'params'=>'',	
			'encode'=>'true'	
		),
		'type_person'=>array(
			'name_input_pars'=>'T',
			'params'=>array(
				"f0"=>"natural",
				"f1"=>"natural - statutory representative",
				"f2"=>"natural - proxy (legal representative)",
				"f3"=>"natural - partner",
				"f4"=>"natural - memb. of the Supervisory Board",
				"f5"=>"natural - liquidator",
				"f6"=>"natural - head of branch of an enterprise",
				"f7"=>"natural - head of organisational unit",
				"f8"=>"natural - limited partner",
				"f9"=>"natural - general partner",
				"f10"=>"natural - entered as entity",
				"p1"=>"legal - member or partner",
				"p2"=>"legal - limited partner",
				"p3"=>"legal - general partner",
				"p4"=>"legal - founder"		
			),	
		),	
		
		'court'=>array(
			'name_input_pars'=>'SID',
			'params'=>array(				
				3=>'District Court Banská Bystrica',
				2=>'District Court Bratislava I',
				4=>'District Court Košice I',
				9=>'District Court Nitra',
				8=>'District Court Prešov',
				6=>'District Court Trenčín',
				7=>'District Court Trnava',
				5=>'District Court Žilina'
			)
		)
	
	);
	
		
?>