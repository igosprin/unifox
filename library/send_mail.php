<?php
class SendMail{
		private $path=NULL;
		
		function __construct($path='')
		{
			if($path=='')
				$path=PATH_ROOT;	
			$this->path=$path;
		}
		
		/**
		* Сборка шаблона и отправка письма 
		* @param $data_publish Array  - настройки письма:action - Тип шаблона title-заголовок,email-кому шлем. Если action пустое то должен быть отрендеренный шаблон body  
		* @param $data_mail Array  - данные для шаблона. Это в том случае если action не пустой  
		*/
		
		public function send($data_publish=array(),$data_mail=array(),$attachment='')
		{
			if(isset($data_publish['action']))						
				$data_publish['body']=$this->create_tpl($data_publish['action'],$data_mail);
			if(!isset($data_publish['title']))
				$data_publish['title']='';
			if($attachment!='')
				$data_publish['attachment']=$attachment;
			//var_dump($data_publish);die('sd');	
			if(isset($data_publish['email']))
				$this->mail_send($data_publish,$data_publish['email']);
				
		}
		/**
		* Сборка шаблона 
		* @param $action  String - Тип шаблона  
		* @param $data Array  - данные для шаблона. 
		*/
		private function create_tpl($action,$data=array())
		{
			$tpl='';
			switch($action)
			{
				case 'recovery':
					$tpl='recovery.html';
				break;
				case 'buy_fox':
					$tpl='buy_fox.html';
				break;
				case 'activate_account':
					$tpl='activate_account.html';
				break;
				case 'activate_email':
					$tpl='activate_email.html';
				break;
				case 'activate_email_new':
					$tpl='activate_email_new.html';
				break;
				case 'admin_request_verif':
					$tpl='admin_request_verif.html';
				break;
				case 'admin_request_airdrop':
					$tpl='admin_request_airdrop.html';
				break;
					
				
			}
			//$result=array();
			$body='';
			if($tpl!='')
				$body=$this->createPage($tpl,$data);
			return $body;
		}
		/**
		* отправка письма
		* @param $data_publish Array  - настройки письма:  title-заголовок,body- текст письма.
		* @param $email_to String  - кому шлем. 
		*/
		private function mail_send($data_publish=array(),$email_to='',$sender_name='Unifox.io',$sender_mail='info@unifox.io')
		{
			if(!$data_publish['title'])
				$data_publish['title']='';
			if(!$data_publish['body'])
				$data_publish['body']='';
			
			if($email_to!='')
			{	
				if(!isset($data_publish['attachment']))
				{
					$headers = array();
					$headers[] = "MIME-Version: 1.0";
					$headers[] = "Content-type: text/html; charset=iso-8859-1";
					$headers[] = "Mesaage-id: " .$this->generateMessageID();
					$headers[] = "From: ".$sender_name." <".$sender_mail.">";	
					$headers[] = "Date: ".date('d.m.Y G:i');
					$headers[] = "Return-Path: <".$sender_mail.">";
					$headers[] = "X-Priority: 1";
					$headers[] = "X-Mailer: PHP/" . phpversion();				
					
					@mail($email_to, $data_publish['title'], $data_publish['body'], implode("\r\n", $headers));
					
				}
				else
				{
					//die('adada ');
					$file_size = filesize($data_publish['attachment']);
					$handle = fopen($data_publish['attachment'], "r");
					$content = fread($handle, $file_size);
					fclose($handle);
					$content = chunk_split(base64_encode($content));
					$uid = md5(uniqid(time()));
					$name = basename($data_publish['attachment']);
					$header= "MIME-Version: 1.0\r\n";
					$header.= "From: ".$sender_name." <".$sender_mail.">\r\n";
					
					
					$header.= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
					$header.= "This is a multi-part message in MIME format.\r\n";
					$header.= "--".$uid."\r\n";
					
					$header.= "Content-type: text/html; charset=\"utf-8\" \r\n";		
					$header.= $data_publish['body']."\r\n\r\n";			
					$header.= "--".$uid."\r\n";
					
					$header.= "Content-Type: application/octet-stream; name=\"".$name."\"\r\n"; // use different content types here
					$header.= "Content-Transfer-Encoding: base64\r\n";
					$header.= "Content-Disposition: attachment; filename=\"".$name."\"\r\n\r\n";
					$header.= $content."\r\n\r\n";
					$header.= "--".$uid."--";
					@mail($email_to, $data_publish['title'], $data_publish['body'], $header);
					
					
				}
			}
		}
		
		
		private function createPage($tpl_files,$mas=array())
		{   
			$strims='';
			$pathToTpl=$this->path.'/'.$tpl_files;
			$view=array();
			$mas['DOMEN_NAME']=DOMEN_NAME;	
			$mas['PATH_TPL']=$this->path.'/';	
			if($tpl_files!='' or (file_exists($pathToTpl)==true))
			{
				ob_start();
				if(count($mas)>0)
					$view=$mas;
				
				include $pathToTpl;
				$strims=ob_get_contents();
				ob_end_clean();
			}
			
			 return $strims;
		}
		private function generateMessageID()
		{
		  return sprintf(
			"<%s.%s@%s>",
			base_convert(microtime(), 10, 36),
			base_convert(bin2hex(openssl_random_pseudo_bytes(8)), 16, 36),
			$_SERVER['SERVER_NAME']
		  );
		}	
		
	} 
?>	