<?php

		function GenerateCaptcha ()
		{
			$code = '';
			for( $i=0; $i<6 ; $i++ )  {
				$code = $code . rand(0,9);
			}
			return $code;
		}

	   
		function uri_func()
	    {      
			$uri= urldecode($_SERVER['REQUEST_URI']);
			$uri=str_replace('"','',$uri);
			$uri=str_replace("'","",$uri);
			$urim=explode('?',$uri);
			$uri_par=explode('/',$urim[0]);                        
                      
			/*LANGUAGE*/
			if($uri_par[1]==DEFAULT_LANG)
			{
				$output = array_slice($uri_par,0,1);
				$output1 = array_slice($uri_par,2);
				$uri_par = array_merge($output, $output1);
				
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /".implode("/",$uri_par));
				die();
			}
			else
			{
				switch($uri_par[1])
				{
					case 'pl':
						$output = array_slice($uri_par,0,1);
						$output1 = array_slice($uri_par,2);
						$uri_par = array_merge($output, $output1);
						$uri_par['language'] = 'pl';
					break;
					case 'cz':
						$output = array_slice($uri_par,0,1);
						$output1 = array_slice($uri_par,2);
						$uri_par = array_merge($output, $output1);
						$uri_par['language'] = 'cz';
					break;
					case 'de':
						$output = array_slice($uri_par,0,1);
						$output1 = array_slice($uri_par,2);
						$uri_par = array_merge($output, $output1);
						$uri_par['language'] = 'de';
					break;
					case 'fr':
						$output = array_slice($uri_par,0,1);
						$output1 = array_slice($uri_par,2);
						$uri_par = array_merge($output, $output1);
						$uri_par['language'] = 'fr';
					break;
					case 'ru':
						$output = array_slice($uri_par,0,1);
						$output1 = array_slice($uri_par,2);
						$uri_par = array_merge($output, $output1);
						$uri_par['language'] = 'ru';
					break;
					default:
						$uri_par['language']='';
					break;
				}
			}
			
            return $uri_par;	
	    }

		function getParams()
		{
			$in=explode('.',$_SERVER['REQUEST_URI']);
			if(count($in)>1)
				$uri_params['controller']='error';	
			$url = uri_func();
			unset($url[0]);
			
			$uri_params=array();
			$uri_params['model']='default';
			$uri_params['language']=$url['language'];
			
			if($url[1]!='')
			{
				switch($url[1])
				{
					
					case 'documents':
						$uri_params['controller'] = 'document';
						$uri_params['action'] = 'index';					
					break;
					/*Personal*/	
					case 'personal':
						$uri_params['model']='personal';
						if(isset($url[2]) and $url[2]!='')
						{
							if($url[2]=='affiliate')
								$url[2]='affailiate';
							if($url[2]=='wallets' and isset($url[3]) and trim($url[3])!="")
								$uri_params['wallet_page']=trim($url[3]);
							
							$uri_params['controller'] = $url[2];						
							$uri_params['action'] = 'index';
						}
						else
						{
							$uri_params['controller'] = 'index';						
							$uri_params['action'] = 'index';
						}
											
					break;
					case 'admin':
						$uri_params['model']='personal';						
						$c='admin/index';
						if(isset($url[2]) and $url[2]!='')
							$c= 'admin/'.$url[2];						
						$uri_params['controller'] = $c;
						$uri_params['action'] = 'index';		
					break;
					/*Login page*/
					case 'signin':
						$uri_params['model']='personal';
						$uri_params['controller'] = 'login';						
						$uri_params['action'] = 'index';								
					break;
					case 'recovery':
						$uri_params['model']='personal';
						$uri_params['controller'] = 'login';						
						$uri_params['action'] = 'recovery';								
					break;
					case 'recovery_access':
						$uri_params['model']='personal';
						$uri_params['controller'] = 'login';						
						$uri_params['action'] = 'recovery_access';								
					break;
					case 'signup':
						$uri_params['model']='personal';
						$uri_params['controller'] = 'login';						
						$uri_params['action'] = 'registration';								
					break;
					case 'auth-fb':
						$uri_params['model']='personal';
						$uri_params['controller'] = 'login';						
						$uri_params['action'] = 'social';								
						$uri_params['type'] = 'social_fb';								
					break;
					case 'logout':
						$uri_params['model']='personal';
						$uri_params['controller'] = 'logout';
						$uri_params['action'] = 'index';		
					break;
					case 'develop':	
						$uri_params['model']='personal';	
						$uri_params['controller'] = 'develop';
						$uri_params['action'] = 'index';		
					break;	
					case 'activate_link':	
						$uri_params['model']='personal';	
						$uri_params['controller'] = 'login';
						$uri_params['action'] = 'activate_link_send';		
					break;
					case 'activate':	
						$uri_params['model']='personal';	
						$uri_params['controller'] = 'login';
						$uri_params['action'] = 'activate_account';		
					break;	
					/*Login page end*/
					
					
					default:
						$uri_params['controller'] = 'error';
						
						
						
						
					break;	
					
				}
				
				$uri_params['is_admin']=false;
				if($url[1]=='admin')
					$uri_params['is_admin']=true;
				
				return $uri_params;
				
			} else {
				$uri_params['controller'] = 'index';
				$uri_params['action'] = 'index';
				
				return $uri_params;
			}
		}	
	
 ?>