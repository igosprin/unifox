<?php

		function GenerateCaptcha ()
		{
			$code = '';
			for( $i=0; $i<6 ; $i++ )  {
				$code = $code . rand(0,9);
			}
			return $code;
		}

	   
		function uri_func()
	    {      
			$uri= urldecode($_SERVER['REQUEST_URI']);
			$uri=str_replace('"','',$uri);
			$uri=str_replace("'","",$uri);
			$urim=explode('?',$uri);
			$uri_par=explode('/',$urim[0]);
			return $uri_par;	
	    }

		function getParams()
		{
			$in=explode('.',$_SERVER['REQUEST_URI']);
			if(count($in)>1)
				$uri_params['controller']='error';	
			$url = uri_func();
			unset($url[0]);
			//var_dump($url);
			$uri_params=array();
			$uri_params['model']='default';
			if($url[1]!='')
			{
				switch($url[1])
				{
					case 'admin':
						$uri_params['model']='admin';
						if(isset($url[2]) and $url[2]!='')
							$uri_params['controller']='error';
					break;					
					
					/*case 'services':
						$uri_params['controller'] = 'services';
					    $uri_params['action'] = '';
						if(isset($url[2]) and $url[2]!='')
							 $uri_params['action'] = $url[2];
					break;*/
					
					case 'services-slovakiya':
						$uri_params['controller'] = 'services';
					    $uri_params['action'] = 'index';
						$uri_params['country']=1;
						if(isset($url[2]) and $url[2]!='')
							 $uri_params['action'] = $url[2];
					break;
					case 'services-chekhiya':
						$uri_params['controller'] = 'services';
					    $uri_params['action'] = 'index';
						$uri_params['country']=2;
						if(isset($url[2]) and $url[2]!='')
							 $uri_params['action'] = $url[2];
					break;
					
					case 'services-polsha':
					$uri_params['controller'] = 'services';
					    $uri_params['action'] = 'index';
						$uri_params['country']=3;
						if(isset($url[2]) and $url[2]!='')
							 $uri_params['action'] = $url[2];
					break;
					case 'services-vengria':
					$uri_params['controller'] = 'services';
					    $uri_params['action'] = 'index';
						$uri_params['country']=4;
						if(isset($url[2]) and $url[2]!='')
							 $uri_params['action'] = $url[2];
					break;
					case 'services-europa':
					$uri_params['controller'] = 'services';
					    $uri_params['action'] = 'index';
						$uri_params['country']=99;
						if(isset($url[2]) and $url[2]!='')
							 $uri_params['action'] = $url[2];
					break;
					
                    case 'blog':
					$uri_params['controller'] = 'blog';
					$uri_params['action'] = 'index';	
						if(!isset($url[2]))    
							$uri_params['action'] = 'index';
						elseif($url[2]!='') 
							 $uri_params['action'] = $url[2];
					break;
                                        
                    case 'sotrudniki':
					$uri_params['controller'] = 'sotrudniki';
						if(!isset($url[2]))    
							$uri_params['action'] = 'index';
						else 
							 $uri_params['action'] = $url[2];
					break;
                                        
                    case 'contacts':
					$uri_params['controller'] = 'contacts';
						if(!isset($url[2]))    
							$uri_params['action'] = 'index';
						else 
							 $uri_params['action'] = $url[2];
					break;
					case 'news':
					$uri_params['controller'] = 'news';
					$uri_params['action'] = 'index';
						if(!isset($url[2]))    
							$uri_params['action'] = 'index';
						elseif($url[2]!='') 
							 $uri_params['action'] = $url[2];
					break;
					case 'business-register-slovakia':
						$uri_params['controller'] = 'register';
						$uri_params['action'] = 'index';	
						if(isset($url[2]) and $url[2]!='')
							$uri_params['controller'] = 'error';
											
					break;
					case 'ajax':
						$uri_params['controller'] = 'ajax';
						$uri_params['action'] = 'index';						
					break;
					case 'business':
					$uri_params['controller'] = 'busines';
					$uri_params['action'] = 'index';
						if(!isset($url[2]))    
							$uri_params['action'] = 'index';
						elseif($url[2]!='') 
							 $uri_params['action'] = $url[2];
					break;
					case 'price':
						$uri_params['controller'] = 'prices';
						$uri_params['action'] = 'index';						
					break;
					case 'test':
						$uri_params['controller'] = 'test';
						$uri_params['action'] = 'index';						
					break;
					case 'faq':
						$uri_params['controller'] = 'faq';
						$uri_params['action'] = 'index';						
					break;
					case 'search':
						$uri_params['params']='';
						$uri_params['action']='';
						$uri_params['controller'] = 'search';
						
						if(isset($url[2]) and $url[2]=='word')
							$uri_params['action'] = 'word';
						if(isset($url[2]) and $url[2]=='tag')	
							$uri_params['action'] = 'tag';
						if(isset($url[3]) and $url[3]!='')
							$uri_params['params'] = $url[3];
							
					break;
					case 'autoriz':
						$uri_params['controller'] = 'autorization';
						$uri_params['action'] = 'index';						
					break;
					case 'profile':
						$uri_params['controller'] = 'registry';
						$uri_params['action'] = 'index';						
					break;
					case 'registry':
						$uri_params['controller'] = 'registry';
						$uri_params['action'] = 'registry';						
					break;
					case 'faktura':
						$uri_params['controller'] = 'fakture';
						$uri_params['action'] = 'index';						
					break;
					case 'checks':
						$uri_params['controller'] = 'check';
						$uri_params['action'] = 'index';						
					break;
					case 'mycompany':
						$uri_params['controller'] = 'companybox';
						$uri_params['action'] = 'index';						
					break;
					
					case 'sitemap.xml':
						$uri_params['controller'] = 'mapsite';
						$uri_params['action'] = 'index';						
					break;
					
					default:
						$uri_params['controller'] = 'error';
						
						if(!isset($url[2]) or @$url[2]=='')
						{
							$url[1]=str_replace(array('"',"'"),'',$url[1]);
							$d=SelectOne('select count(*) as cc from static_'.LANG.' where url="'.htmlspecialchars($url[1]).'"');
							if($d['cc']>0){
								$uri_params['controller']='static';
								$d=SelectOne('select id from static_'.LANG.' where url="'.htmlspecialchars($url[1]).'"');
								$uri_params['action']=$d['id'];
							}
						}
						
						
					break;	
					
				}
				
				return $uri_params;
				
			} else {
				$uri_params['controller'] = 'index';
				$uri_params['action'] = 'index';
				return $uri_params;
			}
		}

		function paramUri()
		    {
                 $url = self::uri_func();
                 $params=array();
				 $params['is_product']=0;
				 $params['is_cat']=0;
                 unset($url[0]);
                 if($url[1]!='')
                 {

                 	 $params['wages']=$url[1];
					 $params['is_cat']=1;
						if(isset($url[2]))
						{
							$params['is_cat']=0;
							if(is_numeric($url[2]))
								{
									$params['link']=$url[2];
									$params['is_cat']=1;
								}
							else
							{
								$temp_url = explode('-',$url[2]);
								if (is_numeric($temp_url[count($temp_url)-1]))
								{
									$params['link']=$url[2];
									$params['is_product']=1;
								}
								else
								{
									$params['brand']=$url[2];
									$params['is_cat']=1;
									if(isset($url[3]))
									{
										$params['link']=$url[3];
									}
								}
							}
							if($url[1]=='contacts' or $url[1]=='cut' or $url[1]=='discounts' or $url[1]=='delivery')
								$params['is_cat']=0;
						}
						
				}
                 else
                 {  if($url[1]=='product')
	                 {

                 	    $params['wages']=$url[2];
	                 }

                 	else
                 	{
                        if($url[1]=='new')
                        {
                        	if(isset($url[2]))
                        	$params['page']=$url[3];
                        }
                        else
                        {
                        	if($url[1]=='docs' and $url[2]!='question')
							{
							
								$params['uri']=$url[2];
							}
							else
							{
								if($url[1]=='search')
								{
									if(isset($_POST['find']))
									{
										header('Location: /search/'.$_POST['find']);
									}
									else
									{
										$params['search_find']=$url[2];
									}
								}
							}
                        }



                 	}

                 }
                		unset($url[1]);
                 		unset($url[2]);
                 		 $i=3;

		                while($i<(count($url)+3))
		                 {

		                   @$url[$i+1]=str_replace('+','',$url[$i+1]);
		                   $url[$i+1]=str_replace('"','',$url[$i+1]);
		                   $url[$i+1]=str_replace("'",'',$url[$i+1]);

		                   $params[$url[$i]]=$url[$i+1];
		                   $i=$i+2;

		                 }

                 return $params;
			}
	
 ?>