<?php
class UsersVisitation
{
	protected $nameCookiePrefix='uniuser_visit';
	protected $user_id=null;
	protected $db=null;
	
	function __construct($user_id,$db)
	{
		$this->user_id=$user_id;	
		$this->db=$db;	
	}
	function put()
	{
		if(!$this->hasCookies())
		{
			$day=intval(date('Ymd'));
			$this->db->InsertRow('users_visitation',array('id_user'=>$this->user_id,'date'=>time(),'day'=>$day));
			$this->setCookies();
		}					
	}
	protected function hasCookies()
	{
		if(!isset($_COOKIE[$this->nameCookiePrefix]))
			return false;		
		return true;
	}
	protected function setCookies()
	{
		$date_life=strtotime(date('Y-m-d 23:59:59'));		
		setCookie($this->nameCookiePrefix,'flag',$date_life,'/');
	}
}