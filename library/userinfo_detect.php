<?php

function user_browser($agent) {
	preg_match("/(MSIE|Opera|Firefox|Chrome|Version|Opera Mini|Netscape|Konqueror|SeaMonkey|Camino|Minefield|Iceweasel|K-Meleon|Maxthon)(?:\/| )([0-9.]+)/", $agent, $browser_info);
	list(,$browser,$version) = $browser_info;
	if (preg_match("/Opera ([0-9.]+)/i", $agent, $opera)) return 'Opera '.$opera[1];
	if ($browser == 'MSIE') {
		preg_match("/(Maxthon|Avant Browser|MyIE2)/i", $agent, $ie);
		if ($ie) return $ie[1].' based on IE '.$version;
		return 'IE '.$version;
	}
    if ($browser == 'Firefox') {
		preg_match("/(Flock|Navigator|Epiphany)\/([0-9.]+)/", $agent, $ff);
		if ($ff) return $ff[1].' '.$ff[2];
	}
	if ($browser == 'Opera' && $version == '9.80') return 'Opera '.substr($agent,-5);
	if ($browser == 'Version') return 'Safari '.$version;
	if (!$browser && strpos($agent, 'Gecko')) return 'Browser based on Gecko';
	return $browser.' '.$version;
}
function getUserIP()
{
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}
define('BROWSER_NAME',user_browser($_SERVER['HTTP_USER_AGENT']));
define('USER_IP_ADDRESS',getUserIP());


$redirect=false;
if(!isset($_SESSION['iplocation_language2']))
{	
	$answer = file_get_contents("http://api.sypexgeo.net/json/".USER_IP_ADDRESS);
	if(!$answer) return;

	$answer = json_decode($answer);
	if(isset($answer->country)) {
		$_SESSION['localisation']=$answer->country;
		$_SESSION['iplocation_tISO']=$answer->country->iso;
		//define('REGION_USER',$answer->country->iso);
		if($answer->country->iso == 'UA' OR $answer->country->iso == 'RU') {
			// RU
			$_SESSION['iplocation_language2'] = 'ru';
			if($_SERVER['REQUEST_URI'] == "/")
				$redirect='/ru';
			}
		elseif($answer->country->iso == 'CZ') {
			// CZ
			$_SESSION['iplocation_language2'] = 'cz';
			if($_SERVER['REQUEST_URI'] == "/")
				$redirect='/cz';
			
		} else {
			// EN
			$_SESSION['iplocation_language2'] = 'en';
		}

	}
}
if($redirect)
{
	header("Location: ".$redirect);
    die();
}	
//var_dump($_SESSION['iplocation_tISO']);
