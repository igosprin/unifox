<?php
include_once '../../config.php';
include PATH_ROOT. '/library/db_clean.php';
$db_connectors=new Database;
//faq, roadmap????
$bounties = $db_connectors->FetchAll("SELECT * FROM `bounty_list`");

$csv = fopen(PATH_ROOT . '/tr_io/tr_out/csvdb.csv', 'w+');

foreach ($bounties as $bounty){

    $stKey = 'db.bounty.' . $bounty['type'] . '.' . $bounty['row_key'];

    fputcsv($csv, [
        $stKey . '.name', $bounty['name']
    ]);
    fputcsv($csv, [
        $stKey . '.comment', $bounty['comment']
    ]);

}
$bounties = $db_connectors->FetchAll("SELECT * FROM `faq_affiliate`");
foreach ($bounties as $bounty){

    $stKey = 'db.faq_affiliate';

    fputcsv($csv, [
        $stKey . '.question', $bounty['question']
    ]);
    fputcsv($csv, [
        $stKey . '.answer', $bounty['answer']
    ]);

}
