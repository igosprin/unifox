<?php

include_once '../../config.php';
require_once PATH_ROOT . '/lang/en.php';

$csv = fopen(PATH_ROOT . '/tr_io/tr_out/csv.csv', 'w+');

unset($_constant_languages['news-link']);
unset($_constant_languages['word']);
unset($_constant_languages['menu']);
foreach ($_constant_languages as $subKey => $subArray){

    foreach ($subArray as $key => $value){

        $key = $subKey . '.' . $key;
        fputcsv($csv, [$key, $value]);

    }

}

