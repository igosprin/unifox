<?
$lang='en';
$type='personal';
if(isset($_GET['type']))
	$type=$_GET['type'];

$path=str_replace('tr_io','',realpath( dirname( __FILE__ )));
include $path.'config.php';

$searchPathTPL=PATH_TPL.$type;
$searchPathCon=PATH_SCRIPT.'/'.$type;

if(!isset($_GET['go']))
{
	$trans_view=Ren($searchPathTPL);
	$trans_view=Ren($searchPathCon,$trans_view);
	//var_dump($trans_view);
	include $path.'lang/'.$lang.'.php';
	
	foreach($trans_view as $p)
	{
		if(!isset($_constant_languages['personal'][$p['t']]))
		{
			echo "'".$p['t']."'=>'".$p['t']."',\r\n";
		}
	}
	echo 'delete keys<br>';
	foreach($_constant_languages['personal'] as $k=>$p)
	{
		if(!isset($trans_view[$k]))
			echo $k.'<br/>';
	}
	
	//var_dump();
}

function Ren($path,$data=array())
{
	//echo $path.'<br>';
	if(is_dir($path))
	{
		if ($dh = opendir($path)) {			
			while (($file = readdir($dh)) !== false) {
				if ($file != "." && $file != "..") {
					$data=Ren($path.'/'.$file,$data);
				}				
			}
			closedir($dh);
		}
	}
	else 
	{
		$data=tr_get($path,$data);
	}
	 	
	return $data;	
}

function tr_get($path,$isset)
{
	$data=array();
	$n=explode('__tr(',file_get_contents($path));
	unset($n[0]);
	/*if($path=='C:\OSPanel\domains\unifox2/view/personal/confirmation/adres_form.html')
	{
		var_dump($n);
		die();
	}*/	
	
	if(isset($n[1]))
	{
		foreach($n as $str)
		{
			$phrase=returnT($str);			
			if($phrase)
			{
				$phrase=str_replace('personal.','',$phrase);
				if(!isset($isset[$phrase]))
					$isset[$phrase]=array('t'=>$phrase,'f'=>str_replace(PATH_ROOT,'',$path));
			}	
		}
	}	
	return $isset;
	
}

function returnT($str)
{
	$n=explode("')",$str);
	if(isset($n[1]))
		return str_replace("'","",$n[0]);
	else return false;
}